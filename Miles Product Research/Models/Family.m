#import "Family.h"


@interface Family ()

// Private interface goes here.

@end


@implementation Family

// Custom logic goes here.
+ (S2SOrderedDictionary*)filterValues{
    S2SOrderedDictionary *data = [[S2SOrderedDictionary alloc] init];
    
    [data setObject:@"" forKey:[NSString stringWithFormat:NSLocalizedString(@" -- All %@ --",@" -- Tutte le famiglie -- "),NSLocalizedString(@"families", @"families")]];
    
    NSSortDescriptor *sortByName = [[NSSortDescriptor alloc] initWithKey:@"item_description" ascending:YES];
    NSFetchRequest *f = [Family fetchRequest];
    [f setSortDescriptors:[NSArray arrayWithObject:sortByName]];
    
    NSMutableString *format = [[NSMutableString alloc] init];
    [format appendFormat:@"(code > 0) AND (language == '%@')",[[BTDataSync shared] language]];
    NSPredicate *p = [NSPredicate predicateWithFormat:format];
    [f setPredicate:p];
    
    for (Family *tmp in [Family objectsWithFetchRequest:f])
        [data setObject:tmp.code.stringValue forKey:tmp.item_description];
    
    return data;
}

@end
