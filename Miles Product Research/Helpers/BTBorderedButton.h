//
//  BTBorderedButton.h
//  RovigoBanca
//
//  Created by Davide Cenzi on 14/06/14.
//  Copyright (c) 2014 BlueTouch Development di Davide Cenzi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface BTBorderedButton : UIButton
- (void)setBorderColor:(UIColor*)color;
@end
