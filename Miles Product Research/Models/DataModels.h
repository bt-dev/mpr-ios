//
//  DataModels.h
//  MSR
//
//  Created by Davide Cenzi on 07/12/13.
//  Copyright (c) 2013 BlueTouch Development di Davide Cenzi. All rights reserved.
//

#ifndef MSR_DataModels_h
#define MSR_DataModels_h

#import "Brand.h"
#import "BrandType.h"
#import "ProductCategory.h"
#import "Family.h"
#import "Fibre.h"
#import "MacroSeason.h"
#import "Picture.h"
#import "Product.h"
#import "StitchType.h"
#import "Supplier.h"
#import "Yarn.h"
#import "User.h"
#import "ProductFibre.h"

#endif
