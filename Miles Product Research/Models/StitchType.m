#import "StitchType.h"


@interface StitchType ()

// Private interface goes here.

@end


@implementation StitchType

// Custom logic goes here.

+ (NSArray*)rootObjects{
    NSFetchRequest *f = [StitchType fetchRequest];
    NSPredicate *p = nil;
    NSMutableString *format = [[NSMutableString alloc] init];
    
    [format appendFormat:@"(parent_code == %@) AND (code > 0) AND (language == '%@')",nil,[[BTDataSync shared] language]];
    
    p = [NSPredicate predicateWithFormat:format];
    [f setPredicate:p];
    
    return [StitchType objectsWithFetchRequest:f];
}

+ (S2SOrderedDictionary*)filterValues{
    S2SOrderedDictionary *data = [[S2SOrderedDictionary alloc] init];
    
    [data setObject:@"" forKey:[NSString stringWithFormat:NSLocalizedString(@" -- All %@ --",@" -- Tutte le tipologie punto -- "),NSLocalizedString(@"stitch_types", @"stitch_types")]];
    
    NSSortDescriptor *sortByName = [[NSSortDescriptor alloc] initWithKey:@"item_description" ascending:YES];
    NSFetchRequest *f = [StitchType fetchRequest];
    [f setSortDescriptors:[NSArray arrayWithObject:sortByName]];
    
    NSMutableString *format = [[NSMutableString alloc] init];
    [format appendFormat:@"(code > 0) AND (language == '%@')",[[BTDataSync shared] language]];
    NSPredicate *p = [NSPredicate predicateWithFormat:format];
    [f setPredicate:p];
    
    for (StitchType *tmp in [StitchType objectsWithFetchRequest:f])
        [data setObject:tmp.code.stringValue forKey:tmp.item_description];
    
    return data;
}


@end
