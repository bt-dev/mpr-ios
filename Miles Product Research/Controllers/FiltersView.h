//
//  FiltersViewController.h
//  Miles Product Research
//
//  Created by Davide Cenzi on 29/06/14.
//  Copyright (c) 2014 BlueTouch Development di Davide Cenzi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BTBorderedButton.h"
#import "BTBorderedField.h"
#import "S2SOrderedDictionary.h"
#import "ActionSheetPicker.h"
#import "MSRTreeViewController.h"
#import <GCTagList.h>

@protocol FiltersViewDelegate <NSObject>

@optional
- (void)filtersUpdated:(NSDictionary*)filters;

@end

@interface FiltersView : UIView <TreeViewDelegate>
@property (nonatomic, strong) id <FiltersViewDelegate> delegate;
@property (nonatomic, weak) IBOutlet GCTagList* filterslist;

@property(nonatomic, strong) S2SOrderedDictionary *filtersFields;

//IBOutlets
@property (weak, nonatomic) IBOutlet BTBorderedField *yearField;
@property (weak, nonatomic) IBOutlet BTBorderedField *thinnessField;
@property (nonatomic, weak) IBOutlet BTBorderedButton *seasonButton;
@property (nonatomic, weak) IBOutlet BTBorderedButton *familyButton;

@property (nonatomic, weak) IBOutlet UISwitch *with_pictures;
@property (nonatomic, weak) IBOutlet UISwitch *stored;
@property (nonatomic, weak) IBOutlet UISwitch *shima;
@property (nonatomic, weak) IBOutlet UISwitch *stoll;

//composition
@property (nonatomic, weak) IBOutlet BTBorderedButton *compositionButton0;
@property (nonatomic, weak) IBOutlet BTBorderedButton *compositionPercentageButton0;

@property (nonatomic, weak) IBOutlet BTBorderedButton *compositionButton1;
@property (nonatomic, weak) IBOutlet BTBorderedButton *compositionPercentageButton1;

@property (nonatomic, weak) IBOutlet BTBorderedButton *compositionButton2;
@property (nonatomic, weak) IBOutlet BTBorderedButton *compositionPercentageButton2;

//stitch type
@property (nonatomic, weak) IBOutlet BTBorderedButton *stitchTypeButton0;
@property (nonatomic, weak) IBOutlet BTBorderedButton *stitchTypeButton1;
@property (nonatomic, weak) IBOutlet BTBorderedButton *stitchTypeButton2;

@property (weak, nonatomic) IBOutlet BTBorderedButton *yarnButton;
@property (weak, nonatomic) IBOutlet BTBorderedButton *supplierButton;

@property (nonatomic, strong) NSString *stitch_type_key;
@property (nonatomic, strong) StitchType *stitch_type0, *stitch_type1, *stitch_type2;

- (void)loadFiltersData;

- (IBAction)pressedOnButton:(id)sender;
- (IBAction)search:(id)sender;
- (void)applyFilters;

@end
