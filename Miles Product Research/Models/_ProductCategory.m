// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to ProductCategory.m instead.

#import "_ProductCategory.h"

const struct ProductCategoryAttributes ProductCategoryAttributes = {
	.code = @"code",
	.family_code = @"family_code",
	.family_string_code = @"family_string_code",
	.item_description = @"item_description",
	.language = @"language",
	.string_code = @"string_code",
};

const struct ProductCategoryRelationships ProductCategoryRelationships = {
	.family = @"family",
	.products = @"products",
};

const struct ProductCategoryFetchedProperties ProductCategoryFetchedProperties = {
};

@implementation ProductCategoryID
@end

@implementation _ProductCategory

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"ProductCategory" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"ProductCategory";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"ProductCategory" inManagedObjectContext:moc_];
}

- (ProductCategoryID*)objectID {
	return (ProductCategoryID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	
	if ([key isEqualToString:@"codeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"code"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"family_codeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"family_code"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}




@dynamic code;



- (int32_t)codeValue {
	NSNumber *result = [self code];
	return [result intValue];
}

- (void)setCodeValue:(int32_t)value_ {
	[self setCode:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveCodeValue {
	NSNumber *result = [self primitiveCode];
	return [result intValue];
}

- (void)setPrimitiveCodeValue:(int32_t)value_ {
	[self setPrimitiveCode:[NSNumber numberWithInt:value_]];
}





@dynamic family_code;



- (int32_t)family_codeValue {
	NSNumber *result = [self family_code];
	return [result intValue];
}

- (void)setFamily_codeValue:(int32_t)value_ {
	[self setFamily_code:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveFamily_codeValue {
	NSNumber *result = [self primitiveFamily_code];
	return [result intValue];
}

- (void)setPrimitiveFamily_codeValue:(int32_t)value_ {
	[self setPrimitiveFamily_code:[NSNumber numberWithInt:value_]];
}





@dynamic family_string_code;






@dynamic item_description;






@dynamic language;






@dynamic string_code;






@dynamic family;

	

@dynamic products;

	
- (NSMutableSet*)productsSet {
	[self willAccessValueForKey:@"products"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"products"];
  
	[self didAccessValueForKey:@"products"];
	return result;
}
	






@end
