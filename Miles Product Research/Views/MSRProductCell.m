//
//  MSRStitchCell.m
//  MSR
//
//  Created by Davide Cenzi on 21/12/13.
//  Copyright (c) 2013 BlueTouch Development di Davide Cenzi. All rights reserved.
//

#import "MSRProductCell.h"
#import <QuartzCore/QuartzCore.h>

@implementation MSRProductCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (BOOL)isActive{
    return _active;
}
- (void)setActive:(BOOL)active{
    _active = active;
    CALayer *l = self.layer;
    [l setBorderColor:[UIColor colorWithRed:0.282 green:0.725 blue:0.839 alpha:1.000].CGColor];
    [l setMasksToBounds:YES];
    [l setBorderWidth:(_active ? 4.0 : 0.0)];
    [self.activeImage setHidden:!_active];
}

- (IBAction)toggleActive:(id)sender{
    [self setActive:!self.isActive];
    
    if(_delegate && [_delegate respondsToSelector:@selector(toggledSelectionForCode:)])
        [_delegate performSelector:@selector(toggledSelectionForCode:) withObject:self.productCode];
}

- (void)toggledSelectionForCode:(NSNumber*)code{
    
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
