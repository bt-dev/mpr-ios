//
//  NSManagedObject+Additions.m
//  Needle
//
//  Created by Davide Cenzi on 23/08/13.
//  Copyright (c) 2013 BlueTouch Development di Davide Cenzi. All rights reserved.
//

#import "NSManagedObject+Additions.h"
#import <objc/runtime.h>

@implementation NSManagedObject(Additions)


#pragma mark - RKManagedObject methods

+ (NSEntityDescription *)entity
{
    NSString *className = [NSString stringWithCString:class_getName([self class]) encoding:NSASCIIStringEncoding];
    return [NSEntityDescription entityForName:className inManagedObjectContext:[BTDataSync shared].context];
}

+ (NSFetchRequest *)fetchRequest
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [self entity];
    [fetchRequest setEntity:entity];
    return fetchRequest;
}

+ (NSArray *)objectsWithFetchRequest:(NSFetchRequest *)fetchRequest
{
    NSError *error = nil;
    NSArray *objects = [[BTDataSync shared].context executeFetchRequest:fetchRequest error:&error];
    if (objects == nil) {
        RKLogError(@"Error: %@", [error localizedDescription]);
    }
    return objects;
}

+ (NSUInteger)countOfObjectsWithFetchRequest:(NSFetchRequest *)fetchRequest
{
    NSError *error = nil;
    NSUInteger objectCount = [[BTDataSync shared].context countForFetchRequest:fetchRequest error:&error];
    if (objectCount    == NSNotFound) {
        RKLogError(@"Error: %@", [error localizedDescription]);
    }
    return objectCount;
}

+ (NSArray *)objectsWithFetchRequests:(NSArray *)fetchRequests
{
    NSMutableArray *mutableObjectArray = [[NSMutableArray alloc] init];
    for (NSFetchRequest *fetchRequest in fetchRequests) {
        [mutableObjectArray addObjectsFromArray:[self objectsWithFetchRequest:fetchRequest]];
    }
    
    return [NSArray arrayWithArray:mutableObjectArray];
}

+ (id)objectWithFetchRequest:(NSFetchRequest *)fetchRequest
{
    [fetchRequest setFetchLimit:1];
    NSArray *objects = [self objectsWithFetchRequest:fetchRequest];
    if ([objects count] == 0) {
        return nil;
    } else {
        return [objects objectAtIndex:0];
    }
}

+ (NSArray *)objectsWithPredicate:(NSPredicate *)predicate
{
    NSFetchRequest *fetchRequest = [self fetchRequest];
    [fetchRequest setPredicate:predicate];
    return [self objectsWithFetchRequest:fetchRequest];
}

+ (id)objectWithPredicate:(NSPredicate *)predicate
{
    NSFetchRequest *fetchRequest = [self fetchRequest];
    [fetchRequest setPredicate:predicate];
    return [self objectWithFetchRequest:fetchRequest];
}

+ (NSArray *)allObjects
{
    return [self objectsWithPredicate:nil];
}

+ (NSUInteger)count:(NSError **)error
{
    NSFetchRequest *fetchRequest = [self fetchRequest];
    return [[BTDataSync shared].context countForFetchRequest:fetchRequest error:error];
}

+ (NSUInteger)count
{
    NSError *error = nil;
    return [self count:&error];
}

+ (id)object
{
    id object = [[self alloc] initWithEntity:[self entity] insertIntoManagedObjectContext:[BTDataSync shared].context];
    return object;
}


/*
+ (NSFetchRequest*)fetchRequest{
    
    NSManagedObject *c = [NSManagedObject object];
    NSString *name = [[c entity] name];
    return [NSFetchRequest fetchRequestWithEntityName:name];
}

+ (id)objectWithFetchRequest:(NSFetchRequest*)fetchRequest{
    
    NSArray * objects = [NSManagedObject objectsWithFetchRequest:fetchRequest];
    
    if(objects != nil && [objects count] > 0)
        return [objects objectAtIndex:0];
    else
        return nil;
}

+ (NSArray*)objectsWithFetchRequest:(NSFetchRequest*)fetchRequest{
    
    NSError *err = nil;
    NSArray *objects =[[BTDataSync shared].context executeFetchRequest:fetchRequest error:&err];
    
    return objects;
}

+ (id)object{
    BTDataSync *sync = [BTDataSync shared];
    
    if(sync.context == nil){
        NSLog(@"NO CONTEXT SET UP FOR BTDataSync, cannot access NSManagedObject Additions");
        return nil;
    }
    return [sync.dataModel create:[self class]];
}
*/
- (BOOL)saved{
    NSNumber *code = [self valueForKey:@"code"];
    return (code != nil && [code intValue] > 0);
}

@end
