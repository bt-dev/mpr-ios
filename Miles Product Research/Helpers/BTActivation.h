//
//  BTActivation.h
//  Geoplast
//
//  Created by Davide Cenzi on 19/08/13.
//
//

#import <Foundation/Foundation.h>
#import "Reachability.h"
#import <AFNetworking/AFNetworking.h>
#import <AFNetworking/AFHTTPClient.h>


@interface BTActivation : NSObject

+ (NSString*)appBundleIdentifier;
+ (NSString*)appVersion;
+ (NSString*)appBuildNr;
+ (NSDate*)appBuildDate;

+ (int)elapsedDays;
+ (BOOL)timeoutElapsed;
+ (void)checkActivation;
+ (BOOL)appActivated;
+ (NSString*)remoteActivationUrl;

@end
