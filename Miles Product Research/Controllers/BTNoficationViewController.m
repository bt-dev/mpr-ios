//
//  BTNoficationViewController.m
//  Needle
//
//  Created by Davide Cenzi on 01/08/13.
//  Copyright (c) 2013 BlueTouch Development di Davide Cenzi. All rights reserved.
//

#import "BTNoficationViewController.h"

@interface BTNoficationViewController ()

@end

@implementation BTNoficationViewController
@synthesize dataSync = _dataSync;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

/* UTILITY METHODS */


- (void)showBannerWithText:(NSString*)text andDuration:(NSTimeInterval)duration{
    
    [AJNotificationView showNoticeInView:self.navigationController.topViewController.view
                                    type:AJNotificationTypeBlue
                                   title:text
                         linedBackground:AJLinedBackgroundTypeAnimated
                               hideAfter:duration];
    
}


- (void)startListeningForNotifications{
    
    for (NSString *message in self.notificationNames) {
        
        LogTrace(@"Registering view controller for '%@' notifications",message);
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(handleNotification:)
                                                     name:message
                                                   object:nil];
    }
    
}

- (void)stopListeningForNotifications{
    
    for (NSString *message in self.notificationNames) {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:message object:nil];
    }
    
}

//Override these methods 
- (NSArray*)notificationNames{
    return [[NSArray alloc] initWithObjects:@"GENERIC_MESSAGE",@"REQUEST_STARTED",@"ERROR_MESSAGE", @"CONNECTED", @"NOT_CONNECTED",@"CHANGING_LANGUAGE",@"CHANGED_LANGUAGE", nil];
}

- (void)handleNotification:(NSNotification *)notification{
    
    if([notification.name isEqualToString:@"REQUEST_STARTED"]){
        [SVProgressHUD show];
    }
    else if([notification.name isEqualToString:@"CHANGING_LANGUAGE"]){
        
        if(self.presentedViewController != nil && self.presentedViewController != self)
            [self dismissViewControllerAnimated:NO completion:nil];
        
        if(self.navigationController)
            [self.navigationController popToRootViewControllerAnimated:NO];
        
        [SVProgressHUD showWithStatus:@"Changing language and updating database..." maskType:SVProgressHUDMaskTypeGradient];
    }
    else{
        //automatically hides the spinner
        [SVProgressHUD dismiss];
    }
    
    NSString *_status = [NSString stringWithFormat:@"Request '%@' %@",notification.name, (([notification.name rangeOfString:@"SUCCESS"].location != NSNotFound) ? @"succeded": @"failed")];
    
    NSLog(@"[%@]Received message: %@",NSStringFromClass(self.class), _status);
    
    if([notification.name rangeOfString:@"SUCCESS"].location != NSNotFound){
        [AJNotificationView showNoticeInView:self.navigationController.topViewController.view
                                        type:AJNotificationTypeBlue
                                       title:@"Synchronization completed"
                             linedBackground:AJLinedBackgroundTypeAnimated
                                   hideAfter:2.5f];
    }
    else if([notification.name rangeOfString:@"FAILURE"].location != NSNotFound){
        [AJNotificationView showNoticeInView:self.navigationController.topViewController.view
                                        type:AJNotificationTypeRed
                                       title:@"Synchronization failed"
                             linedBackground:AJLinedBackgroundTypeAnimated
                                   hideAfter:2.5f];
        
    }
    
}

// ---

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    _dataSync = [BTDataSync shared];
    [self startListeningForNotifications];
}

- (void)viewWillUnload{
    [self stopListeningForNotifications];
    [super viewWillUnload];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
