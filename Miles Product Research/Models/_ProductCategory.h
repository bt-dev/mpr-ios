// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to ProductCategory.h instead.

#import <CoreData/CoreData.h>


extern const struct ProductCategoryAttributes {
	__unsafe_unretained NSString *code;
	__unsafe_unretained NSString *family_code;
	__unsafe_unretained NSString *family_string_code;
	__unsafe_unretained NSString *item_description;
	__unsafe_unretained NSString *language;
	__unsafe_unretained NSString *string_code;
} ProductCategoryAttributes;

extern const struct ProductCategoryRelationships {
	__unsafe_unretained NSString *family;
	__unsafe_unretained NSString *products;
} ProductCategoryRelationships;

extern const struct ProductCategoryFetchedProperties {
} ProductCategoryFetchedProperties;

@class Family;
@class Product;








@interface ProductCategoryID : NSManagedObjectID {}
@end

@interface _ProductCategory : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (ProductCategoryID*)objectID;





@property (nonatomic, strong) NSNumber* code;



@property int32_t codeValue;
- (int32_t)codeValue;
- (void)setCodeValue:(int32_t)value_;

//- (BOOL)validateCode:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* family_code;



@property int32_t family_codeValue;
- (int32_t)family_codeValue;
- (void)setFamily_codeValue:(int32_t)value_;

//- (BOOL)validateFamily_code:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* family_string_code;



//- (BOOL)validateFamily_string_code:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* item_description;



//- (BOOL)validateItem_description:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* language;



//- (BOOL)validateLanguage:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* string_code;



//- (BOOL)validateString_code:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) Family *family;

//- (BOOL)validateFamily:(id*)value_ error:(NSError**)error_;




@property (nonatomic, strong) NSSet *products;

- (NSMutableSet*)productsSet;





@end

@interface _ProductCategory (CoreDataGeneratedAccessors)

- (void)addProducts:(NSSet*)value_;
- (void)removeProducts:(NSSet*)value_;
- (void)addProductsObject:(Product*)value_;
- (void)removeProductsObject:(Product*)value_;

@end

@interface _ProductCategory (CoreDataGeneratedPrimitiveAccessors)


- (NSNumber*)primitiveCode;
- (void)setPrimitiveCode:(NSNumber*)value;

- (int32_t)primitiveCodeValue;
- (void)setPrimitiveCodeValue:(int32_t)value_;




- (NSNumber*)primitiveFamily_code;
- (void)setPrimitiveFamily_code:(NSNumber*)value;

- (int32_t)primitiveFamily_codeValue;
- (void)setPrimitiveFamily_codeValue:(int32_t)value_;




- (NSString*)primitiveFamily_string_code;
- (void)setPrimitiveFamily_string_code:(NSString*)value;




- (NSString*)primitiveItem_description;
- (void)setPrimitiveItem_description:(NSString*)value;




- (NSString*)primitiveLanguage;
- (void)setPrimitiveLanguage:(NSString*)value;




- (NSString*)primitiveString_code;
- (void)setPrimitiveString_code:(NSString*)value;





- (Family*)primitiveFamily;
- (void)setPrimitiveFamily:(Family*)value;



- (NSMutableSet*)primitiveProducts;
- (void)setPrimitiveProducts:(NSMutableSet*)value;


@end
