//
//  BTBorderedField.h
//  Miles Product Research
//
//  Created by Davide Cenzi on 29/06/14.
//  Copyright (c) 2014 BlueTouch Development di Davide Cenzi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface BTBorderedField : UITextField
- (void)setBorderColor:(UIColor*)color;
@end
