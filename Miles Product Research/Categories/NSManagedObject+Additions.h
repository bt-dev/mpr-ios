//
//  NSManagedObject+Additions.h
//  Needle
//
//  Created by Davide Cenzi on 23/08/13.
//  Copyright (c) 2013 BlueTouch Development di Davide Cenzi. All rights reserved.
//

#import <CoreData/CoreData.h>
//#import "BTDataSync.h"

@interface NSManagedObject (Additions)
/*
+ (NSFetchRequest*)fetchRequest;
+ (id)objectWithFetchRequest:(NSFetchRequest*)fetchRequest;
+ (NSArray*)objectsWithFetchRequest:(NSFetchRequest*)fetchRequest;
+ (id)object;
 */

+ (NSEntityDescription *)entity;
+ (NSFetchRequest *)fetchRequest;
+ (NSArray *)objectsWithFetchRequest:(NSFetchRequest *)fetchRequest;
+ (NSUInteger)countOfObjectsWithFetchRequest:(NSFetchRequest *)fetchRequest;
+ (NSArray *)objectsWithFetchRequests:(NSArray *)fetchRequests;
+ (id)objectWithFetchRequest:(NSFetchRequest *)fetchRequest;
+ (NSArray *)objectsWithPredicate:(NSPredicate *)predicate;
+ (id)objectWithPredicate:(NSPredicate *)predicate;
+ (NSArray *)allObjects;
+ (NSUInteger)count:(NSError **)error;
+ (NSUInteger)count;
+ (id)object;






- (BOOL)saved;

@end
