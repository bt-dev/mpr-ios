//
//  NMHelpers.h
//  Needle
//
//  Created by Davide Cenzi on 01/08/13.
//  Copyright (c) 2013 BlueTouch Development di Davide Cenzi. All rights reserved.
//

#import "BTHelpers.h"

@interface NMHelpers : BTHelpers

+ (CGSize)effectiveSizeForText:(NSString*)text withFont:(UIFont*)font contrainedToSize:(CGSize)size;
+ (id)flatButtonWithText:(NSString*)text ForTarget:(id)target andAction:(SEL)action;
+ (id)iOS7ButtonWithImage:(UIImage*)image ForTarget:(id)target andAction:(SEL)action;

@end
