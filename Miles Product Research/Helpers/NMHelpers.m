//
//  NMHelpers.m
//  Needle
//
//  Created by Davide Cenzi on 01/08/13.
//  Copyright (c) 2013 BlueTouch Development di Davide Cenzi. All rights reserved.
//

#import "NMHelpers.h"

@implementation NMHelpers


+ (CGSize)effectiveSizeForText:(NSString*)text withFont:(UIFont*)font contrainedToSize:(CGSize)size{
    
    /* replacement method for iOS7 */
    NSAttributedString *attributedText = [[NSAttributedString alloc] initWithString:text
                                                                         attributes:@{NSFontAttributeName: font}];
    
    CGRect rect = [attributedText boundingRectWithSize:(CGSize){size.width, CGFLOAT_MAX}
                                               options:NSStringDrawingUsesLineFragmentOrigin
                                               context:nil];
    
    return rect.size;
    
}

//CUSTOM BACK BUTTON HELPER
+ (id)flatButtonWithText:(NSString*)text ForTarget:(id)target andAction:(SEL)action{
    
    NSMutableArray *arrayBack = [[NSMutableArray alloc] init];
    
    if(text == nil) text = @"  <";
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle:text style:UIBarButtonItemStylePlain target:target action:action];
    
    [backButton setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                        [UIColor whiteColor],
                                        NSForegroundColorAttributeName,
                                        [UIFont fontWithName:@"Helvetica-Bold" size:15.0],
                                        NSFontAttributeName,
                                        nil] forState:UIControlStateNormal];
    
    [arrayBack addObject:backButton];
    
    UIToolbar *toolsBack = [[UIToolbar alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 54.01f, 44.01f)];
    // 44.01 shifts it up 1px for some reason
    
    toolsBack.clearsContextBeforeDrawing = NO;
    toolsBack.clipsToBounds = NO;
    toolsBack.barStyle = -1; // clear background
    [toolsBack setItems:arrayBack animated:YES];
    
    return [[UIBarButtonItem alloc] initWithCustomView:toolsBack];
}

+ (id)iOS7ButtonWithImage:(UIImage*)image ForTarget:(id)target andAction:(SEL)action{
    
    UIButton *button =  [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:image forState:UIControlStateNormal];
    [button addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    [button setFrame:CGRectMake(0, 0, 53, 31)];
    
    
    return  [[UIBarButtonItem alloc] initWithCustomView:button];
    

}



@end
