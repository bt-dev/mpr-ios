// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to MacroSeason.m instead.

#import "_MacroSeason.h"

const struct MacroSeasonAttributes MacroSeasonAttributes = {
	.code = @"code",
	.item_description = @"item_description",
	.language = @"language",
	.string_code = @"string_code",
};

const struct MacroSeasonRelationships MacroSeasonRelationships = {
	.products = @"products",
};

const struct MacroSeasonFetchedProperties MacroSeasonFetchedProperties = {
};

@implementation MacroSeasonID
@end

@implementation _MacroSeason

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"MacroSeason" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"MacroSeason";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"MacroSeason" inManagedObjectContext:moc_];
}

- (MacroSeasonID*)objectID {
	return (MacroSeasonID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	
	if ([key isEqualToString:@"codeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"code"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}




@dynamic code;



- (int32_t)codeValue {
	NSNumber *result = [self code];
	return [result intValue];
}

- (void)setCodeValue:(int32_t)value_ {
	[self setCode:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveCodeValue {
	NSNumber *result = [self primitiveCode];
	return [result intValue];
}

- (void)setPrimitiveCodeValue:(int32_t)value_ {
	[self setPrimitiveCode:[NSNumber numberWithInt:value_]];
}





@dynamic item_description;






@dynamic language;






@dynamic string_code;






@dynamic products;

	
- (NSMutableSet*)productsSet {
	[self willAccessValueForKey:@"products"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"products"];
  
	[self didAccessValueForKey:@"products"];
	return result;
}
	






@end
