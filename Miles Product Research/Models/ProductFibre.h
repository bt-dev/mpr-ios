#import "_ProductFibre.h"

@interface ProductFibre : _ProductFibre {}
// Custom logic goes here.

+ (float)floatPercentageFromString:(NSString*)percentageString;
@end
