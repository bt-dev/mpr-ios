//
//  SliderView.m
//  Miles Product Research
//
//  Created by Davide Cenzi on 06/07/14.
//  Copyright (c) 2014 BlueTouch Development di Davide Cenzi. All rights reserved.
//

#import "SliderView.h"

@implementation SliderView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (BOOL)isActive{
    return _active;
}
- (void)setActive:(BOOL)active{
    _active = active;
    CALayer *l = self.layer;
    [l setBorderColor:[UIColor colorWithRed:0.282 green:0.725 blue:0.839 alpha:1.000].CGColor];
    [l setMasksToBounds:YES];
    [l setBorderWidth:(_active ? 2.0 : 0.0)];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
