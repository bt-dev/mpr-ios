#import "_Product.h"
#import "Yarn.h"
#import "StitchType.h"
#import "S2SOrderedDictionary.h"

@interface Product : _Product {}
// Custom logic goes here.

- (int)cleanedYear;
- (NSArray*)yarns;
- (Yarn*)yarn;
- (NSArray*)yarnCodes;

- (Supplier*)supplier;
- (NSArray*)suppliers;
- (NSArray*)supplierCodes;

- (StitchType *)stitchType;
- (NSArray*)stitchTypes;
- (NSArray*)stitchTypeCodes;


- (BOOL)hasGenericPictureFile;
- (BOOL)hasDetailPictureFile;

- (Picture*)genericPicture;
- (NSArray*)sortedPictures;

- (int)defaultPictureIndex;
- (NSURL*)defaultPictureURL;
- (NSURL*)thumbURL;
- (NSString*)defaultPicturePath;

- (NSString*)genericPicturePath;
- (NSURL*)genericPictureURL;

//+ (S2SOrderedDictionary*)filterValues;

@end
