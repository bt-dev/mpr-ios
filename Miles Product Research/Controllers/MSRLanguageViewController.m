//
//  MSRLanguageViewController.m
//  MSR
//
//  Created by Davide Cenzi on 07/01/14.
//  Copyright (c) 2014 BlueTouch Development di Davide Cenzi. All rights reserved.
//

#import "MSRLanguageViewController.h"

@interface MSRLanguageViewController ()

@end

@implementation MSRLanguageViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (IBAction)choosedLanguage:(id)sender{
    UIButton *b = sender;
    NSString *lang = @"en";
    
    switch (b.tag) {
        case 0:
            lang = @"it";
            //italian
            break;
        case 1:
            lang = @"en";
            //english
            break;
        case 2:
            lang = @"fr";
            //francais
            break;
            
        default:
            break;
    }
    
    if([[[BTLanguage shared] currentLanguage] isEqualToString:lang])
        NSLog(@"Same language");
    else
        [[BTLanguage shared] changeLanguageTo:lang withReboot:YES];
    
    if(self.popover)
        [self.popover dismissPopoverAnimated:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
