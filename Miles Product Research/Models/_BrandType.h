// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to BrandType.h instead.

#import <CoreData/CoreData.h>


extern const struct BrandTypeAttributes {
	__unsafe_unretained NSString *code;
	__unsafe_unretained NSString *item_description;
	__unsafe_unretained NSString *language;
	__unsafe_unretained NSString *string_code;
} BrandTypeAttributes;

extern const struct BrandTypeRelationships {
	__unsafe_unretained NSString *products;
} BrandTypeRelationships;

extern const struct BrandTypeFetchedProperties {
} BrandTypeFetchedProperties;

@class Product;






@interface BrandTypeID : NSManagedObjectID {}
@end

@interface _BrandType : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (BrandTypeID*)objectID;





@property (nonatomic, strong) NSNumber* code;



@property int32_t codeValue;
- (int32_t)codeValue;
- (void)setCodeValue:(int32_t)value_;

//- (BOOL)validateCode:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* item_description;



//- (BOOL)validateItem_description:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* language;



//- (BOOL)validateLanguage:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* string_code;



//- (BOOL)validateString_code:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSSet *products;

- (NSMutableSet*)productsSet;





@end

@interface _BrandType (CoreDataGeneratedAccessors)

- (void)addProducts:(NSSet*)value_;
- (void)removeProducts:(NSSet*)value_;
- (void)addProductsObject:(Product*)value_;
- (void)removeProductsObject:(Product*)value_;

@end

@interface _BrandType (CoreDataGeneratedPrimitiveAccessors)


- (NSNumber*)primitiveCode;
- (void)setPrimitiveCode:(NSNumber*)value;

- (int32_t)primitiveCodeValue;
- (void)setPrimitiveCodeValue:(int32_t)value_;




- (NSString*)primitiveItem_description;
- (void)setPrimitiveItem_description:(NSString*)value;




- (NSString*)primitiveLanguage;
- (void)setPrimitiveLanguage:(NSString*)value;




- (NSString*)primitiveString_code;
- (void)setPrimitiveString_code:(NSString*)value;





- (NSMutableSet*)primitiveProducts;
- (void)setPrimitiveProducts:(NSMutableSet*)value;


@end
