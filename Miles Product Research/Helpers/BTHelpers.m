//
//  NMVarious.m
//  Needle
//
//  Created by Davide Cenzi on 01/08/13.
//  Copyright (c) 2013 BlueTouch Development di Davide Cenzi. All rights reserved.
//

#import "BTHelpers.h"

@implementation BTHelpers

+ (BOOL)hasFourInchDisplay {
    return ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone && [UIScreen mainScreen].bounds.size.height == 568.0);
}

//metodo 1
+ (BOOL)hasBash{
	BOOL jailbroken = NO;
    FILE *f;
	f = fopen("/bin/bash","r");
	jailbroken = (f != NULL);
	fclose(f);
	return jailbroken;
	
}

//metodo 2
+ (BOOL)hasCydia{
	NSURL *url = [NSURL URLWithString:@"cydia://package/com.bluetouchdev.Needle"];
	return [[UIApplication sharedApplication] canOpenURL:url];
}

+ (BOOL)jailbrokenDevice{
    return ([self hasBash] || [self hasCydia]);
}

+ (NSString*)appVersion{
    return [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
}

+ (NSString*)appBuildNr{
    return [[NSBundle mainBundle] objectForInfoDictionaryKey:(NSString*)kCFBundleVersionKey];
}

+ (UIAlertView *)alertText:(NSString*)text withTitle:(NSString*)title{
    UIAlertView *al = [[UIAlertView alloc] initWithTitle:title message:text delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
    return al;
    
}


//Image extensions

+ (UIImage *)convertImageToGrayScale:(UIImage *)image
{
    // Create image rectangle with current image width/height
    CGRect imageRect = CGRectMake(0, 0, image.size.width, image.size.height);
    
    // Grayscale color space
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceGray();
    
    // Create bitmap content with current image size and grayscale colorspace
    CGContextRef context = CGBitmapContextCreate(nil, image.size.width, image.size.height, 8, 0, colorSpace, kCGImageAlphaNone);
    
    // Draw image into current context, with specified rectangle
    // using previously defined context (with grayscale colorspace)
    CGContextDrawImage(context, imageRect, [image CGImage]);
    
    // Create bitmap image info from pixel data in current context
    CGImageRef imageRef = CGBitmapContextCreateImage(context);
    
    // Create a new UIImage object
    UIImage *newImage = [UIImage imageWithCGImage:imageRef];
    
    // Release colorspace, context and bitmap information
    CGColorSpaceRelease(colorSpace);
    CGContextRelease(context);
    CFRelease(imageRef);
    
    // Return the new grayscale image
    return newImage;
}

//Validators

+ (BOOL)validEmail:(NSString*)email{
    if(email == nil || [email isEqualToString:@""] || [email length] < 8)
        return NO;
    else{
        BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
        NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
        NSString *laxString = @".+@([A-Za-z0-9]+\\.)+[A-Za-z]{2}[A-Za-z]*";
        NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
        NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
        return [emailTest evaluateWithObject:email];
    }
}

+ (BOOL)validString:(NSString*)string withMinLength:(int)min andMaxLength:(int)max{
    if(min < 1)
        return NO;
    
    if(max < min)
        max = 1000000;
    
    return (min <= [string length] <= max);
}

+ (BOOL)numericString:(NSString *)string{
    
    BOOL valid;
    NSCharacterSet *alphaNums = [NSCharacterSet decimalDigitCharacterSet];
    NSCharacterSet *inStringSet = [NSCharacterSet characterSetWithCharactersInString:string];
    valid = [alphaNums isSupersetOfSet:inStringSet];
    
    return valid;
}




@end
