#import "ProductFibre.h"


@interface ProductFibre ()

// Private interface goes here.

@end


@implementation ProductFibre

// Custom logic goes here.
+ (float)floatPercentageFromString:(NSString*)percentageString{
    if (percentageString)
        return [[[percentageString stringByReplacingOccurrencesOfString:@"composition_percentage." withString:@""] stringByReplacingOccurrencesOfString:@"_percent" withString:@""] floatValue];
    else
        return 0.0;
}

@end
