//
//  NSString+UrlAddictions.m
//  Needle
//
//  Created by Davide Cenzi on 23/08/13.
//  Copyright (c) 2013 BlueTouch Development di Davide Cenzi. All rights reserved.
//

#import "NSString+UrlAddictions.h"

@implementation NSString (UrlAddictions)


-(NSString *)urlEncodedString {
    
    return (__bridge_transfer NSString *)CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault, (__bridge CFStringRef)self, NULL, CFSTR(":/?#[]@!$&’() '*+,;="), kCFStringEncodingUTF8);

}

-(NSString *)urlDecodedString {
    return [self stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
}

@end