// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to ProductFibre.h instead.

#import <CoreData/CoreData.h>


extern const struct ProductFibreAttributes {
	__unsafe_unretained NSString *code;
	__unsafe_unretained NSString *fibre_code;
	__unsafe_unretained NSString *language;
	__unsafe_unretained NSString *percentage;
	__unsafe_unretained NSString *product_code;
} ProductFibreAttributes;

extern const struct ProductFibreRelationships {
	__unsafe_unretained NSString *fibre;
	__unsafe_unretained NSString *product;
} ProductFibreRelationships;

extern const struct ProductFibreFetchedProperties {
} ProductFibreFetchedProperties;

@class Fibre;
@class Product;







@interface ProductFibreID : NSManagedObjectID {}
@end

@interface _ProductFibre : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (ProductFibreID*)objectID;





@property (nonatomic, strong) NSNumber* code;



@property int32_t codeValue;
- (int32_t)codeValue;
- (void)setCodeValue:(int32_t)value_;

//- (BOOL)validateCode:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* fibre_code;



@property int32_t fibre_codeValue;
- (int32_t)fibre_codeValue;
- (void)setFibre_codeValue:(int32_t)value_;

//- (BOOL)validateFibre_code:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* language;



//- (BOOL)validateLanguage:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* percentage;



@property float percentageValue;
- (float)percentageValue;
- (void)setPercentageValue:(float)value_;

//- (BOOL)validatePercentage:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* product_code;



@property int32_t product_codeValue;
- (int32_t)product_codeValue;
- (void)setProduct_codeValue:(int32_t)value_;

//- (BOOL)validateProduct_code:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) Fibre *fibre;

//- (BOOL)validateFibre:(id*)value_ error:(NSError**)error_;




@property (nonatomic, strong) Product *product;

//- (BOOL)validateProduct:(id*)value_ error:(NSError**)error_;





@end

@interface _ProductFibre (CoreDataGeneratedAccessors)

@end

@interface _ProductFibre (CoreDataGeneratedPrimitiveAccessors)


- (NSNumber*)primitiveCode;
- (void)setPrimitiveCode:(NSNumber*)value;

- (int32_t)primitiveCodeValue;
- (void)setPrimitiveCodeValue:(int32_t)value_;




- (NSNumber*)primitiveFibre_code;
- (void)setPrimitiveFibre_code:(NSNumber*)value;

- (int32_t)primitiveFibre_codeValue;
- (void)setPrimitiveFibre_codeValue:(int32_t)value_;




- (NSString*)primitiveLanguage;
- (void)setPrimitiveLanguage:(NSString*)value;




- (NSNumber*)primitivePercentage;
- (void)setPrimitivePercentage:(NSNumber*)value;

- (float)primitivePercentageValue;
- (void)setPrimitivePercentageValue:(float)value_;




- (NSNumber*)primitiveProduct_code;
- (void)setPrimitiveProduct_code:(NSNumber*)value;

- (int32_t)primitiveProduct_codeValue;
- (void)setPrimitiveProduct_codeValue:(int32_t)value_;





- (Fibre*)primitiveFibre;
- (void)setPrimitiveFibre:(Fibre*)value;



- (Product*)primitiveProduct;
- (void)setPrimitiveProduct:(Product*)value;


@end
