// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Supplier.h instead.

#import <CoreData/CoreData.h>


extern const struct SupplierAttributes {
	__unsafe_unretained NSString *code;
	__unsafe_unretained NSString *item_description;
	__unsafe_unretained NSString *language;
	__unsafe_unretained NSString *string_code;
} SupplierAttributes;

extern const struct SupplierRelationships {
	__unsafe_unretained NSString *yarns;
} SupplierRelationships;

extern const struct SupplierFetchedProperties {
} SupplierFetchedProperties;

@class Yarn;






@interface SupplierID : NSManagedObjectID {}
@end

@interface _Supplier : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (SupplierID*)objectID;





@property (nonatomic, strong) NSNumber* code;



@property int32_t codeValue;
- (int32_t)codeValue;
- (void)setCodeValue:(int32_t)value_;

//- (BOOL)validateCode:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* item_description;



//- (BOOL)validateItem_description:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* language;



//- (BOOL)validateLanguage:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* string_code;



//- (BOOL)validateString_code:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSSet *yarns;

- (NSMutableSet*)yarnsSet;





@end

@interface _Supplier (CoreDataGeneratedAccessors)

- (void)addYarns:(NSSet*)value_;
- (void)removeYarns:(NSSet*)value_;
- (void)addYarnsObject:(Yarn*)value_;
- (void)removeYarnsObject:(Yarn*)value_;

@end

@interface _Supplier (CoreDataGeneratedPrimitiveAccessors)


- (NSNumber*)primitiveCode;
- (void)setPrimitiveCode:(NSNumber*)value;

- (int32_t)primitiveCodeValue;
- (void)setPrimitiveCodeValue:(int32_t)value_;




- (NSString*)primitiveItem_description;
- (void)setPrimitiveItem_description:(NSString*)value;




- (NSString*)primitiveLanguage;
- (void)setPrimitiveLanguage:(NSString*)value;




- (NSString*)primitiveString_code;
- (void)setPrimitiveString_code:(NSString*)value;





- (NSMutableSet*)primitiveYarns;
- (void)setPrimitiveYarns:(NSMutableSet*)value;


@end
