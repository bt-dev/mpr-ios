// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Yarn.m instead.

#import "_Yarn.h"

const struct YarnAttributes YarnAttributes = {
	.code = @"code",
	.item_description = @"item_description",
	.string_code = @"string_code",
	.supplier_code = @"supplier_code",
	.supplier_string_code = @"supplier_string_code",
};

const struct YarnRelationships YarnRelationships = {
	.supplier = @"supplier",
};

const struct YarnFetchedProperties YarnFetchedProperties = {
};

@implementation YarnID
@end

@implementation _Yarn

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Yarn" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Yarn";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Yarn" inManagedObjectContext:moc_];
}

- (YarnID*)objectID {
	return (YarnID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	
	if ([key isEqualToString:@"codeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"code"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}




@dynamic code;



- (int32_t)codeValue {
	NSNumber *result = [self code];
	return [result intValue];
}

- (void)setCodeValue:(int32_t)value_ {
	[self setCode:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveCodeValue {
	NSNumber *result = [self primitiveCode];
	return [result intValue];
}

- (void)setPrimitiveCodeValue:(int32_t)value_ {
	[self setPrimitiveCode:[NSNumber numberWithInt:value_]];
}





@dynamic item_description;






@dynamic string_code;






@dynamic supplier_code;






@dynamic supplier_string_code;






@dynamic supplier;

	






@end
