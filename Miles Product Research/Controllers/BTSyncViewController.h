//
//  BTSyncViewController.h
//  NeedleIOS
//
//  Created by Davide Cenzi on 11/07/13.
//  Copyright (c) 2013 Davide Cenzi. All rights reserved.
//

#import "BTNoficationViewController.h"

@interface BTSyncViewController : BTNoficationViewController{

}

- (NSString*)remotePath;
- (NSString*)modelName;
- (void)refreshData;


@end
