//
//  BTSeeder.m
//  MobileCatalogTemplate
//
//  Created by Davide Cenzi on 22/06/13.
//  Copyright (c) 2013 BlueTouch Development di Davide Cenzi. All rights reserved.
//

#import "BTSeeder.h"

@implementation BTSeeder


- (void)importDatabaseFromFile:(NSString*)filename onComplete:(BTSeederOnCompleteBlock)onComplete{
    
    NSString *homeDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *importDB = [[NSBundle mainBundle] pathForResource:filename ofType:@"sqlite"];
    NSString *destDB = [homeDir stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.sqlite",filename]];
    NSError *err = nil;
    
    //import the pre-compiled database
    NSNumber * imported_version = [[NSUserDefaults standardUserDefaults] objectForKey:@"imported_database"];
    
    NSString *versionNrString = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    NSString *compileNrString = [[NSBundle mainBundle] objectForInfoDictionaryKey:(NSString*)kCFBundleVersionKey];
    NSLog(@"\n\nCurrent application version: %@.%@\n\n",versionNrString,compileNrString);
    
    NSNumber *current_version = [NSNumber numberWithFloat:[[versionNrString stringByReplacingOccurrencesOfString:@"." withString:@""] intValue]];
    
	NSLog(@"%@",[NSString stringWithFormat:@"%d - %d", [imported_version intValue], [current_version intValue]]);
    
    if(!imported_version || [imported_version intValue] < [current_version intValue]){

        //just overwrite database file
        if([[NSFileManager defaultManager] fileExistsAtPath:destDB])
            [[NSFileManager defaultManager] removeItemAtPath:destDB error:nil];
        
        NSLog(@"Importing seeding database");
        
        if(![[NSFileManager defaultManager] copyItemAtPath:importDB toPath:destDB error:&err]){
            NSLog(@"Error importing seeding database: %@",[err localizedDescription]);
        }
        else{
            [[NSUserDefaults standardUserDefaults] setObject:current_version forKey:@"imported_database"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"SEEDING_COMPLETED" object:nil];
        }
        

        
    }
    
    if(onComplete)
        onComplete(err);


}

- (void)seedDatabaseFromFile:(NSString*)filename onComplete:(BTSeederOnCompleteBlock)onComplete{
    
    //NSString *homeDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *importDB = [[NSBundle mainBundle] pathForResource:filename ofType:@"sqlite"];
    NSError *err = nil;
    
    //import the pre-compiled database
    NSNumber * imported_version = [[NSUserDefaults standardUserDefaults] objectForKey:@"imported_database"];
    
    NSString *versionNrString = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    NSString *compileNrString = [[NSBundle mainBundle] objectForInfoDictionaryKey:(NSString*)kCFBundleVersionKey];
    NSLog(@"\n\nCurrent application version: %@.%@\n\n",versionNrString,compileNrString);
    
    NSNumber *current_version = [NSNumber numberWithFloat:[[versionNrString stringByReplacingOccurrencesOfString:@"." withString:@""] intValue]];
    
	NSLog(@"%@",[NSString stringWithFormat:@"%d - %d", [imported_version intValue], [current_version intValue]]);
    
    if(!imported_version || [imported_version intValue] < [current_version intValue]){
        
        NSManagedObjectModel *model = [BTDataSync shared].manager.managedObjectStore.managedObjectModel;
        
        RKManagedObjectImporter *importer = [[RKManagedObjectImporter alloc] initWithManagedObjectModel:model storePath:importDB];
        
        [importer importObjectsFromItemAtPath:[[NSBundle mainBundle] pathForResource:@"restkit" ofType:@"json"]
                                  withMapping: [[BTDataSync shared].manager.responseDescriptors lastObject]
                                      keyPath:nil
                                        error:&err];
        
        
        BOOL success = [importer finishImporting:&err];
        if (success) {
            [importer logSeedingInfo];
        } else {
            RKLogError(@"Failed to finish import and save seed database due to error: %@", err);
        }
 
    }
    
    if(onComplete)
        onComplete(err);
    
}

@end
