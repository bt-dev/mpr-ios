//
//  BTBorderedField.m
//  Miles Product Research
//
//  Created by Davide Cenzi on 29/06/14.
//  Copyright (c) 2014 BlueTouch Development di Davide Cenzi. All rights reserved.
//

#import "BTBorderedField.h"

@implementation BTBorderedField{
    UIColor *_borderColor;
}

- (void)awakeFromNib{
    if(!_borderColor)
        _borderColor = [UIColor whiteColor];
    
    [self setBorderColor:_borderColor];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setBorderColor:(UIColor*)color{
    
    self.tintColor = color;
    [self setTextColor:color];
    
    CALayer *l = self.layer;
    [l setBorderWidth:2.0];
    [l setBorderColor:color.CGColor];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
