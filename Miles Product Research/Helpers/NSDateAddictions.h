//
//  NSDateAddictions.h
//  NeedleIOS
//
//  Created by Davide Cenzi on 06/06/13.
//  Copyright (c) 2013 Davide Cenzi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (WebFormattedDate)

- (NSString*)formattedDate;

@end