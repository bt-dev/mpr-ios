//
//  MSRItemsViewController.h
//  MSR
//
//  Created by Davide Cenzi on 13/12/13.
//  Copyright (c) 2013 BlueTouch Development di Davide Cenzi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ECSlidingViewController.h"
#import "BTCollapsableCollectionViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "FiltersView.h"

@interface MSRItemsViewController : BTCollapsableCollectionViewController <ECSlidingViewControllerDelegate, UIAlertViewDelegate>{
    NSDictionary *_filters;
}

@property (weak, nonatomic) IBOutlet UIButton *menuButton;
@property (weak, nonatomic) IBOutlet UILabel *resultsLabel;

@property (weak, nonatomic) IBOutlet UIButton *chooseLanguageButton;
@property (weak, nonatomic) IBOutlet UIButton *loginButton;
@property (weak, nonatomic) IBOutlet UIImageView *flagView;

@property (weak, nonatomic) IBOutlet UIButton *refreshButton;
@property (weak, nonatomic) IBOutlet UIButton *filtersButton;

@property (weak, nonatomic) IBOutlet UIView *titleView;

@property (weak, nonatomic) IBOutlet FiltersView *activeFiltersView;

/* results view outlets */
@property (weak, nonatomic) IBOutlet UIView *resultsView;
@property (weak, nonatomic) IBOutlet UIButton *infoButton;
@property (weak, nonatomic) IBOutlet UIButton *proceedButton;

@property (weak, nonatomic) IBOutlet UILabel *modelsNumber;
@property (weak, nonatomic) IBOutlet UILabel *seasonsNumber;
@property (weak, nonatomic) IBOutlet UILabel *categoriesNumber;

//dynamically visible brand results
@property (weak, nonatomic) IBOutlet UILabel *brandName0;
@property (weak, nonatomic) IBOutlet UILabel *brandNumber0;

@property (weak, nonatomic) IBOutlet UILabel *brandName1;
@property (weak, nonatomic) IBOutlet UILabel *brandNumber1;

@property (weak, nonatomic) IBOutlet UILabel *brandName2;
@property (weak, nonatomic) IBOutlet UILabel *brandNumber2;

@property (weak, nonatomic) IBOutlet UILabel *brandName3;
@property (weak, nonatomic) IBOutlet UILabel *brandNumber3;

@property (weak, nonatomic) IBOutlet UILabel *brandName4;
@property (weak, nonatomic) IBOutlet UILabel *brandNumber4;

@property (weak, nonatomic) IBOutlet UILabel *brandName5;
@property (weak, nonatomic) IBOutlet UILabel *brandNumber5;




- (void)setFilters:(NSDictionary *)filtersDictionary;
- (NSDictionary*)filtersDictionary;

- (IBAction)importButtonTapper:(id)sender;
- (IBAction)checkAuth:(id)sender;
- (IBAction)toggleFilters:(id)sender;

- (IBAction)searchAndHidePanel:(id)sender;

- (IBAction)toggleInfos:(id)sender;
- (IBAction)proceed:(id)sender;

- (void)launchUpdateImages;

@end
