//
//  main.m
//  Miles Product Research
//
//  Created by Davide Cenzi on 25/06/14.
//  Copyright (c) 2014 BlueTouch Development di Davide Cenzi. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
