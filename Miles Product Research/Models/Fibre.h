#import "_Fibre.h"
#import "S2SOrderedDictionary.h"

@interface Fibre : _Fibre {}
// Custom logic goes here.
+ (S2SOrderedDictionary*)filterValues;
@end
