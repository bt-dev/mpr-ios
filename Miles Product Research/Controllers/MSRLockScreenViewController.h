//
//  MSRLockScreenViewController.h
//  MSR
//
//  Created by Davide Cenzi on 19/04/14.
//  Copyright (c) 2014 BlueTouch Development di Davide Cenzi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MSRLockScreenViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *imageView;

- (IBAction)hide:(id)sender;
@end
