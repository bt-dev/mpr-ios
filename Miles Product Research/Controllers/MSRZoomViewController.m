//
//  MSRZoomViewController.m
//  MSR
//
//  Created by Davide Cenzi on 22/12/13.
//  Copyright (c) 2013 BlueTouch Development di Davide Cenzi. All rights reserved.
//

#import "MSRZoomViewController.h"


@interface MSRZoomViewController ()
{
    SSUIViewMiniMe *ssMiniMeView;
}
@end

@implementation MSRZoomViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (IBAction)close:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)prepareView{
    
    CGRect frame = CGRectMake(0.0, 0.0, 1024.0, 768.0);
    
    UIView *tempView = [[UIView alloc]initWithFrame:frame];
    UIImageView *imgView = [[UIImageView alloc]initWithImage:self.currentImage];
    imgView.frame = tempView.frame;
    [imgView setContentMode:UIViewContentModeScaleAspectFit];
    tempView.frame = imgView.frame;
    [tempView addSubview:imgView];

    ssMiniMeView = [[SSUIViewMiniMe alloc]initWithView:tempView withRatio:4];
    ssMiniMeView.delegate = self;
    ssMiniMeView.scrollView.backgroundColor = [UIColor whiteColor];
    ssMiniMeView.scrollView.frame = CGRectMake(0.0, 59.0, 1024.0, 709.0);
    [self.view addSubview:ssMiniMeView];
    [self.view bringSubviewToFront:self.headerView];

}

#pragma mark - delegate

- (void)enlargedView:(SSUIViewMiniMe *)enlargedView willBeginDragging:(UIScrollView *)scrollView
{
    //Delegate Example
}

#pragma mark -

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self prepareView];
    
    self.titleLabel.text = self.titleText;
    [self.titleLabel setFont:[UIFont fontWithName:@"Futura-CondensedMedium" size:20.0]];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
