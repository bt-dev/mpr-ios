//
//  BTCollapsableCollectionViewController.h
//  MSR
//
//  Created by Davide Cenzi on 04/06/14.
//  Copyright (c) 2014 BlueTouch Development di Davide Cenzi. All rights reserved.
//

#import "BTCollectionViewController.h"

@interface BTCollapsableCollectionViewController : BTCollectionViewController
@property (nonatomic, retain) NSMutableArray *collapsedSections;
- (BOOL)collapsedSection:(NSInteger)section;
- (void)toggleCollapseSection:(NSInteger)section;
- (IBAction)toggleCollapse:(id)sender;
@end
