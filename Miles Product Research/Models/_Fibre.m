// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Fibre.m instead.

#import "_Fibre.h"

const struct FibreAttributes FibreAttributes = {
	.code = @"code",
	.item_description = @"item_description",
	.language = @"language",
	.string_code = @"string_code",
};

const struct FibreRelationships FibreRelationships = {
	.product_fibres = @"product_fibres",
};

const struct FibreFetchedProperties FibreFetchedProperties = {
};

@implementation FibreID
@end

@implementation _Fibre

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Fibre" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Fibre";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Fibre" inManagedObjectContext:moc_];
}

- (FibreID*)objectID {
	return (FibreID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	
	if ([key isEqualToString:@"codeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"code"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}




@dynamic code;



- (int32_t)codeValue {
	NSNumber *result = [self code];
	return [result intValue];
}

- (void)setCodeValue:(int32_t)value_ {
	[self setCode:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveCodeValue {
	NSNumber *result = [self primitiveCode];
	return [result intValue];
}

- (void)setPrimitiveCodeValue:(int32_t)value_ {
	[self setPrimitiveCode:[NSNumber numberWithInt:value_]];
}





@dynamic item_description;






@dynamic language;






@dynamic string_code;






@dynamic product_fibres;

	
- (NSMutableSet*)product_fibresSet {
	[self willAccessValueForKey:@"product_fibres"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"product_fibres"];
  
	[self didAccessValueForKey:@"product_fibres"];
	return result;
}
	






@end
