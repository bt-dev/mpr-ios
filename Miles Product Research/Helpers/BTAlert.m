//
//  BTAlert.m
//  iKeyring
//
//  Created by Davide Cenzi on 25/11/13.
//  Copyright (c) 2013 BlueTouch Development di Davide Cenzi. All rights reserved.
//

#import "BTAlert.h"

@implementation BTAlert
@synthesize onConfirm = _onConfirm;
@synthesize onDismiss = _onDismiss;

- (id)initWithTitle:(NSString *)title text:(NSString*)text style:(UIAlertViewStyle)style onConfirm:(BTAlertOnConfirmAction)confirm
          onDismiss:(BTAlertOnDismissAction)dismiss{
    return [self initWithTitle:title text:text style:style confirmText:@"Ok" dismissText:@"Cancel" onConfirm:confirm onDismiss:dismiss];
}

- (id)initWithTitle:(NSString *)title text:(NSString*)text style:(UIAlertViewStyle)style confirmText:(NSString*)confirmText dismissText:(NSString*)dismissText  onConfirm:(BTAlertOnConfirmAction)confirm
                 onDismiss:(BTAlertOnDismissAction)dismiss{
    
    _dismissText = dismissText;
    _confirmText = confirmText;
    
    self = [super initWithTitle:title message:text delegate:self cancelButtonTitle:_dismissText otherButtonTitles:_confirmText, nil];

    if(self){
        [self setAlertViewStyle:style];
        _onConfirm = confirm;
        _onDismiss = dismiss;
    }
    return self;
}

#pragma mark - alertview delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if([[alertView buttonTitleAtIndex:buttonIndex] isEqualToString:_dismissText] && _onDismiss)
        _onDismiss(alertView);
    else if([[alertView buttonTitleAtIndex:buttonIndex] isEqualToString:_confirmText] && _onConfirm)
        _onConfirm(alertView);
}

#pragma mark -


@end
