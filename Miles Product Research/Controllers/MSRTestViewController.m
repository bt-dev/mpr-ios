//
//  MSRTestViewController.m
//  MSR
//
//  Created by Davide Cenzi on 07/12/13.
//  Copyright (c) 2013 BlueTouch Development di Davide Cenzi. All rights reserved.
//

#import "MSRTestViewController.h"

@interface MSRTestViewController (){
    BTLanguage *lang;
}

@end

@implementation MSRTestViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (IBAction)import:(id)sender{
    [self performSegueWithIdentifier:@"import" sender:self];
}

- (IBAction)changeLanguage:(id)sender{

    if([[[BTLanguage shared] currentLanguage] isEqualToString:@"en"])
        [[BTLanguage shared] changeLanguageTo:@"it" withReboot:YES];
    else
        [[BTLanguage shared] changeLanguageTo:@"en" withReboot:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{

}

@end
