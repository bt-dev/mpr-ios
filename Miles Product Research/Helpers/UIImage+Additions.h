//
//  UIImage+Additions.h
//  NeedleIOS
//
//  Created by Davide Cenzi on 10/11/13.
//  Copyright (c) 2013 Davide Cenzi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Additions)

- (UIImage *) scaleToSize: (CGSize)size;
- (UIImage *) scaleProportionalToSize: (CGSize)size1;

@end
