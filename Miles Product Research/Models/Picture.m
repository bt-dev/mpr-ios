#import "Picture.h"


@interface Picture (){
    NSURL *_fileURL;
    NSURL *_thumbURL;
}

// Private interface goes here.
+ (NSString*)baseDirectory;

@end


@implementation Picture

// Custom logic goes here.



+ (NSString*)baseDirectory{
    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    return documentsDirectory;
}

- (BOOL)primaryPicture{
    //NSLog(@"Checking picture name: %@ - against %@",[self.image_file_name lowercaseString], [self.product.default_image lowercaseString]);
    return ([[self.image_file_name lowercaseString] isEqualToString:[self.product.default_image lowercaseString]]);
}

// Custom logic goes here.
- (NSString*)remoteFilePath{
    NSString *baseURL = [[BTDataSync shared] apiURL];
    NSString *path = [NSString stringWithFormat:@"%@/system/images/original/%d.jpg",baseURL,self.codeValue];
    
    return path;
}


- (NSString*)localFilePath{
    NSString *path = [[[Picture baseDirectory] stringByAppendingFormat:@"/images/original/%d.jpg",self.codeValue] stringByReplacingOccurrencesOfString:@" " withString:@"\ "];
    
    if([[NSFileManager defaultManager] fileExistsAtPath:path isDirectory:NO])
        return path;
    else
        return nil;
}

- (NSString*)bundledFilePath{
    NSString *path = [[[NSBundle mainBundle] bundlePath] stringByAppendingFormat:@"/images.bundle/original/%d.jpg",self.codeValue];
    
    if([[NSFileManager defaultManager] fileExistsAtPath:path isDirectory:NO])
        return path;
    else
        return nil;
}

- (NSString*)filePath{
    NSString *path = nil;
    
    if([[BTDataSync shared] forceRemoteAssets]){
        path = [self remoteFilePath];
    }
    
    if(!path)
        path = [self localFilePath];
    
    if(!path)
        path = [self bundledFilePath];
    
    if(!path)
        path = [self remoteFilePath];
    
    return path;
}

- (NSURL*)fileURL{
    if(_fileURL)
        return _fileURL;
    
    NSString *path = nil;
    
    path = [self filePath];
    
    if(path){
        if([path rangeOfString:@"http"].location != NSNotFound)
            _fileURL = [NSURL URLWithString:path];
        else
            _fileURL = [NSURL fileURLWithPath:path];
    }

    return _fileURL;
}

- (NSURL*)thumbURL{
    if(_thumbURL)
        return _thumbURL;
    
    NSString *path = [self filePath];
    
    if(path){
        //NSString *_thumbpath = [path stringByReplacingOccurrencesOfString:@"original" withString:@"thumb"];
        //path = [path stringByReplacingOccurrencesOfString:@"original" withString:@"thumb"];
        if([path rangeOfString:@"http"].location != NSNotFound)
            _thumbURL = [NSURL URLWithString:path];
        else
            _thumbURL = [NSURL fileURLWithPath:path];
    }
    else
        _thumbURL = [self fileURL];
    
    return _thumbURL;
}

@end
