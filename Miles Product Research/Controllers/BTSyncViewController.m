//
//  BTSyncViewController.m
//  NeedleIOS
//
//  Created by Davide Cenzi on 11/07/13.
//  Copyright (c) 2013 Davide Cenzi. All rights reserved.
//

#import "BTSyncViewController.h"

@interface BTSyncViewController (){

}

@end

@implementation BTSyncViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

// override these
#pragma mark - methods to override in subclasses

- (void)refreshData{
    
}

- (NSString*)remotePath{
    return @"";
}

- (NSString*)modelName{
    return @"Category";
}

#pragma mark -

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
