//
//  NMFieldDelegate.h
//  Needle
//
//  Created by Davide Cenzi on 01/08/13.
//  Copyright (c) 2013 BlueTouch Development di Davide Cenzi. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^NMChangedTextFieldBlock)(id textField, id nextField);
typedef BOOL (^NMTextFieldShouldReturnBlock)(id textField);
typedef BOOL (^NMTextFieldShouldEndBlock)(id textField);

@interface NMFieldDelegate : NSObject <UITextFieldDelegate>{
    BOOL _useToolbar;
    NSArray *_fields;
    id _contentView;
    BOOL _autoScroll;
    BOOL _fieldsDelegate;
    
    NMChangedTextFieldBlock _changeBlock;
    NMTextFieldShouldReturnBlock _returnBlock;
    NMTextFieldShouldEndBlock _endBlock;
}

@property(nonatomic,retain) UITextField *currentField;

- (id)initWithFields:(NSArray*)fields fieldChangeBlock:(NMChangedTextFieldBlock)changeBlock
   shouldReturnBlock:(NMTextFieldShouldReturnBlock)returnBlock
      shouldEndBlock:(NMTextFieldShouldEndBlock)endBlock
        withAutoScrollOnView:(id)view;

- (void)addField:(UITextField*)field;
- (void)useToolbar:(BOOL)enableToolbar;
- (void)setFieldsDelegate:(BOOL)fieldsDelegate;

@end
