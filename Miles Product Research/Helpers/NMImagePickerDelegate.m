//
//  NMImagePickerDelegate.m
//  Needle
//
//  Created by Davide Cenzi on 21/08/13.
//  Copyright (c) 2013 BlueTouch Development di Davide Cenzi. All rights reserved.
//

#import "NMImagePickerDelegate.h"

@interface NMImagePickerDelegate ()

- (void)showPicker;

@end

@implementation NMImagePickerDelegate


- (id)initWithSourceType:(UIImagePickerControllerSourceType)sourceType pickedImageBlock:(NMPickedImageBlock)pickedBlock dismissedBlock:(NMDismissedPickerBlock)dismissedBlock{
    
    self =[super init];
    
    if(self){
        _sourceType = sourceType;
        _pickedBlock = pickedBlock;
        _dismissedBlock = dismissedBlock;
    }
    
    return self;
}

- (id)initWithRequestText:(NSString*)alertText pickedImageBlock:(NMPickedImageBlock)pickedBlock dismissedBlock:(NMDismissedPickerBlock)dismissedBlock{
    
    self =[super init];
    
    if(self){
        _alertText = alertText;
        _pickedBlock = pickedBlock;
        _dismissedBlock = dismissedBlock;
        _sourceType = -100;

    }
    
    return self;
    
}

- (void)showPicker{
    
    if(_viewController == nil) return;
    
    if(_pickerController == nil){
        
        _pickerController = [[UIImagePickerController alloc] init];
        
        _pickerController.allowsEditing = YES;
        _pickerController.delegate = self;
        _pickerController.sourceType = _sourceType;
        
        if(_viewController != nil)
            [_viewController presentModalViewController:_pickerController animated:YES];
    }
}

- (void)presentInViewController:(id)viewController{
    if(viewController == nil)
        return;
    
    _viewController = viewController;
    
    if(_sourceType == UIImagePickerControllerSourceTypeCamera || _sourceType == UIImagePickerControllerSourceTypePhotoLibrary){
        [self showPicker];
    }
    else{ //need to create a UIAlertView
        NSString *alertText = (_alertText != nil && ![_alertText isEqualToString:@""]) ? _alertText : NSLocalizedString(@"Choose image from", @"Generic image picker title");
        
        UIAlertView *al = [[UIAlertView alloc] initWithTitle:alertText message:NSLocalizedString(@"Please choose a picture from",@"Generic image picker message") delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel",@"") otherButtonTitles:NSLocalizedString(@"Camera",@""),NSLocalizedString(@"Photo album", @""), nil];
        [al show];

    }
}

#pragma mark - UIImagePickerDelegate

- (void)imagePickerController:(UIImagePickerController *)imagePicker didFinishPickingImage:(UIImage *)image
                  editingInfo:(NSDictionary *)editingInfo{
    
    if(_pickedBlock)
        _pickedBlock(image);
    
    //reset value
    _sourceType = -100;
    
    [_viewController dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    
    if(_dismissedBlock)
        _dismissedBlock();
    
    //reset value
    _sourceType = -100;
    
    [_viewController dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark -

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if([[alertView buttonTitleAtIndex:buttonIndex] isEqualToString:NSLocalizedString(@"Photo album",@"")]){
        _sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self showPicker];
    }
    else if([[alertView buttonTitleAtIndex:buttonIndex] isEqualToString:NSLocalizedString(@"Camera",@"")]){
        _sourceType = UIImagePickerControllerSourceTypeCamera;
        [self showPicker];
    }
    //else cancelled
    
    
}

#pragma mark -

@end
