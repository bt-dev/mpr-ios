#import "BrandType.h"


@interface BrandType ()

// Private interface goes here.

@end


@implementation BrandType

// Custom logic goes here.
+ (S2SOrderedDictionary*)filterValues{
    S2SOrderedDictionary *data = [[S2SOrderedDictionary alloc] init];
    
    [data setObject:@"" forKey:[NSString stringWithFormat:NSLocalizedString(@" -- All %@ --",@" -- Tutte le tipologie marchio -- "),NSLocalizedString(@"brand types", @"brand types")]];
    
    NSSortDescriptor *sortByName = [[NSSortDescriptor alloc] initWithKey:@"item_description" ascending:YES];
    NSFetchRequest *f = [BrandType fetchRequest];
    [f setSortDescriptors:[NSArray arrayWithObject:sortByName]];
    
    NSMutableString *format = [[NSMutableString alloc] init];
    [format appendFormat:@"(code > 0) AND (language == '%@')",[[BTDataSync shared] language]];
    NSPredicate *p = [NSPredicate predicateWithFormat:format];
    [f setPredicate:p];
    
    for (BrandType *tmp in [BrandType objectsWithFetchRequest:f])
        [data setObject:tmp.code.stringValue forKey:tmp.item_description];
    
    return data;
}

@end
