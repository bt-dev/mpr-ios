//
//  NSManagedObjectContext+SRFetchAsync.h
//  Miles Product Research
//
//  Created by Davide Cenzi on 29/07/14.
//  Copyright (c) 2014 BlueTouch Development di Davide Cenzi. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface NSManagedObjectContext(SRFetchAsync)
- (void)executeFetchRequest:(NSFetchRequest *)request completion:(void (^)(NSArray *objects, NSError *error))completion;

@end
