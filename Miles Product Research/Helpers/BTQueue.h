//
//  BTQueue.h
//  PhotoMailer
//
//  Created by Davide Cenzi on 05/01/13.
//  Copyright (c) 2013 BlueTouch Development di Davide Cenzi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BTQueue : NSMutableArray

- (void)enqueue:(id)object;
- (id)dequeue;

@end
