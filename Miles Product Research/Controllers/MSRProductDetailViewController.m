//
//  MSRProductDetailViewController.m
//  MSR
//
//  Created by Davide Cenzi on 19/01/14.
//  Copyright (c) 2014 BlueTouch Development di Davide Cenzi. All rights reserved.
//

#import "MSRProductDetailViewController.h"
#import "MSRZoomViewController.h"
#import "SliderView.h"

@interface MSRProductDetailViewController (){
    Product *choosenProduct;
    NSArray *sortedPictures;
    SliderView *choosedThumbView;
}

@end

@implementation MSRProductDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (NSArray *)titlesLabels{
    return @[self.modelLabelTitle,
             self.categoryLabelTitle,
             self.seasonLabelTitle,
             self.brandTypeLabelTitle,
             self.brandLabelTitle,
             self.supplierLabelTitle,
             self.yarnLabelTitle,
             self.compositionLabelTitle,
             self.stitchTypeLabelTitle,
             self.thinnessLabelTitle];
}

- (NSArray *)valuesLabels{
    return @[
             self.modelLabel,
             self.categoryLabel,
             self.seasonLabel,
             self.brandTypeLabel,
             self.brandLabel,
             self.supplierLabel,
             self.thinnessLabel
             ];
}

- (NSArray *)thumbsButtons{
    return @[
             self.thumbButton0, //sketch?
             self.thumbButton1,
             self.thumbButton2,
             self.thumbButton3,
             self.thumbButton4,
             self.thumbButton5,
             self.thumbButton6,
             self.thumbButton7,
             self.thumbButton8,
             self.thumbButton9
             ];
}

- (NSArray *)thumbsImageViews{
    return @[
             self.thumb0, //sketch?
             self.thumb1,
             self.thumb2,
             self.thumb3,
             self.thumb4,
             self.thumb5,
             self.thumb6,
             self.thumb7,
             self.thumb8,
             self.thumb9
             ];
}

- (NSArray *)yarnLabels{
    return @[
             self.yarnLabel0,
             self.yarnLabel1,
             self.yarnLabel2,
            ];
}

- (NSArray *)compositionLabels{
    return @[self.compositionLabel0,
             self.compositionLabel1,
             self.compositionLabel2,
             ];
}

- (NSArray *)stitchTypeLabels{
    return @[
             self.stitchTypeLabel0,
             self.stitchTypeLabel1,
             self.stitchTypeLabel2
            ];
}

- (void)setupSlider{
    for (UIView *v in self.sliderView.subviews)
        [v removeFromSuperview];
    
    int count = 0;
    
    for (Product *p in self.choosenProducts) {
        SliderView *c = [[[NSBundle mainBundle] loadNibNamed:@"SliderView" owner:nil options:nil] firstObject];
        CGRect frame = c.frame;
        frame.origin.x = ((count * frame.size.width) + (count +1) *10.0);
        c.frame = frame;
        
        //[c.productImage setImage:[UIImage imageNamed:@"product.jpg"]];
        if(p.defaultPictureURL)
            [c.productImage setImageWithURL:p.defaultPictureURL placeholderImage:nil];
        else
            [c.productImage setImageWithURL:[NSURL URLWithString:@"http://placehold.it/364x480&text=IMAGE+MISSING"]];
        
        c.seasonLabel.text = [NSString stringWithFormat:@"%@ - %d",[p.macro_season item_description], p.cleanedYear];
        
        if(p.category)
            c.categoryLabel.text = [[p.category item_description] uppercaseString];
        else
            c.categoryLabel.text = NSLocalizedString(@"NO CATEGORY FOUND", @"");
        
        c.supplierLabel.text = p.supplier.item_description;
        c.productCode = p.code;
        [c.selectionButton addTarget:self action:@selector(chooseProduct:) forControlEvents:UIControlEventTouchUpInside];
        
        [self.sliderView addSubview:c];
        
        count += 1;
    }
    
    self.sliderView.contentSize = CGSizeMake((self.choosenProducts.count * 130 +
                                              (self.choosenProducts.count +1) * 10),self.sliderView.contentSize.height);
}

- (void)cleanView{
    
    
    self.bigTitleLabel.text = @"";
    
    for(UILabel *label in [[[[self valuesLabels]
        arrayByAddingObjectsFromArray:[self yarnLabels]]
            arrayByAddingObjectsFromArray:[self compositionLabels]]
                arrayByAddingObjectsFromArray: [self stitchTypeLabels]]
                           )
        label.text = @"";
    
    for(UIButton *btn in [self thumbsButtons])
        [btn setImage:nil forState:UIControlStateNormal];
    
    
    //self.brandName;
    //self.brandView;

    [self.productView setImage:[UIImage imageNamed:@"logo_big.png"]];
}


- (void)setupView{
    
    [self cleanView];
    
    if(!choosenProduct)
        return;
    
    sortedPictures = choosenProduct.sortedPictures;
    NSLog(@"Setting up view for product: %d",choosenProduct.codeValue);
    
    self.modelLabel.text = [NSString stringWithFormat:@"%d",choosenProduct.model.intValue];
    self.categoryLabel.text = choosenProduct.category.item_description;
    self.seasonLabel.text = choosenProduct.macro_season.item_description;
    //self.brandTypeLabel.text = choosenProduct.brand_type
    self.brandLabel.text = choosenProduct.brand.item_description;
    self.supplierLabel.text = choosenProduct.supplier.item_description;
    self.thinnessLabel.text = [choosenProduct.thinness description];
    
    NSArray *labels = [self yarnLabels];
    
    for(int i = 0; i< [choosenProduct.yarns count] && i < [labels count]; i++){
        UILabel *label = [labels objectAtIndex:i];
        Yarn *y = [choosenProduct.yarns objectAtIndex:i];
        label.text = [y item_description];
    }
    
    labels = [self compositionLabels];
    for(int i = 0; i< [choosenProduct.product_fibres count] && i < [labels count]; i++){
        UILabel *label = [labels objectAtIndex:i];
        ProductFibre *p = [choosenProduct.product_fibres.allObjects objectAtIndex:i];
        label.text = [NSString stringWithFormat:@"%@ %@ %@%%",p.fibre.item_description, ((p.percentage != nil && p.percentage.floatValue < 100.0) ? @">" : @""),((p.percentage != nil && p.percentage.floatValue < 100.0) ? p.percentage.description : @"100") ];
    }
    
    labels = [self stitchTypeLabels];
    for(int i = 0; i< [choosenProduct.stitchTypes count] && i < [labels count]; i++){
        UILabel *label = [labels objectAtIndex:i];
        StitchType *y = [choosenProduct.stitchTypes objectAtIndex:i];
        label.text = [y item_description];
    }
    
    if([BTDataSync shared].signedIn && self.brandName != nil)
        self.bigTitleLabel.text = [NSString stringWithFormat:@"%d %@ - %@ %@",choosenProduct.cleanedYear, choosenProduct.macro_season.item_description, choosenProduct.model, choosenProduct.brand.item_description];
    else
        self.bigTitleLabel.text = [NSString stringWithFormat:@"%d %@ - %@ %@",choosenProduct.cleanedYear, choosenProduct.macro_season.item_description, choosenProduct.model, (choosenProduct.category.item_description == nil ? @"" : choosenProduct.category.item_description)];

    self.productView.tag = 0; //default selected index == 0
    
    if(choosenProduct.defaultPictureURL){
        
        NSLog(@"Default image index: %d",choosenProduct.defaultPictureIndex);
        [self loadProductImageAtIndex:choosenProduct.defaultPictureIndex];
    }
    else
        [self.productView setImage:[UIImage imageNamed:@"missing.gif"]];
    
    NSArray *thumbsButtons = [self thumbsButtons];
    NSArray *thumbsViews = [self thumbsImageViews];
    for (int i = 0; i < [sortedPictures count] && i < [thumbsButtons count]; i++) {
        UIButton *btn = [thumbsButtons objectAtIndex:i];
        UIImageView *v = [thumbsViews objectAtIndex:i];
        
        Picture *pic = [sortedPictures objectAtIndex:i];
        
        btn.tag = i;
        
        if(pic.fileURL)
            [v setImageWithURL:pic.fileURL placeholderImage:[UIImage imageNamed:@"loading.gif"]];
        else
            [v setImage:nil];
        
    }
    
    //new fields
    self.familyLabel.text = choosenProduct.family.item_description;
    self.shimaLabel.text = (choosenProduct.prgshima_existsValue ? NSLocalizedString(@"YES",@"Yes") : NSLocalizedString(@"NO",@"No"));
    self.stollLabel.text = (choosenProduct.prgstoll_existsValue ? NSLocalizedString(@"YES",@"Yes") : NSLocalizedString(@"NO",@"No"));
    
    self.archivedLabel.text = (choosenProduct.storedValue ? NSLocalizedString(@"YES",@"Yes") : NSLocalizedString(@"NO",@"No"));
    self.roomLabel.text = choosenProduct.room.description;
    self.positionLabel.text = choosenProduct.position.description;
    
    self.roomContainer.hidden = !choosenProduct.storedValue;
    self.positionContainer.hidden = !choosenProduct.storedValue;
}

- (Product*)productWithCode:(NSNumber*)code{
    Product *choosen = nil;
    
    for (Product *p in self.choosenProducts) {
        if(p.codeValue == code.intValue){
            choosen = p;
            break;
        }
    }
    
    return choosen;
}

- (void)loadProductImageAtIndex:(int)index{
    
    if(choosenProduct){
        NSLog(@"Searching image for product with code: %d",choosenProduct.code.intValue);
        
        if(!sortedPictures || [sortedPictures count] <= index)
            return;
        
        self.productView.tag = index;
        
        for (BTBorderedButton *b in [self thumbsButtons])
            [b setBorderColor:[UIColor whiteColor]];
        BTBorderedButton *btn = [[self thumbsButtons] objectAtIndex:index];
        
        if(btn)
            [btn setBorderColor:[UIColor colorWithRed:0.282 green:0.725 blue:0.839 alpha:1.000]];
        
        
        Picture *pic = [sortedPictures objectAtIndex:index];
        if(pic.fileURL)
            [self.productView setImageWithURL:pic.fileURL placeholderImage:[UIImage imageNamed:@"loading.gif"]];
        else
            [self.productView setImage:[UIImage imageNamed:@"missing.gif"]];
    }
}

#pragma mark - IBActions

- (IBAction)chooseProduct:(id)sender{
    UIButton *btn = (UIButton*)sender;
    SliderView *v = (SliderView*)btn.superview;
    
    if(choosedThumbView)
       [choosedThumbView setActive:NO];
    
    NSArray *thumbsViews = [self thumbsImageViews];
    for (UIImageView *tmp in thumbsViews)
        [tmp setImage:nil];
    
    
    if(v && v.productCode && !v.isActive){
        choosedThumbView = v;
        choosenProduct = [self productWithCode:v.productCode];
        
        [choosedThumbView setActive:!choosedThumbView.isActive];
        [self setupView];
    }
}

- (IBAction)choosePicture:(id)sender{
    BTBorderedButton *btn = (BTBorderedButton*)sender;
    int index = (int)btn.tag;
    
    if(index > -1 && index < [sortedPictures count])
        [self loadProductImageAtIndex:index];
}

- (IBAction)showPicture:(id)sender{
    if(self.productView.image)
        [self performSegueWithIdentifier:@"showZoom" sender:self];
}

- (IBAction)nextPicture:(id)sender{
    
    int index = (int)self.productView.tag;
    index += 1;
    
    if(index >= ([sortedPictures count]))
        index = 0;
    
    [self loadProductImageAtIndex:index];
    
}

- (IBAction)prevPicture:(id)sender{
    
    int index = (int)self.productView.tag;
    index -= 1;
    
    if(index < 0)
        index = ([sortedPictures count] -1);
    
    [self loadProductImageAtIndex:index];
    
}

#pragma mark -

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if([segue.identifier isEqualToString:@"showZoom"]){
        MSRZoomViewController *z = [segue destinationViewController];
        z.currentImage = self.productView.image;
        z.titleText = [NSLocalizedString(@"Product image",@"") uppercaseString];
    }
    
}

- (void)checkBrandsVisibility{
    //NSLog(@"Checking brand visibility based on user authentication: %hhd",[BTDataSync shared].signedIn);
    return;
    
    if(![BTDataSync shared].signedIn){
        [UIView animateWithDuration:0.3 animations:^{
            CGRect f = self.model_wrapper.frame;
            [self.model_wrapper setFrame:CGRectMake(f.origin.x, 126.0, f.size.width, f.size.height)];
            [self.brandView setHidden:YES];
        }];
    }
    else{
        [UIView animateWithDuration:0.3 animations:^{
            
            CGRect f = self.model_wrapper.frame;
            [self.model_wrapper setFrame:CGRectMake(f.origin.x, 155.0, f.size.width, f.size.height)];
            [self.brandView setHidden:NO];
        }];
    }
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    [self checkBrandsVisibility];
//    
//

}
 
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    if(!self.choosenProducts)
        self.choosenProducts = @[];
    if(!choosenProduct && [self.choosenProducts count] > 0)
        choosenProduct = [self.choosenProducts firstObject];
    
    self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width, self.containerView.frame.size.height+1);
    [self setupView];
    [self setupSlider];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
