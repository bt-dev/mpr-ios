//
//  MSRTreeViewController.m
//  MSR
//
//  Created by Davide Cenzi on 11/12/13.
//  Copyright (c) 2013 BlueTouch Development di Davide Cenzi. All rights reserved.
//

#import "MSRTreeViewController.h"
#import "UIViewController+ECSlidingViewController.h"
#import "MSRTreeCell.h"
#import "NMFieldDelegate.h"
#import "S2SOrderedDictionary.h"



#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

@interface MSRTreeViewController (){

}

@property (strong, nonatomic) NSArray *data;
@property (nonatomic, strong) UINavigationController *transitionsNavigationController;

- (void)notifyDelegate;

@end

@implementation MSRTreeViewController

- (void)awakeFromNib
{
    self.preferredContentSize = CGSizeMake(320.0, 400.0);
    [super awakeFromNib];
}

- (void)setupView{
    
    NSLog(@"Actual language: %@",[[BTDataSync shared] language]);
    if(!self.data)
        self.data = [StitchType rootObjects];
    
    NSLog(@"Current width: %f",self.view.frame.size.width);
    
    CGRect frame = CGRectMake(0.0, 79.0, 320.0, 321.0);
    
    RATreeView *treeView = [[RATreeView alloc] initWithFrame:frame];
    
    treeView.delegate = self;
    treeView.dataSource = self;
    treeView.separatorStyle = RATreeViewCellSeparatorStyleNone;

    [treeView setBackgroundColor:[UIColor colorWithRed:30.0 / 255.0 green:30.0 / 255.0 blue:30.0 / 255.0 alpha:1.0]];
    treeView.autoresizingMask = UIViewAutoresizingNone;
    
    self.treeView = treeView;
    [self.view addSubview:treeView];
    
    
    self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width, 321.0); //+1 la fa scrollare
	self.scrollView.maximumZoomScale = 1.0; //puo' variare se voglio renderla zoomabile
	self.scrollView.minimumZoomScale = 1.0;
	self.scrollView.clipsToBounds = YES;
	self.scrollView.delegate = self;

}

#pragma mark TreeView Delegate methods
- (CGFloat)treeView:(RATreeView *)treeView heightForRowForItem:(id)item treeNodeInfo:(RATreeNodeInfo *)treeNodeInfo
{
    return 39;
}

- (NSInteger)treeView:(RATreeView *)treeView indentationLevelForRowForItem:(id)item treeNodeInfo:(RATreeNodeInfo *)treeNodeInfo
{
    return 3 * treeNodeInfo.treeDepthLevel;
}

- (BOOL)treeView:(RATreeView *)treeView shouldExpandItem:(id)item treeNodeInfo:(RATreeNodeInfo *)treeNodeInfo
{
    return YES;
}

- (BOOL)treeView:(RATreeView *)treeView shouldItemBeExpandedAfterDataReload:(id)item treeDepthLevel:(NSInteger)treeDepthLevel
{
    if ([item isEqual:self.expanded]) {
        return YES;
    }
    
    return NO;
}

- (void)treeView:(RATreeView *)treeView didCollapseRowForItem:(id)item treeNodeInfo:(RATreeNodeInfo *)treeNodeInfo{
    [treeView reloadRowsForItems:@[item] withRowAnimation:RATreeViewRowAnimationAutomatic];
}

- (void)treeView:(RATreeView *)treeView didExpandRowForItem:(id)item treeNodeInfo:(RATreeNodeInfo *)treeNodeInfo{
    
    [treeView reloadRowsForItems:@[item] withRowAnimation:RATreeViewRowAnimationAutomatic];
    [self resizeTableViewFrameHeight];
    
}

- (void)treeView:(RATreeView *)treeView didSelectRowForItem:(id)item treeNodeInfo:(RATreeNodeInfo *)treeNodeInfo{
    
    StitchType *old_item = self.expanded;
    
    if(self.expanded && [self.expanded isEqual:item]){
           self.expanded = nil;
    }
    else
        self.expanded = item;
    
    //remove previous X
    if(old_item)
        [treeView reloadRowsForItems:@[old_item] withRowAnimation:RATreeViewRowAnimationAutomatic];

    
    //update current node
    [treeView reloadRowsForItems:@[item] withRowAnimation:RATreeViewRowAnimationAutomatic];

}

- (IBAction)dismiss:(id)sender{
    [self notifyDelegate];
}

- (void)resizeTableViewFrameHeight
{
    // Table view does not scroll, so its frame height should be equal to its contentSize height

    CGRect frame = self.treeView.frame;
    self.treeView.frame = CGRectMake(0.0,79.0,frame.size.width,self.view.frame.size.height - 79.0);
    self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width, 79.0 + self.treeView.frame.size.height +1.0);
}

#pragma mark TreeView Data Source

- (UITableViewCell *)treeView:(RATreeView *)treeView cellForItem:(id)item treeNodeInfo:(RATreeNodeInfo *)treeNodeInfo
{
    StitchType *__node = item;
    NSInteger numberOfChildren = [__node.childrens count];
    
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"MSRTreeCell" owner:self options:nil];
    MSRTreeCell *cell = (MSRTreeCell *)[nib objectAtIndex:0];
    cell.titleLabel.text = [[__node item_description] uppercaseString];
    cell.titleLabel.frame = CGRectMake(cell.titleLabel.frame.origin.x + (treeNodeInfo.treeDepthLevel * 25), cell.titleLabel.frame.origin.y, cell.titleLabel.frame.size.width, cell.titleLabel.frame.size.height);

    [cell.checkButton setUserInteractionEnabled:NO];
    
    if ([treeNodeInfo isExpanded]) {
        [cell.checkButton setImage:[UIImage imageNamed:@"menu_contract.png"] forState:UIControlStateNormal];
        
    }
    else{ //selected
        if(numberOfChildren > 0)
            [cell.checkButton setImage:[UIImage imageNamed:@"menu_expand.png"] forState:UIControlStateNormal];
        else if([item isEqual:self.expanded]) //selezione finale
            [cell.checkButton setImage:[UIImage imageNamed:@"treeview_selection.png"] forState:UIControlStateNormal];
        else
            [cell.checkButton setImage:Nil forState:UIControlStateNormal];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

- (NSInteger)treeView:(RATreeView *)treeView numberOfChildrenOfItem:(id)item
{
    StitchType *__node = item;
    if (__node == nil) {
        return [self.data count];
    }

    return [__node.childrens count];
}

- (id)treeView:(RATreeView *)treeView child:(NSInteger)index ofItem:(id)item
{
    StitchType *__node = item;
    if (__node == nil) {
        return [self.data objectAtIndex:index];
    }
    
    return [__node.childrens.allObjects objectAtIndex:index];
}

- (void)treeView:(RATreeView *)treeView accessoryButtonTappedForRowForItem:(id)item treeNodeInfo:(RATreeNodeInfo *)treeNodeInfo{
    NSLog(@"Accessory button tapped");
}

#pragma mark -

#pragma mark -

- (void)upcaseLabels{
    for (UIView *v in self.view.subviews) {
        if([v isKindOfClass:[UILabel class]] && [((UILabel*)v) text])
            [((UILabel*)v) setText:[[((UILabel*)v) text] uppercaseString]];
    }
}

- (void)notifyDelegate{
    NSLog(@"Updating delegate with selection ....");
    
    if(self.delegate && [self.delegate respondsToSelector:@selector(selectedStitchType:)])
        [self.delegate performSelector:@selector(selectedStitchType:) withObject:self.expanded];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    //remove / restore selection
    [self.treeView reloadData];
    NSLog(@"Updating treeview...");
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setupView];
    
    [self upcaseLabels];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
