#import "ProductCategory.h"


@interface ProductCategory ()

// Private interface goes here.

@end


@implementation ProductCategory

// Custom logic goes here.
+ (S2SOrderedDictionary*)filterValues{
    S2SOrderedDictionary *data = [[S2SOrderedDictionary alloc] init];
    
    [data setObject:@"" forKey:[NSString stringWithFormat:NSLocalizedString(@" -- All %@ --",@" -- Tutte le categorie -- "),NSLocalizedString(@"categories", @"categories")]];
    
    NSSortDescriptor *sortByName = [[NSSortDescriptor alloc] initWithKey:@"item_description" ascending:YES];
    NSFetchRequest *f = [ProductCategory fetchRequest];
    [f setSortDescriptors:[NSArray arrayWithObject:sortByName]];
    
    
    for (ProductCategory *tmp in [ProductCategory objectsWithFetchRequest:f])
        [data setObject:tmp.code.stringValue forKey:tmp.item_description];
    
    return data;
}

@end
