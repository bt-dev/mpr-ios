#import "_Picture.h"

@interface Picture : _Picture {}
// Custom logic goes here.
- (NSString*)filePath;
- (NSURL*)fileURL;
- (NSURL*)thumbURL;
- (NSString*)localFilePath;
- (NSString*)bundledFilePath;
- (BOOL)primaryPicture;

@end
