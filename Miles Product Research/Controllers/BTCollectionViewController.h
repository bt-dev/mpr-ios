//
//  BTCollectionViewController.h
//  MSR
//
//  Created by Davide Cenzi on 20/12/13.
//  Copyright (c) 2013 BlueTouch Development di Davide Cenzi. All rights reserved.
//

#import "BTSyncViewController.h"

@interface BTCollectionViewController : BTSyncViewController<UICollectionViewDataSource, UICollectionViewDelegate, NSFetchedResultsControllerDelegate, UICollectionViewDelegateFlowLayout>

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (nonatomic,weak) IBOutlet UICollectionView *collectionView;
@property (nonatomic, strong) NSString *groupingKeypath;

- (void)refreshData;
- (NSFetchRequest*)fetchRequest;

@end
