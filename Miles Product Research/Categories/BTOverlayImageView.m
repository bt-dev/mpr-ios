//
//  BTOverlayImageView.m
//  MSR
//
//  Created by Davide Cenzi on 10/01/14.
//  Copyright (c) 2014 BlueTouch Development di Davide Cenzi. All rights reserved.
//

#import "BTOverlayImageView.h"


@interface BTOverlayImageView (){
    UIView *whiteLayer;
    BOOL animating;
    UIColor *_overlayColor;
}
- (void)setup;
@end

@implementation BTOverlayImageView


- (void)setup{
    current_value = 1.0;
    _overlayColor = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:0.6];
    whiteLayer = [[UIView alloc] init];
    [whiteLayer setFrame:CGRectMake(0.0, 0.0, self.frame.size.height, self.frame.size.height)];
    [whiteLayer setBackgroundColor:_overlayColor];
    //[[whiteLayer layer] setCornerRadius:kCornerRadius];
    [self insertSubview:whiteLayer atIndex:0];
    
}

- (id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    
    if (self) {
        // Initialization code
        [self setup];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self setup];
    }
    return self;
}


- (double)onePercentHeight{
    return (self.frame.size.height / 100.0);
}

- (double)progress{
    double foo = (1.0 - current_value);
    return foo;
}

- (void)setProgress:(double)value{
    
    //user invert the progress
    double new_val = (1.0 - value);
    

    //only values between 0.0 and 1.0
    if (new_val < 0.0)
        new_to_value = 0.0;
    else if(new_val > 1.0)
        new_to_value = 1.0;
    else
        new_to_value = new_val;
    
    if(animating){
        NSLog(@"Skipping animation for progress %2f",value);
        return;
    }
    
    double one_percent = [self onePercentHeight];
    double newPercent = (new_to_value * 100.0);
    double newHeight = (one_percent * newPercent);
    
    animating = YES;
    [UIView animateWithDuration:0.2 animations:^{
        
        /* This direct assignment is possible
         * using the UIView+ALQuickFrame category
         * http://github.com/andrealufino/ALQuickFrame */
        whiteLayer.frame = CGRectMake(0.0, 0.0, self.frame.size.width, newHeight);
        // Set the color based on battery level
        whiteLayer.backgroundColor = _overlayColor;
        current_value = new_to_value;
        animating = NO;
    }];

}

- (void)setOverlayColor:(UIColor*)color{
    _overlayColor = color;
    [whiteLayer setBackgroundColor:_overlayColor];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
