// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Supplier.m instead.

#import "_Supplier.h"

const struct SupplierAttributes SupplierAttributes = {
	.code = @"code",
	.item_description = @"item_description",
	.language = @"language",
	.string_code = @"string_code",
};

const struct SupplierRelationships SupplierRelationships = {
	.yarns = @"yarns",
};

const struct SupplierFetchedProperties SupplierFetchedProperties = {
};

@implementation SupplierID
@end

@implementation _Supplier

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Supplier" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Supplier";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Supplier" inManagedObjectContext:moc_];
}

- (SupplierID*)objectID {
	return (SupplierID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	
	if ([key isEqualToString:@"codeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"code"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}




@dynamic code;



- (int32_t)codeValue {
	NSNumber *result = [self code];
	return [result intValue];
}

- (void)setCodeValue:(int32_t)value_ {
	[self setCode:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveCodeValue {
	NSNumber *result = [self primitiveCode];
	return [result intValue];
}

- (void)setPrimitiveCodeValue:(int32_t)value_ {
	[self setPrimitiveCode:[NSNumber numberWithInt:value_]];
}





@dynamic item_description;






@dynamic language;






@dynamic string_code;






@dynamic yarns;

	
- (NSMutableSet*)yarnsSet {
	[self willAccessValueForKey:@"yarns"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"yarns"];
  
	[self didAccessValueForKey:@"yarns"];
	return result;
}
	






@end
