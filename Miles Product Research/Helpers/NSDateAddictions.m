//
//  NSDateAddictions.m
//  NeedleIOS
//
//  Created by Davide Cenzi on 06/06/13.
//  Copyright (c) 2013 Davide Cenzi. All rights reserved.
//

#import "NSDateAddictions.h"

@implementation NSDate (WebFormattedDate)

- (NSString*)formattedDate{
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setLocale:[NSLocale currentLocale]];
    //[df setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:]];
    NSLog(@"Converted date (before timezone): %@",[df stringFromDate:self]);
    
    [df setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT+2"]];
    [df setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    //return [df stringFromDate:self];
    NSLog(@"Converted date(after timezone): %@",[df stringFromDate:self]);


    int stamp = [self timeIntervalSince1970];
    return [NSString stringWithFormat:@"%d", stamp];
}

@end
