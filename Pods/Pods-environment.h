
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// AFNetworking
#define COCOAPODS_POD_AVAILABLE_AFNetworking
#define COCOAPODS_VERSION_MAJOR_AFNetworking 1
#define COCOAPODS_VERSION_MINOR_AFNetworking 3
#define COCOAPODS_VERSION_PATCH_AFNetworking 4

// AJNotificationView
#define COCOAPODS_POD_AVAILABLE_AJNotificationView
#define COCOAPODS_VERSION_MAJOR_AJNotificationView 0
#define COCOAPODS_VERSION_MINOR_AJNotificationView 7
#define COCOAPODS_VERSION_PATCH_AJNotificationView 0

// ECSlidingViewController
#define COCOAPODS_POD_AVAILABLE_ECSlidingViewController
#define COCOAPODS_VERSION_MAJOR_ECSlidingViewController 2
#define COCOAPODS_VERSION_MINOR_ECSlidingViewController 0
#define COCOAPODS_VERSION_PATCH_ECSlidingViewController 2

// GCTagList
#define COCOAPODS_POD_AVAILABLE_GCTagList
#define COCOAPODS_VERSION_MAJOR_GCTagList 1
#define COCOAPODS_VERSION_MINOR_GCTagList 3
#define COCOAPODS_VERSION_PATCH_GCTagList 0

// ISO8601DateFormatterValueTransformer
#define COCOAPODS_POD_AVAILABLE_ISO8601DateFormatterValueTransformer
#define COCOAPODS_VERSION_MAJOR_ISO8601DateFormatterValueTransformer 0
#define COCOAPODS_VERSION_MINOR_ISO8601DateFormatterValueTransformer 5
#define COCOAPODS_VERSION_PATCH_ISO8601DateFormatterValueTransformer 0

// MDRadialProgress
#define COCOAPODS_POD_AVAILABLE_MDRadialProgress
#define COCOAPODS_VERSION_MAJOR_MDRadialProgress 1
#define COCOAPODS_VERSION_MINOR_MDRadialProgress 0
#define COCOAPODS_VERSION_PATCH_MDRadialProgress 6

// NewRelicAgent
#define COCOAPODS_POD_AVAILABLE_NewRelicAgent
#define COCOAPODS_VERSION_MAJOR_NewRelicAgent 1
#define COCOAPODS_VERSION_MINOR_NewRelicAgent 396
#define COCOAPODS_VERSION_PATCH_NewRelicAgent 0

// OpenUDID
#define COCOAPODS_POD_AVAILABLE_OpenUDID
#define COCOAPODS_VERSION_MAJOR_OpenUDID 1
#define COCOAPODS_VERSION_MINOR_OpenUDID 0
#define COCOAPODS_VERSION_PATCH_OpenUDID 0

// RATreeView
#define COCOAPODS_POD_AVAILABLE_RATreeView
#define COCOAPODS_VERSION_MAJOR_RATreeView 0
#define COCOAPODS_VERSION_MINOR_RATreeView 1
#define COCOAPODS_VERSION_PATCH_RATreeView 5

// RKValueTransformers
#define COCOAPODS_POD_AVAILABLE_RKValueTransformers
#define COCOAPODS_VERSION_MAJOR_RKValueTransformers 1
#define COCOAPODS_VERSION_MINOR_RKValueTransformers 0
#define COCOAPODS_VERSION_PATCH_RKValueTransformers 1

// RestKit
#define COCOAPODS_POD_AVAILABLE_RestKit
#define COCOAPODS_VERSION_MAJOR_RestKit 0
#define COCOAPODS_VERSION_MINOR_RestKit 22
#define COCOAPODS_VERSION_PATCH_RestKit 0

// RestKit/Core
#define COCOAPODS_POD_AVAILABLE_RestKit_Core
#define COCOAPODS_VERSION_MAJOR_RestKit_Core 0
#define COCOAPODS_VERSION_MINOR_RestKit_Core 22
#define COCOAPODS_VERSION_PATCH_RestKit_Core 0

// RestKit/CoreData
#define COCOAPODS_POD_AVAILABLE_RestKit_CoreData
#define COCOAPODS_VERSION_MAJOR_RestKit_CoreData 0
#define COCOAPODS_VERSION_MINOR_RestKit_CoreData 22
#define COCOAPODS_VERSION_PATCH_RestKit_CoreData 0

// RestKit/Network
#define COCOAPODS_POD_AVAILABLE_RestKit_Network
#define COCOAPODS_VERSION_MAJOR_RestKit_Network 0
#define COCOAPODS_VERSION_MINOR_RestKit_Network 22
#define COCOAPODS_VERSION_PATCH_RestKit_Network 0

// RestKit/ObjectMapping
#define COCOAPODS_POD_AVAILABLE_RestKit_ObjectMapping
#define COCOAPODS_VERSION_MAJOR_RestKit_ObjectMapping 0
#define COCOAPODS_VERSION_MINOR_RestKit_ObjectMapping 22
#define COCOAPODS_VERSION_PATCH_RestKit_ObjectMapping 0

// RestKit/Support
#define COCOAPODS_POD_AVAILABLE_RestKit_Support
#define COCOAPODS_VERSION_MAJOR_RestKit_Support 0
#define COCOAPODS_VERSION_MINOR_RestKit_Support 22
#define COCOAPODS_VERSION_PATCH_RestKit_Support 0

// SDWebImage
#define COCOAPODS_POD_AVAILABLE_SDWebImage
#define COCOAPODS_VERSION_MAJOR_SDWebImage 3
#define COCOAPODS_VERSION_MINOR_SDWebImage 6
#define COCOAPODS_VERSION_PATCH_SDWebImage 0

// SDWebImage/Core
#define COCOAPODS_POD_AVAILABLE_SDWebImage_Core
#define COCOAPODS_VERSION_MAJOR_SDWebImage_Core 3
#define COCOAPODS_VERSION_MINOR_SDWebImage_Core 6
#define COCOAPODS_VERSION_PATCH_SDWebImage_Core 0

// SOCKit
#define COCOAPODS_POD_AVAILABLE_SOCKit
#define COCOAPODS_VERSION_MAJOR_SOCKit 1
#define COCOAPODS_VERSION_MINOR_SOCKit 1
#define COCOAPODS_VERSION_PATCH_SOCKit 0

// SSZipArchive
#define COCOAPODS_POD_AVAILABLE_SSZipArchive
#define COCOAPODS_VERSION_MAJOR_SSZipArchive 0
#define COCOAPODS_VERSION_MINOR_SSZipArchive 3
#define COCOAPODS_VERSION_PATCH_SSZipArchive 1

// SVProgressHUD
#define COCOAPODS_POD_AVAILABLE_SVProgressHUD
#define COCOAPODS_VERSION_MAJOR_SVProgressHUD 1
#define COCOAPODS_VERSION_MINOR_SVProgressHUD 0
#define COCOAPODS_VERSION_PATCH_SVProgressHUD 0

// TestFlightSDK
#define COCOAPODS_POD_AVAILABLE_TestFlightSDK
#define COCOAPODS_VERSION_MAJOR_TestFlightSDK 3
#define COCOAPODS_VERSION_MINOR_TestFlightSDK 0
#define COCOAPODS_VERSION_PATCH_TestFlightSDK 2

// TransitionKit
#define COCOAPODS_POD_AVAILABLE_TransitionKit
#define COCOAPODS_VERSION_MAJOR_TransitionKit 2
#define COCOAPODS_VERSION_MINOR_TransitionKit 0
#define COCOAPODS_VERSION_PATCH_TransitionKit 0

// WTGlyphFontSet
#define COCOAPODS_POD_AVAILABLE_WTGlyphFontSet
#define COCOAPODS_VERSION_MAJOR_WTGlyphFontSet 0
#define COCOAPODS_VERSION_MINOR_WTGlyphFontSet 6
#define COCOAPODS_VERSION_PATCH_WTGlyphFontSet 0

// WTGlyphFontSet/core
#define COCOAPODS_POD_AVAILABLE_WTGlyphFontSet_core
#define COCOAPODS_VERSION_MAJOR_WTGlyphFontSet_core 0
#define COCOAPODS_VERSION_MINOR_WTGlyphFontSet_core 6
#define COCOAPODS_VERSION_PATCH_WTGlyphFontSet_core 0

