//
//  BTOverlayImageView.h
//  MSR
//
//  Created by Davide Cenzi on 10/01/14.
//  Copyright (c) 2014 BlueTouch Development di Davide Cenzi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "UIView+ALQuickFrame.h"

@interface BTOverlayImageView : UIImageView{
    
    double current_value;
    double new_to_value;
    
    BOOL IsAnimationInProgress;
}

- (double)progress;
- (void)setProgress:(double)value;
- (void)setOverlayColor:(UIColor*)color;

@end
