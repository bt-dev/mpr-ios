//
//  AppDelegate.m
//  Miles Product Research
//
//  Created by Davide Cenzi on 25/06/14.
//  Copyright (c) 2014 BlueTouch Development di Davide Cenzi. All rights reserved.
//

#import "AppDelegate.h"
#import "BTDataSync.h"
#import "BTSeeder.h"

@interface AppDelegate (){
    NSString *previous_connectivity;
}

- (void)handleNotification:(NSDictionary *)userInfo;
- (void)reloadUI;

//connectivity tracking
- (NSArray*)notificationNames;
- (void)startListeningForNotifications;
- (void)stopListeningForNotifications;
- (void)handleLocalNotification:(NSNotification *)notification;

@end

@implementation AppDelegate
@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;



#pragma mark - Extra methods



- (void)customizeAppearance{
    //[[UILabel appearance] setFont:[UIFont fontWithName:@"Futura-CondensedLight" size:16.0]];

    [[UINavigationBar appearance] setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:1.0],
      NSForegroundColorAttributeName,
      [UIFont fontWithName:@"FuturaLT-CondensedLight" size:21.0],
      NSFontAttributeName,
      nil]];
    
}

- (void)handleNotification:(NSDictionary *)userInfo{
    
#ifdef Parse
    if ([userInfo objectForKey:@"ref"]) {
        
        //Parse framework auto notify on push
        [PFPush handlePush:userInfo];
        [PFInstallation currentInstallation].badge = 0;
        
        NSLog(@"\n\n\n\nReceived push notification\n\n\n\n");
        
        NSDictionary *dict = [userInfo objectForKey:@"ref"];
        if(dict != nil){
            NSLog(@"Messaggio: %@",[dict valueForKey:@"alert"]);
            [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
        }
    }
#endif
}

- (void)setupApplicationWithOptions:(NSDictionary *)launchOptions forEnvironment:(NSString*)environment{
    
    //Automatically track if loading requests and start / stops activity indicators
    [AFNetworkActivityIndicatorManager sharedManager].enabled = YES;
    
    [[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeBadge |UIRemoteNotificationTypeSound)];
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    
    //setting up log level
    
    //LogTrace(@"Simple message");
    //LogTrace(@"Message with %i argument...oops I mean %.1f arguments", 1, 2.0f);
    //LogInfo(@"Do you object to this %@ date object?", [NSDate new]);
    //LogDebug(@"I'll take this out if ever I solve this bug");
    //LogError(@"Don't do that: %@", [NSException exceptionWithName: @"CRYING" reason: @"Spilled milk" userInfo:nil]);
    [WTGlyphFontSet setDefaultFontSetName: @"fontawesome"];
    
#ifdef LOGGING_ENABLED
    
    if (LOGGING_ENABLED == 1)
        NSLog(@"\n\n\n#############################\n# WARNING - LOGGING ENABLED #\n#############################\n\n\n");
#endif
    
#ifdef Parse
    //setup Parse backend
    [Parse setApplicationId:@"cxqPCuVgWLfJo7hVUSNJjbzv3Ww2JGR2jTAJi90R"
                  clientKey:@"ySQxW3zk4HICTswPF3byrECCMy4AyTmlmeQf0wcZ"];
    [PFAnalytics trackAppOpenedWithLaunchOptions:launchOptions];
    
    NSDictionary* userInfo = [launchOptions valueForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    
    //Parse framework auto notify on push
    [PFPush handlePush:userInfo];
#endif
    
    
    
    [self customizeAppearance];
    
    //check in seeding is needed
    if (true){
        
        BTSeeder *seeder = [[BTDataSync shared] seeder];
        [seeder importDatabaseFromFile:@"MPR" onComplete:^(NSError *error) {
            if(error){
                UIAlertView *al = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Database import",@"") message:NSLocalizedString(@"Unable to complete database import",@"") delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                [al show];
                NSLog(@"Error seeding database: %@",error.localizedDescription);
            }
            else
                NSLog(@"Database seeding completed.");
            
            double delayInSeconds = 2.0;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                [[NSNotificationCenter defaultCenter] postNotificationName:@"SEEDING_COMPLETED" object:nil];
            });
            
        }];
        
    }
    else{
        NSLog(@"-- DATABASE SEEDING SKIPPED --");
        
//        BTSeeder *seeder = [[BTDataSync shared] seeder];
//        [seeder seedDatabaseFromFile:@"it.products" onComplete:^(NSError *error) {
//            if(error){
//                UIAlertView *al = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Database import",@"") message:NSLocalizedString(@"Unable to complete database import",@"") delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
//                [al show];
//                NSLog(@"Error seeding database: %@",error.localizedDescription);
//            }
//            else
//                NSLog(@"Database seeding completed.");
//            
//            double delayInSeconds = 2.0;
//            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
//            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
//                [[NSNotificationCenter defaultCenter] postNotificationName:@"SEEDING_COMPLETED" object:nil];
//            });
//            
//        }];
        
    }
    //must be after
    [[BTDataSync shared] setEnvironment:environment];
    [[BTDataSync shared] setForceRemoteAssets:NO];
    
    if([[BTDataSync shared] forceRemoteAssets])
        NSLog(@"Warning: forcing remote assets.");
    
    //notifico che l'app è in development
    if(![environment isEqualToString:@"production"]){
        
        double delayInSeconds = 2.0;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            
            [AJNotificationView showNoticeInView:self.window.rootViewController.view
                                            type:AJNotificationTypeBlue
                                           title:[NSString stringWithFormat:@"Warning: %@ environment",environment]
                                 linedBackground:AJLinedBackgroundTypeAnimated
                                       hideAfter:4.0];
            
        });
    }
    
    
    //listen to notifications
    //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadUI) name:@"RELOAD_UI" object:nil];
    
}

- (void) showSplash {
    CGRect rect = [UIScreen mainScreen].bounds;
    UIImageView *splashImage = [[UIImageView alloc] initWithFrame:rect];
    splashImage.image = [UIImage imageNamed:@"splash"];
    
    UIViewController *v = [[UIViewController alloc] init];
    [v.view setFrame:rect];
    [v.view addSubview:splashImage];
    
    self.window.rootViewController = v;
    
}

- (void) removeSplash {
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    self.window.rootViewController = [sb instantiateInitialViewController];
}

- (void)reloadUI{
    
    self.window.rootViewController = nil;
    
    [self showSplash];
    
    double delayInSeconds = 2.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [self removeSplash];
    });
    
    
}

#pragma mark -


#pragma mark - Push notification registration callbacks

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error{
    
    if ([error code] == 3010) // 3010 is for the iPhone Simulator
    {
        // show some alert or otherwise handle the failure to register.
        NSString *token = @"myfaketoken";
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:token forKey:@"device_token"];
        [defaults synchronize];
        
        [[BTDataSync shared] registerDevice:token];
        
    }
    else{
        NSLog(@"Unable to register to push notifications service");
    }
    
}

- (void)application:(UIApplication *)app didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    
    NSLog(@"Successfully registered for remote notifications");
    
    NSString *token = [NSString stringWithFormat:@"%@",deviceToken];
    token = [token stringByReplacingOccurrencesOfString:@" " withString:@""];
    token = [token stringByReplacingOccurrencesOfString:@"<" withString:@""];
    token = [token stringByReplacingOccurrencesOfString:@">" withString:@""];
    
    NSLog(@"device Token: %@",token);
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:token forKey:@"device_token"];
    [defaults synchronize];
    
    //verifico se ho le credenziali per la webApp
    [[BTDataSync shared] registerDevice:token];
    
    
}

-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo{
    
    NSLog(@"\n\n\n\nReceived push notification\n\n\n\n");
    [self handleNotification:userInfo];
    
}

#pragma mark -

#pragma mark - Notifications management
- (NSArray*)notificationNames{
    return @[@"CONNECTED", @"NOT_CONNECTED"];
}

- (void)startListeningForNotifications{
    
    for (NSString *message in self.notificationNames) {
        
        LogTrace(@"Registering App delegate for '%@' notifications",message);
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(handleLocalNotification:)
                                                     name:message
                                                   object:nil];
    }
    
}

- (void)stopListeningForNotifications{
    
    for (NSString *message in self.notificationNames) {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:message object:nil];
    }
    
}

- (void)handleLocalNotification:(NSNotification *)notification{
    /*
     if([notification.name isEqualToString:@"NOT_CONNECTED"]){
     
     [AJNotificationView showNoticeInView:self.window.rootViewController.view
     type:AJNotificationTypeRed
     title:NSLocalizedString(@"not_connected", @"disconnected messager")
     linedBackground:AJLinedBackgroundTypeAnimated
     hideAfter:3.0];
     }
     else if([notification.name isEqualToString:@"CONNECTED"] && ([previous_connectivity isEqualToString:@"NOT_CONNECTED"])){
     
     [AJNotificationView showNoticeInView:self.window.rootViewController.view
     type:AJNotificationTypeGreen
     title:NSLocalizedString(@"connected", @"connected messager")
     linedBackground:AJLinedBackgroundTypeAnimated
     hideAfter:2.0];
     }
     */
    previous_connectivity = notification.name;
}

#pragma mark -


- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url
{
    // Inserisci qui le istruzioni da eseguire
    NSLog(@"Url scheme: %@",[url scheme]);
    //return [[url scheme] isEqualToString:@"msr"];
    return NO;
}


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    previous_connectivity = @"";
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [self customizeAppearance];
    [self setupApplicationWithOptions:launchOptions forEnvironment:@"production"];
    [self startListeningForNotifications];
    
    [TestFlight takeOff:@"5c848b65-d313-4aa0-8020-517e4934ef31"];
    
    // Override point for customization after application launch.
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    [self saveContext];
}

- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

#pragma mark - Core Data stack

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"MPR" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"MPR.sqlite"];
    
    NSError *error = nil;
    
    /* MIGRATION SUPPORT */
    NSDictionary *options = @{
                              NSMigratePersistentStoresAutomaticallyOption : @YES,
                              NSInferMappingModelAutomaticallyOption : @YES
                              };
    /********************/
    
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:options error:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
         
         Typical reasons for an error here include:
         * The persistent store is not accessible;
         * The schema for the persistent store is incompatible with current managed object model.
         Check the error message to determine what the actual problem was.
         
         
         If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
         
         If you encounter schema incompatibility errors during development, you can reduce their frequency by:
         * Simply deleting the existing store:
         [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
         
         * Performing automatic lightweight migration by passing the following dictionary as the options parameter:
         @{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES}
         
         Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
         
         */
        [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil];
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        //abort();
    }
    
    return _persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

@end
