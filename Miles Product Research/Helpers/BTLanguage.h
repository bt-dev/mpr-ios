//
//  BTLanguage.h
//  MSR
//
//  Created by Davide Cenzi on 12/12/13.
//  Copyright (c) 2013 BlueTouch Development di Davide Cenzi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BTLanguage : NSObject

@property (nonatomic, assign) UIBackgroundTaskIdentifier backgroundUpdateTask;

+ (instancetype) shared;

- (void)changeLanguageTo:(NSString*)languageId withReboot:(BOOL)withReboot;
- (NSString*)currentLanguage;

@end
