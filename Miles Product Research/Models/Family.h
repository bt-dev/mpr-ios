#import "_Family.h"
#import "S2SOrderedDictionary.h"

@interface Family : _Family {}
// Custom logic goes here.
+ (S2SOrderedDictionary*)filterValues;
@end
