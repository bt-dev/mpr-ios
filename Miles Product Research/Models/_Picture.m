// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Picture.m instead.

#import "_Picture.h"

const struct PictureAttributes PictureAttributes = {
	.code = @"code",
	.image_file_name = @"image_file_name",
	.image_updated = @"image_updated",
	.item_description = @"item_description",
	.language = @"language",
	.product_code = @"product_code",
	.sketch = @"sketch",
	.url = @"url",
};

const struct PictureRelationships PictureRelationships = {
	.product = @"product",
};

const struct PictureFetchedProperties PictureFetchedProperties = {
};

@implementation PictureID
@end

@implementation _Picture

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Picture" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Picture";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Picture" inManagedObjectContext:moc_];
}

- (PictureID*)objectID {
	return (PictureID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	
	if ([key isEqualToString:@"codeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"code"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"image_updatedValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"image_updated"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"product_codeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"product_code"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"sketchValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"sketch"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}




@dynamic code;



- (int32_t)codeValue {
	NSNumber *result = [self code];
	return [result intValue];
}

- (void)setCodeValue:(int32_t)value_ {
	[self setCode:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveCodeValue {
	NSNumber *result = [self primitiveCode];
	return [result intValue];
}

- (void)setPrimitiveCodeValue:(int32_t)value_ {
	[self setPrimitiveCode:[NSNumber numberWithInt:value_]];
}





@dynamic image_file_name;






@dynamic image_updated;



- (BOOL)image_updatedValue {
	NSNumber *result = [self image_updated];
	return [result boolValue];
}

- (void)setImage_updatedValue:(BOOL)value_ {
	[self setImage_updated:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveImage_updatedValue {
	NSNumber *result = [self primitiveImage_updated];
	return [result boolValue];
}

- (void)setPrimitiveImage_updatedValue:(BOOL)value_ {
	[self setPrimitiveImage_updated:[NSNumber numberWithBool:value_]];
}





@dynamic item_description;






@dynamic language;






@dynamic product_code;



- (int32_t)product_codeValue {
	NSNumber *result = [self product_code];
	return [result intValue];
}

- (void)setProduct_codeValue:(int32_t)value_ {
	[self setProduct_code:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveProduct_codeValue {
	NSNumber *result = [self primitiveProduct_code];
	return [result intValue];
}

- (void)setPrimitiveProduct_codeValue:(int32_t)value_ {
	[self setPrimitiveProduct_code:[NSNumber numberWithInt:value_]];
}





@dynamic sketch;



- (BOOL)sketchValue {
	NSNumber *result = [self sketch];
	return [result boolValue];
}

- (void)setSketchValue:(BOOL)value_ {
	[self setSketch:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveSketchValue {
	NSNumber *result = [self primitiveSketch];
	return [result boolValue];
}

- (void)setPrimitiveSketchValue:(BOOL)value_ {
	[self setPrimitiveSketch:[NSNumber numberWithBool:value_]];
}





@dynamic url;






@dynamic product;

	






@end
