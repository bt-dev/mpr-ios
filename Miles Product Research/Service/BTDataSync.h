//
//  BTDataSync.h
//  MobileCatalogTemplate
//
//  Created by Davide Cenzi on 22/06/13.
//  Copyright (c) 2013 BlueTouch Development di Davide Cenzi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFHTTPClient.h>
#import <AFNetworking.h>
#import <RestKit/RestKit.h>
#import "SVProgressHUD.h"
#include "BTNetworking.h"
#import "BTLanguage.h"
#import "BTSeeder.h"

@interface BTDataSync : NSObject{
    NSString *_environment;
    BTNetworking *networking;
    NSString *_activeLanguage;
    NSNumber *_curr_user_id;
    NSString *_deviceToken;
    NSString *_apiKey;
    AFNetworkReachabilityStatus _network_status;
}

//@property (nonatomic, strong) BTDataModel *dataModel;
@property (nonatomic, strong) NSManagedObjectContext * context;
@property (assign) BOOL forceRemoteAssets;

+ (BTDataSync*) shared;

/* UTILITY METHODS */

- (NSString*)environment;
- (void)setEnvironment:(NSString*)env;
- (void)registerDevice:(NSString*)deviceToken;
- (void)setHeaders;

- (NSString*)apiURL;
- (NSString*)language;
- (void)setLanguage:(NSString*)language;
- (NSArray*)availableLanguages;
- (void) sendPushNotificationtoUserWithID:(NSString*)userID withMessage:(NSString*)message;

/* User account stuff */
- (NSNumber*)currentUserID;
- (int)current_user_id;
- (BOOL)adminUser;


//called after user registration
- (void)signedInUser:(User*)me;

/* NOT USED ACTUALLY */
- (User*)currentUser;
- (BOOL)loggedIn;
- (void)loginWithEmail:(NSString*)email andPassword:(NSString*)pass;
- (void)logout;
- (void)resetPasswordForEmail:(NSString*)email;
/* NOT USED ACTUALLY */

- (BOOL)signInWithUsername:(NSString*)username andPassword:(NSString*)password;
- (void)signOut;
- (BOOL)signedIn;

/* Seeding database / assets methods */
- (void)syncWithRemote;
- (BOOL)existImageBundle;
- (BOOL)existImagesInDocuments;

- (void)retrieveImages;


- (void)downloadAllImages;
- (void)downloadUpdatedImages;
- (BTSeeder *)seeder;

/* User settings wrapper */
- (BOOL)saveSettingValue:(id)value forKey:(NSString*)key;
- (id)settingsValueForKey:(NSString*)key;

/* User settings helper methods */
- (NSDate*)databaseLastUpdate;
- (NSDate*)stitchesPicturesLastUpdate;
- (NSDate*)productsPicturesLastUpdate;
- (NSDate*)productsSketchesLastUpdate;







/* RestKit Methods */
- (RKObjectManager*)manager;
- (BOOL)networkAvailable;

/* CRUD VERBS METHODS */
- (void)getObjectAsync:(NSManagedObject *)object withParams:(NSDictionary*)params;
- (void)listObjectsForClass:(Class)class atPath:(NSString*)customPath forAction:(NSString*)customActioName withParams:(NSDictionary*)params;

- (void)listObjectsForClass:(Class)class atPath:(NSString*)customPath forAction:(NSString*)customActioName withParams:(NSDictionary*)params onSuccess:(void (^)(NSArray *objects))successBlock onFailure:(void (^)(NSError *error))failureBlock;

- (void)saveObjectAsync:(NSManagedObject *)object;
- (void)saveObjectAsync:(NSManagedObject *)object withParams:(NSDictionary*)params;
- (void)saveObjectAsync:(NSManagedObject *)object withParams:(NSDictionary*)params withAttachments:(NSDictionary*)attachments mimeType:(NSString*)mimetype;
- (void)deleteObjectAsync:(NSManagedObject *)object;

/* ADDITIONAL METHODS FOR NON-REST ROUTES */
- (void)requestDataAtPath:(NSString*)path forAction:(NSString*)actionName withMethod:(RKRequestMethod)method withParams:(NSDictionary*)params;

- (void)requestDataAtPath:(NSString*)path forAction:(NSString*)actionName withMethod:(RKRequestMethod)method withParams:(NSDictionary*)params successBlock:(void (^)(RKObjectRequestOperation *operation, RKMappingResult *mappingResult))success failureBlock:(void (^)(RKObjectRequestOperation *operation, NSError *error))failure;

@end
