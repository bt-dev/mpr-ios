// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to ProductFibre.m instead.

#import "_ProductFibre.h"

const struct ProductFibreAttributes ProductFibreAttributes = {
	.code = @"code",
	.fibre_code = @"fibre_code",
	.language = @"language",
	.percentage = @"percentage",
	.product_code = @"product_code",
};

const struct ProductFibreRelationships ProductFibreRelationships = {
	.fibre = @"fibre",
	.product = @"product",
};

const struct ProductFibreFetchedProperties ProductFibreFetchedProperties = {
};

@implementation ProductFibreID
@end

@implementation _ProductFibre

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"ProductFibre" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"ProductFibre";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"ProductFibre" inManagedObjectContext:moc_];
}

- (ProductFibreID*)objectID {
	return (ProductFibreID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	
	if ([key isEqualToString:@"codeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"code"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"fibre_codeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"fibre_code"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"percentageValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"percentage"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"product_codeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"product_code"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}




@dynamic code;



- (int32_t)codeValue {
	NSNumber *result = [self code];
	return [result intValue];
}

- (void)setCodeValue:(int32_t)value_ {
	[self setCode:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveCodeValue {
	NSNumber *result = [self primitiveCode];
	return [result intValue];
}

- (void)setPrimitiveCodeValue:(int32_t)value_ {
	[self setPrimitiveCode:[NSNumber numberWithInt:value_]];
}





@dynamic fibre_code;



- (int32_t)fibre_codeValue {
	NSNumber *result = [self fibre_code];
	return [result intValue];
}

- (void)setFibre_codeValue:(int32_t)value_ {
	[self setFibre_code:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveFibre_codeValue {
	NSNumber *result = [self primitiveFibre_code];
	return [result intValue];
}

- (void)setPrimitiveFibre_codeValue:(int32_t)value_ {
	[self setPrimitiveFibre_code:[NSNumber numberWithInt:value_]];
}





@dynamic language;






@dynamic percentage;



- (float)percentageValue {
	NSNumber *result = [self percentage];
	return [result floatValue];
}

- (void)setPercentageValue:(float)value_ {
	[self setPercentage:[NSNumber numberWithFloat:value_]];
}

- (float)primitivePercentageValue {
	NSNumber *result = [self primitivePercentage];
	return [result floatValue];
}

- (void)setPrimitivePercentageValue:(float)value_ {
	[self setPrimitivePercentage:[NSNumber numberWithFloat:value_]];
}





@dynamic product_code;



- (int32_t)product_codeValue {
	NSNumber *result = [self product_code];
	return [result intValue];
}

- (void)setProduct_codeValue:(int32_t)value_ {
	[self setProduct_code:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveProduct_codeValue {
	NSNumber *result = [self primitiveProduct_code];
	return [result intValue];
}

- (void)setPrimitiveProduct_codeValue:(int32_t)value_ {
	[self setPrimitiveProduct_code:[NSNumber numberWithInt:value_]];
}





@dynamic fibre;

	

@dynamic product;

	






@end
