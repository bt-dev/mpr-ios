//
//  MSRTreeViewController.h
//  MSR
//
//  Created by Davide Cenzi on 11/12/13.
//  Copyright (c) 2013 BlueTouch Development di Davide Cenzi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RATreeView.h"
#import "BTNoficationViewController.h"

@protocol TreeViewDelegate <NSObject>

@required

- (void)selectedStitchType:(StitchType*)stitchType;

@end

@interface MSRTreeViewController : BTNoficationViewController <RATreeViewDataSource,RATreeViewDelegate, UIScrollViewDelegate>

@property (nonatomic, weak) RATreeView *treeView;
@property (nonatomic, strong) id <TreeViewDelegate> delegate;
@property (strong, nonatomic) StitchType *expanded;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

- (IBAction)dismiss:(id)sender;

@end
