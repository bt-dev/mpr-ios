//
//  MSRStitchCell.h
//  MSR
//
//  Created by Davide Cenzi on 21/12/13.
//  Copyright (c) 2013 BlueTouch Development di Davide Cenzi. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ProductCellDelegate <NSObject>

@required

- (void)toggledSelectionForCode:(NSNumber*)code;

@end

@interface MSRProductCell : UICollectionViewCell{
    BOOL _active;
}

@property (nonatomic, strong) NSNumber *productCode;
@property (nonatomic, strong) id <ProductCellDelegate> delegate;

@property (nonatomic, weak) IBOutlet UIImageView *productImage;
@property (nonatomic, weak) IBOutlet UIImageView *activeImage;
@property (nonatomic, weak) IBOutlet UILabel *seasonLabel;
@property (nonatomic, weak) IBOutlet UILabel *productNameLabel;
@property (nonatomic, weak) IBOutlet UILabel *supplierLabel;
@property (nonatomic, weak) IBOutlet UILabel *categoryLabel;
@property (nonatomic, weak) IBOutlet UILabel *modelLabel;

- (BOOL)isActive;
- (void)setActive:(BOOL)active;
- (IBAction)toggleActive:(id)sender;
@end
