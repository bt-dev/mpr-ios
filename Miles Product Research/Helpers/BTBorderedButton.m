//
//  BTBorderedButton.m
//  RovigoBanca
//
//  Created by Davide Cenzi on 14/06/14.
//  Copyright (c) 2014 BlueTouch Development di Davide Cenzi. All rights reserved.
//

#import "BTBorderedButton.h"

@implementation BTBorderedButton{
    UIColor *_borderColor;
}

- (void)awakeFromNib{
    if(!_borderColor)
        _borderColor = [UIColor whiteColor];
    
    [self setBorderColor:_borderColor];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setBorderColor:(UIColor*)color{

    self.tintColor = color;
    [self setTitleColor:color forState:UIControlStateNormal];
    
    CALayer *l = self.layer;
    [l setBorderWidth:2.0];
    [l setBorderColor:color.CGColor];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
