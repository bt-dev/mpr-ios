#import "_Brand.h"
#import "S2SOrderedDictionary.h"

@interface Brand : _Brand {}
// Custom logic goes here.
+ (S2SOrderedDictionary*)filterValues;
@end
