//
//  MSRProductDetailViewController.h
//  MSR
//
//  Created by Davide Cenzi on 19/01/14.
//  Copyright (c) 2014 BlueTouch Development di Davide Cenzi. All rights reserved.
//

#import "BTNoficationViewController.h"
#import "BTBorderedButton.h"

@interface MSRProductDetailViewController : BTNoficationViewController

@property (weak, nonatomic) IBOutlet UIView *model_wrapper;
@property (strong, nonatomic) NSArray *choosenProducts;

//product attributes

@property (weak, nonatomic) IBOutlet UILabel *bigTitleLabel;

@property (weak, nonatomic) IBOutlet UILabel *modelLabelTitle;
@property (weak, nonatomic) IBOutlet UILabel *categoryLabelTitle;
@property (weak, nonatomic) IBOutlet UILabel *seasonLabelTitle;
@property (weak, nonatomic) IBOutlet UILabel *brandTypeLabelTitle;
@property (weak, nonatomic) IBOutlet UILabel *brandLabelTitle;
@property (weak, nonatomic) IBOutlet UILabel *supplierLabelTitle;

@property (weak, nonatomic) IBOutlet UILabel *yarnLabelTitle;

@property (weak, nonatomic) IBOutlet UILabel *compositionLabelTitle;

@property (weak, nonatomic) IBOutlet UILabel *stitchTypeLabelTitle;

@property (weak, nonatomic) IBOutlet UILabel *thinnessLabelTitle;

@property (weak, nonatomic) IBOutlet UILabel *modelLabel;
@property (weak, nonatomic) IBOutlet UILabel *categoryLabel;
@property (weak, nonatomic) IBOutlet UILabel *seasonLabel;
@property (weak, nonatomic) IBOutlet UILabel *brandTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *brandLabel;
@property (weak, nonatomic) IBOutlet UILabel *supplierLabel;

@property (weak, nonatomic) IBOutlet UILabel *yarnLabel0;
@property (weak, nonatomic) IBOutlet UILabel *yarnLabel1;
@property (weak, nonatomic) IBOutlet UILabel *yarnLabel2;

@property (weak, nonatomic) IBOutlet UILabel *compositionLabel0;
@property (weak, nonatomic) IBOutlet UILabel *compositionLabel1;
@property (weak, nonatomic) IBOutlet UILabel *compositionLabel2;

@property (weak, nonatomic) IBOutlet UILabel *stitchTypeLabel0;
@property (weak, nonatomic) IBOutlet UILabel *stitchTypeLabel1;
@property (weak, nonatomic) IBOutlet UILabel *stitchTypeLabel2;

@property (weak, nonatomic) IBOutlet UILabel *thinnessLabel;


@property (strong, nonatomic) NSString *brandName;
@property (weak, nonatomic) IBOutlet UIView *brandView;

//new attributes
@property(weak, nonatomic) IBOutlet UIView *containerView;
@property(weak, nonatomic) IBOutlet UILabel *familyTitleLabel;
@property(weak, nonatomic) IBOutlet UILabel *familyLabel;

@property(weak, nonatomic) IBOutlet UILabel *shimaTitleLabel;
@property(weak, nonatomic) IBOutlet UILabel *shimaLabel;

@property(weak, nonatomic) IBOutlet UILabel *stollTitleLabel;
@property(weak, nonatomic) IBOutlet UILabel *stollLabel;

@property(weak, nonatomic) IBOutlet UILabel *archivedTitleLabel;
@property(weak, nonatomic) IBOutlet UILabel *archivedLabel;

@property(weak, nonatomic) IBOutlet UIView *roomContainer;
@property(weak, nonatomic) IBOutlet UILabel *roomTitleLabel;
@property(weak, nonatomic) IBOutlet UILabel *roomLabel;

@property(weak, nonatomic) IBOutlet UIView *positionContainer;
@property(weak, nonatomic) IBOutlet UILabel *positionTitleLabel;
@property(weak, nonatomic) IBOutlet UILabel *positionLabel;

@property (nonatomic, weak) IBOutlet UIScrollView *scrollView;
//-- product attributes

@property (nonatomic, weak) IBOutlet UIScrollView *sliderView;



@property (weak, nonatomic) IBOutlet UIImageView *productView;

@property (weak, nonatomic) IBOutlet BTBorderedButton *thumbButton0; //sketch?
@property (weak, nonatomic) IBOutlet BTBorderedButton *thumbButton1;
@property (weak, nonatomic) IBOutlet BTBorderedButton *thumbButton2;
@property (weak, nonatomic) IBOutlet BTBorderedButton *thumbButton3;
@property (weak, nonatomic) IBOutlet BTBorderedButton *thumbButton4;
@property (weak, nonatomic) IBOutlet BTBorderedButton *thumbButton5;
@property (weak, nonatomic) IBOutlet BTBorderedButton *thumbButton6;
@property (weak, nonatomic) IBOutlet BTBorderedButton *thumbButton7;
@property (weak, nonatomic) IBOutlet BTBorderedButton *thumbButton8;
@property (weak, nonatomic) IBOutlet BTBorderedButton *thumbButton9;

@property (weak, nonatomic) IBOutlet UIImageView *thumb0; //sketch?
@property (weak, nonatomic) IBOutlet UIImageView *thumb1;
@property (weak, nonatomic) IBOutlet UIImageView *thumb2;
@property (weak, nonatomic) IBOutlet UIImageView *thumb3;
@property (weak, nonatomic) IBOutlet UIImageView *thumb4;
@property (weak, nonatomic) IBOutlet UIImageView *thumb5;
@property (weak, nonatomic) IBOutlet UIImageView *thumb6;
@property (weak, nonatomic) IBOutlet UIImageView *thumb7;
@property (weak, nonatomic) IBOutlet UIImageView *thumb8;
@property (weak, nonatomic) IBOutlet UIImageView *thumb9;


- (IBAction)showPicture:(id)sender;
- (IBAction)chooseProduct:(id)sender;


@end
