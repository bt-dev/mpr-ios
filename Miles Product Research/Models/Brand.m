#import "Brand.h"


@interface Brand ()

// Private interface goes here.

@end


@implementation Brand

// Custom logic goes here.
+ (S2SOrderedDictionary*)filterValues{
    S2SOrderedDictionary *data = [[S2SOrderedDictionary alloc] init];
    
    [data setObject:@"" forKey:[NSString stringWithFormat:NSLocalizedString(@" -- All %@ --",@" -- Tutti i marchi -- "),NSLocalizedString(@"brands", @"brands")]];
    
    NSSortDescriptor *sortByName = [[NSSortDescriptor alloc] initWithKey:@"item_description" ascending:YES];
    NSFetchRequest *f = [Brand fetchRequest];
    [f setSortDescriptors:[NSArray arrayWithObject:sortByName]];
    
    NSMutableString *format = [[NSMutableString alloc] init];
    [format appendFormat:@"(code > 0) AND (language == '%@')",[[BTDataSync shared] language]];
    NSPredicate *p = [NSPredicate predicateWithFormat:format];
    [f setPredicate:p];
    
    
    for (Brand *tmp in [Brand objectsWithFetchRequest:f])
        [data setObject:tmp.code.stringValue forKey:tmp.item_description];
    
    return data;
}

@end
