#import "MacroSeason.h"


@interface MacroSeason ()

// Private interface goes here.

@end


@implementation MacroSeason

// Custom logic goes here.
+ (S2SOrderedDictionary*)filterValues{
    S2SOrderedDictionary *data = [[S2SOrderedDictionary alloc] init];
    
    [data setObject:@"" forKey:[NSString stringWithFormat:NSLocalizedString(@" -- All %@ --",@" -- Tutte le stagioni -- "),NSLocalizedString(@"seasons", @"seasons")]];
    
    NSSortDescriptor *sortByName = [[NSSortDescriptor alloc] initWithKey:@"item_description" ascending:YES];
    NSFetchRequest *f = [MacroSeason fetchRequest];
    [f setSortDescriptors:[NSArray arrayWithObject:sortByName]];
    
    NSMutableString *format = [[NSMutableString alloc] init];
    [format appendFormat:@"(code > 0) AND (language == '%@')",[[BTDataSync shared] language]];
    NSPredicate *p = [NSPredicate predicateWithFormat:format];
    [f setPredicate:p];
    
    for (MacroSeason *tmp in [MacroSeason objectsWithFetchRequest:f])
        [data setObject:tmp.code.stringValue forKey:tmp.item_description];
    
    return data;
}

@end
