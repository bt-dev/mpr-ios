//
//  MSRZoomViewController.h
//  MSR
//
//  Created by Davide Cenzi on 22/12/13.
//  Copyright (c) 2013 BlueTouch Development di Davide Cenzi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SSUIViewMiniMe.h"

@interface MSRZoomViewController : UIViewController<SSUIViewMiniMeDelegate>

@property (nonatomic,strong) UIImage *currentImage;
@property (nonatomic, weak) IBOutlet UIView *headerView;
@property (nonatomic, weak) IBOutlet UIButton *closeButton;
@property (nonatomic, weak) IBOutlet UILabel *titleLabel;

@property (nonatomic, strong) NSString *titleText;

@end
