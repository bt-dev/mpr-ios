#import "_BrandType.h"
#import "S2SOrderedDictionary.h"

@interface BrandType : _BrandType {}
// Custom logic goes here.
+ (S2SOrderedDictionary*)filterValues;
@end
