// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Picture.h instead.

#import <CoreData/CoreData.h>


extern const struct PictureAttributes {
	__unsafe_unretained NSString *code;
	__unsafe_unretained NSString *image_file_name;
	__unsafe_unretained NSString *image_updated;
	__unsafe_unretained NSString *item_description;
	__unsafe_unretained NSString *language;
	__unsafe_unretained NSString *product_code;
	__unsafe_unretained NSString *sketch;
	__unsafe_unretained NSString *url;
} PictureAttributes;

extern const struct PictureRelationships {
	__unsafe_unretained NSString *product;
} PictureRelationships;

extern const struct PictureFetchedProperties {
} PictureFetchedProperties;

@class Product;










@interface PictureID : NSManagedObjectID {}
@end

@interface _Picture : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (PictureID*)objectID;





@property (nonatomic, strong) NSNumber* code;



@property int32_t codeValue;
- (int32_t)codeValue;
- (void)setCodeValue:(int32_t)value_;

//- (BOOL)validateCode:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* image_file_name;



//- (BOOL)validateImage_file_name:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* image_updated;



@property BOOL image_updatedValue;
- (BOOL)image_updatedValue;
- (void)setImage_updatedValue:(BOOL)value_;

//- (BOOL)validateImage_updated:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* item_description;



//- (BOOL)validateItem_description:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* language;



//- (BOOL)validateLanguage:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* product_code;



@property int32_t product_codeValue;
- (int32_t)product_codeValue;
- (void)setProduct_codeValue:(int32_t)value_;

//- (BOOL)validateProduct_code:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* sketch;



@property BOOL sketchValue;
- (BOOL)sketchValue;
- (void)setSketchValue:(BOOL)value_;

//- (BOOL)validateSketch:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* url;



//- (BOOL)validateUrl:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) Product *product;

//- (BOOL)validateProduct:(id*)value_ error:(NSError**)error_;





@end

@interface _Picture (CoreDataGeneratedAccessors)

@end

@interface _Picture (CoreDataGeneratedPrimitiveAccessors)


- (NSNumber*)primitiveCode;
- (void)setPrimitiveCode:(NSNumber*)value;

- (int32_t)primitiveCodeValue;
- (void)setPrimitiveCodeValue:(int32_t)value_;




- (NSString*)primitiveImage_file_name;
- (void)setPrimitiveImage_file_name:(NSString*)value;




- (NSNumber*)primitiveImage_updated;
- (void)setPrimitiveImage_updated:(NSNumber*)value;

- (BOOL)primitiveImage_updatedValue;
- (void)setPrimitiveImage_updatedValue:(BOOL)value_;




- (NSString*)primitiveItem_description;
- (void)setPrimitiveItem_description:(NSString*)value;




- (NSString*)primitiveLanguage;
- (void)setPrimitiveLanguage:(NSString*)value;




- (NSNumber*)primitiveProduct_code;
- (void)setPrimitiveProduct_code:(NSNumber*)value;

- (int32_t)primitiveProduct_codeValue;
- (void)setPrimitiveProduct_codeValue:(int32_t)value_;




- (NSNumber*)primitiveSketch;
- (void)setPrimitiveSketch:(NSNumber*)value;

- (BOOL)primitiveSketchValue;
- (void)setPrimitiveSketchValue:(BOOL)value_;




- (NSString*)primitiveUrl;
- (void)setPrimitiveUrl:(NSString*)value;





- (Product*)primitiveProduct;
- (void)setPrimitiveProduct:(Product*)value;


@end
