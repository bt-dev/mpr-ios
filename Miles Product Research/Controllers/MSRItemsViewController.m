//
//  MSRItemsViewController.m
//  MSR
//
//  Created by Davide Cenzi on 13/12/13.
//  Copyright (c) 2013 BlueTouch Development di Davide Cenzi. All rights reserved.
//

#import "MSRItemsViewController.h"
#import "MSRProductCell.h"
#import "MSRDownloadViewController.h"
#import "MSRLockScreenViewController.h"
#import "MSRProductDetailViewController.h"
#import "MSRTreeViewController.h"
#import "BTCollectionHeaderView.h"
#import <WTGlyphFontSet.h>
#import <QuartzCore/QuartzCore.h>
#import "BTAlert.h"
#import "NSManagedObjectContext+SRFetchAsync.h"

#define kMaxIdleTimeSeconds 60.0

@interface MSRItemsViewController () <ProductCellDelegate, FiltersViewDelegate, UIPopoverControllerDelegate>{
    NSMutableArray *selectedCodes;
    NSString *importType;
    
    BOOL first_load;
    NSTimer *idleTimer;
    BOOL checked_user;
    
    int pictures_page;

    
    
    UIPopoverController *popover;
    BOOL searching;
}
@property (atomic, strong) NSMutableDictionary *picturesHash;
@property (nonatomic, strong) UIPanGestureRecognizer *dynamicTransitionPanGesture;
@property (strong, nonatomic) UIPopoverController *masterPopoverController;

- (void)configureView;
- (void)clearResultsView;
- (void)prepareResultsView;

@end

@implementation MSRItemsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (NSPredicate*)generateFilters{
    
    //clear filters
    selectedCodes = [[NSMutableArray alloc] init];
    
    NSPredicate *p = nil;
    NSMutableString *format = [[NSMutableString alloc] init];
    //default condition
    [format appendFormat:@"(code > 0) AND (language == '%@')",[[BTDataSync shared] language]];
    
    if(_filters && ([_filters count] > 0)){
        
        
        
        
        for (NSString *key in _filters.allKeys) {
            
            if((![[_filters objectForKey:key] isKindOfClass:[NSNumber class]] && [[_filters objectForKey:key] isEqualToString:@""]) || [key rangeOfString:@"composition_perc"].location != NSNotFound)
                continue;
            
            if([key isEqualToString:@"query"]){
                
                [format appendString:@" AND ("];
                [format appendFormat:@"(%@ CONTAINS[cd] '%@')",@"string_code", [_filters objectForKey:key]];
                [format appendFormat:@" OR (%@ CONTAINS[cd] '%@')",@"item_description", [_filters objectForKey:key]];
                [format appendFormat:@" OR (%@ CONTAINS[cd] '%@')",@"composition", [_filters objectForKey:key]];

                [format appendString:@")"];
                break; //exit for loop
            }
            
            //stitch_type0
            
            if([key rangeOfString:@"composition"].location != NSNotFound){
                
                NSString* fibre_code = [_filters objectForKey:key];
                NSString *percentage_key = @"composition_perc0";
                
                if([key rangeOfString:@"1"].location != NSNotFound)
                    percentage_key = @"composition_perc1";
                else if([key rangeOfString:@"1"].location != NSNotFound)
                    percentage_key = @"composition_perc2";
                
                float percentage = 0.0;
                
                if([_filters objectForKey:percentage_key] && ![[_filters objectForKey:percentage_key] isEqualToString:@""])
                    percentage = [ProductFibre floatPercentageFromString:[_filters objectForKey:percentage_key]];
                
                //[format appendFormat:@" AND ((ANY product_fibres.percentage >= %.2f) AND (ANY product_fibres.fibre_code == %@))",percentage,fibre_code];
                
                [format appendFormat:@" AND (SUBQUERY(product_fibres, $r,$r.fibre_code == %@ AND  $r.percentage >= %.2f).@count != 0)",fibre_code,percentage];
                
            }
            else if([key rangeOfString:@"stitch_type"].location != NSNotFound){
                NSNumber* stitch_type_code = [_filters objectForKey:key];
                [format appendFormat:@" AND (stitch_type_codes CONTAINS[cd] '#%@#')",stitch_type_code.description];
                
            }
            else if([key isEqualToString:@"with_pictures"]){
                [format appendFormat:@" AND (pictures.@count != 0)"];
            }
            else if([key rangeOfString:@"codes"].location != NSNotFound)
                [format appendFormat:@" AND (%@ CONTAINS[cd] '%@')",key, [NSString stringWithFormat:@"#%@#",[_filters objectForKey:key]]];
            else if([[_filters objectForKey:key] isKindOfClass:[NSNumber class]])
                [format appendFormat:@" AND (%@ == %@)",key, [_filters objectForKey:key]];
            else
                [format appendFormat:@" AND (%@ == '%@')",key, [_filters objectForKey:key]];
        }

    }
    NSLog(@"Format: %@",format);
    p = [NSPredicate predicateWithFormat:format];

    return p;
}

#pragma mark - Overrides template methods

- (void)filtersUpdated:(NSDictionary*)filters{
    [popover dismissPopoverAnimated:YES];
    popover = nil;
    self.fetchedResultsController = nil;
    _filters = filters;
    
    searching = YES;
    [selectedCodes removeAllObjects];
    [self.collapsedSections removeAllObjects];
    
    //fade out / in effect
    [UIView animateWithDuration:0.2 animations:^{
        self.collectionView.alpha = 0.0;
    } completion:^(BOOL finished) {
        
        [self prepareResultsView];
        [self.collectionView reloadData];
        
        [UIView animateWithDuration:0.2 animations:^{
           self.collectionView.alpha = 1.0;
        } completion:^(BOOL finished) {
            searching = NO;
            if(pictures_page > 0)
                [self loadPicturesHash];
        }];
    }];
    
    
}

- (void)updateFlag{
    
    NSString *image_name = @"united_kingdom_flag";
    NSString *lang = [[BTLanguage shared] currentLanguage];
    
    if([lang isEqualToString:@"it"])
        image_name = @"italy_flag";
    else if([lang isEqualToString:@"fr"])
        image_name = @"france_flag";
    
    [self.flagView setImage:[UIImage imageNamed:image_name]];
    [self.chooseLanguageButton setTitle:[[[BTLanguage shared] currentLanguage] uppercaseString] forState:UIControlStateNormal];
}

// customize headers
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    
    if (kind == UICollectionElementKindSectionHeader) {
        
        BTCollectionHeaderView *reusableview = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"BTSectionView" forIndexPath:indexPath];
        
        if (reusableview==nil) {
            reusableview=[[BTCollectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, 1024, 44)];
        }

        reusableview.tag = indexPath.section;
        reusableview.titleButton.tag = indexPath.section;
        reusableview.collapsedSymbolLabel.text = ([self collapsedSection:indexPath.section] ? @"+" : @"-");
        
        Product *object = [self.fetchedResultsController objectAtIndexPath:indexPath];
        reusableview.titleLabel.text=[NSString stringWithFormat:@"%d %@ %@",object.yearValue, object.macro_season.item_description,(object.category.item_description == nil ? @"" : object.category.item_description)];
        
        
        
        return reusableview;
    }
    return nil;

}

- (NSFetchRequest*)fetchRequest{
    
    NSFetchRequest * f = [[NSFetchRequest alloc] init];
    // Edit the entity name as appropriate.
    NSEntityDescription *entity = [NSEntityDescription entityForName:[self modelName] inManagedObjectContext:self.managedObjectContext];
    [f setEntity:entity];

    // Set the batch size to a suitable number.
    [f setFetchBatchSize:40];
    
    //USE FILTERS HERE
    if(!first_load)
        [f setPredicate:[self generateFilters]];
    else
        [f setPredicate:[NSPredicate predicateWithFormat:@"code < 0"]];


    
    // Edit the sort key as appropriate.
    NSSortDescriptor *seasonSort = [[NSSortDescriptor alloc] initWithKey:@"macro_season_code" ascending:YES];
    NSSortDescriptor *yearSort = [[NSSortDescriptor alloc] initWithKey:@"year" ascending:NO];
    NSSortDescriptor *categorySort = [[NSSortDescriptor alloc] initWithKey:@"category_code" ascending:YES];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"item_description" ascending:YES];
    
    [f setSortDescriptors:@[yearSort,seasonSort,categorySort, sortDescriptor]];

    
    [f setReturnsObjectsAsFaults:NO];
    [f setRelationshipKeyPathsForPrefetching:[NSArray arrayWithObjects:@"pictures", nil]];
    //enable group for this attribute
    self.groupingKeypath = @"category_code";
    
    first_load = NO;
    
    return f;
}

- (NSString*)remotePath{
    return @""; //not used
}

- (NSString*)modelName{
    return @"Product";
}

#pragma mark - results view functions

- (void)clearResultsView{
    
    [self.resultsView setHidden:YES];
    self.modelsNumber.text = @"";
    self.seasonsNumber.text = @"";
    self.categoriesNumber.text = @"";
    
    //dynamically visible brand results
    for(UILabel* label in @[self.brandName0,self.brandName1,self.brandName2,self.brandName3,self.brandName4,self.brandName5,
                            self.brandNumber0,self.brandNumber1,self.brandNumber2,self.brandNumber3,self.brandNumber4,self.brandNumber5
                            ]){
        label.text = @"";
        [label setHidden:YES];
    }
    CALayer *l = self.resultsView.layer;
    [l setBorderColor:[UIColor whiteColor].CGColor];
    [l setBorderWidth:1.0];
    
    l = self.infoButton.layer;
    [l setBorderColor:[UIColor whiteColor].CGColor];
    [l setBorderWidth:1.0];
    
    l = self.proceedButton.layer;
    [l setBorderColor:[UIColor whiteColor].CGColor];
    [l setBorderWidth:1.0];

}

- (void)prepareResultsView{
    
    [self clearResultsView];
    
    if(first_load)
        self.resultsLabel.text = NSLocalizedString(@"Choose a filter",@"Seleziona un filtro");
    else if(!self.managedObjectContext || [[[self fetchedResultsController] fetchedObjects] count] == 0)
        self.resultsLabel.text = NSLocalizedString(@"No product found",@"Nessun prodotto trovato");
    else
        self.resultsLabel.text = @"";
    
    
    
    //Totals
    
    //new background Q
    dispatch_queue_t resultsCalculationQueue = dispatch_queue_create("com.bluetouchdev.mpr.results_calculation", NULL); // create my serial queue
    
    dispatch_async(resultsCalculationQueue, ^{
        dispatch_async(dispatch_get_main_queue(), ^{
            // add this to the main queue as the last item in my serial queue
            // when I get to this point I know everything in my queue has been run
            
            
            //Nr of models
            self.modelsNumber.text = [NSString stringWithFormat:@"%lu",(unsigned long)[[self.fetchedResultsController  fetchedObjects] count]];
            
            //Nr of seasons
            int seasons_count = 0;
            NSMutableArray *seasons = [[NSMutableArray alloc] init];
            
            //Nr of categories
            int categories_count = 0;
            NSMutableArray *categories = [[NSMutableArray alloc] init];
            
            //Nr of products per Brand
            S2SOrderedDictionary *brands = [[S2SOrderedDictionary alloc] init];
            
            NSLog(@"Found %d products",[[self.fetchedResultsController fetchedObjects] count]);
            for (Product *p in [self.fetchedResultsController fetchedObjects]) {
                
                if(p.macro_season_code != nil && [seasons indexOfObject:p.macro_season_code] == NSNotFound){
                    [seasons addObject:p.macro_season_code];
                    seasons_count += 1;
                }
                
                if(p.category_code != nil && [categories indexOfObject:p.category_code] == NSNotFound){
                    [categories addObject:p.category_code];
                    categories_count += 1;
                }
                
                NSNumber *products_count = [[NSNumber alloc] initWithInt:0];
                
                if([brands indexForKey:[p.brand_code description]] != NSNotFound)
                    products_count = [brands objectForKey:p.brand.item_description];
                
                products_count = [[NSNumber alloc] initWithInt:(products_count.intValue +1)];
                
                [brands setObject:products_count forKey:p.brand.item_description];
                
            }
            
            self.seasonsNumber.text = [NSString stringWithFormat:@"%d",seasons_count];
            self.categoriesNumber.text = [NSString stringWithFormat:@"%d",categories_count];
            
            
            NSDictionary *dict = brands.dictionary;
            
            NSArray *orderedKeys = [[dict allKeys] sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
                return [[dict objectForKey:obj1] compare:[dict objectForKey:obj2]];
            }];
            
            
            NSArray *brand_names = @[self.brandName0,self.brandName1,self.brandName2,self.brandName3,self.brandName4,self.brandName5];
            NSArray *brand_values = @[self.brandNumber0,self.brandNumber1,self.brandNumber2,self.brandNumber3,self.brandNumber4,self.brandNumber5];
            
            for (int counter = 0; counter < [brand_names count] && counter < [brands count]; counter++) {
                int rev_index = ([orderedKeys count] - counter - 1);
                UILabel *name = [brand_names objectAtIndex:counter];
                UILabel *value = [brand_values objectAtIndex:counter];
                
                NSString *key = [orderedKeys objectAtIndex:rev_index];
                
                if(name != nil && value != nil){
                    name.text = key;
                    name.hidden = NO;
                    value.text = [[brands objectForKey:key] description];
                    value.hidden = NO;
                }
                NSLog(@"%@: %d products",key, [[brands objectForKey:key] intValue]);
            }
            
        });
    });
    

}

#pragma mark - collectionView delegate / datasource methods overrides


- (void)configureCell:(UICollectionViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    MSRProductCell *c = (MSRProductCell*)cell;
    
    Product *object = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    [c.contentView setBackgroundColor:[UIColor grayColor]];
    c.seasonLabel.text = [NSString stringWithFormat:@"%@ - %d",[[object.macro_season item_description] uppercaseString], object.cleanedYear];
    c.modelLabel.text = [NSString stringWithFormat:@"%@ %@",object.brand.string_code, object.model];
    
    c.productNameLabel.text = [object.item_description uppercaseString];
    if(object.category)
        c.categoryLabel.text = [[object.category item_description] uppercaseString];
    else
        c.categoryLabel.text = NSLocalizedString(@"NO CATEGORY FOUND", @"");
    
    if([self.dataSync loggedIn]){ //shows brand
        c.supplierLabel.text = [[object.brand item_description] uppercaseString];
    }
    else{ //shows supplier
        Yarn *yarn = [object yarn];
        if(yarn){
            c.supplierLabel.text = [yarn.supplier cleanedDescription];
        }
        else{
            c.supplierLabel.text = NSLocalizedString(@"NO SUPPLIER FOUND",@"");
        }
    }
    
    //[self.picturesHash objectForKey:object.code.description]
    NSURL *tmp = object.thumbURL;
    if(tmp)
        [c.productImage setImageWithURL:object.thumbURL placeholderImage:[UIImage imageNamed:@"logo_big.png"]];
    else
        [c.productImage setImage:[UIImage imageNamed:@"logo_big.png"]];
    
    UIImage *img = [UIImage imageGlyphNamed:@"check-square" size:CGSizeMake(80.0, 80.0) color:[UIColor colorWithRed:0.282 green:0.725 blue:0.839 alpha:0.600]];
    
    [c.activeImage  setImage:img];
    [c setProductCode:object.code];
    [c setActive:[self selectedObjectCode:object.code]];
    [c setDelegate:self];
    
}

- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath{
    Product *p = [self.fetchedResultsController objectAtIndexPath:indexPath];
    MSRProductCell *c = (MSRProductCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    if(p && c)
        [c setActive:!c.isActive];

}

- (void)toggledSelectionForCode:(NSNumber*)code{
    
    if([self selectedObjectCode:code])
        [selectedCodes removeObject:code.description];
    else
        [selectedCodes addObject:code.description];
}

- (BOOL)selectedObjectCode:(NSNumber*)code{
    return [selectedCodes indexOfObject:code.description] != NSNotFound;
}

- (NSArray *)selectedObjects{
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    
//    NSMutableString *format = [[NSMutableString alloc] init];
//    //default condition
//    [format appendFormat:@"(code IN %@)",selectedCodes];
//    NSPredicate *p = [NSPredicate predicateWithFormat:format];
//    
//    
//    return [Product objectsWithPredicate:p]
    for (Product *p in [self.fetchedResultsController fetchedObjects])
        if([selectedCodes indexOfObject:p.code.description] != NSNotFound)
            [arr addObject:p];
    
    return arr;
}

- (IBAction)clearSelection:(id)sender{
    
}

#pragma mark -

#pragma mark - Managing the detail item


- (void)setFilters:(NSDictionary *)filtersDictionary{
    _filters = filtersDictionary;
    [self configureView];
    
}

- (NSDictionary *)filtersDictionary{
    return _filters;
}

- (void)configureView
{
    // Update the user interface for the detail item.
  
    self.fetchedResultsController = nil;
    [self.collectionView reloadData];

}

#pragma mark -

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier isEqualToString:@"productDetails"]){
        MSRProductDetailViewController *v = [segue destinationViewController];
        v.choosenProducts = [self selectedObjects];
    }
    else if([segue.identifier isEqualToString:@"downloadView"]){
        MSRDownloadViewController *tmp = [segue destinationViewController];
        tmp.importType = importType;
    }
    else if([segue.identifier isEqualToString:@"treeView"]){
        popover = [(UIStoryboardPopoverSegue*)segue popoverController];
        popover.delegate = self;
        MSRTreeViewController *t = [segue destinationViewController];
        [t setDelegate:self.activeFiltersView];
        
//        if([self.activeFiltersView.stitch_type_key isEqualToString:@"stitch_type0"])
//            t.expanded = self.activeFiltersView.stitch_type0;
//        else if([self.activeFiltersView.stitch_type_key isEqualToString:@"stitch_type1"])
//            t.expanded = self.activeFiltersView.stitch_type1;
//        else
//            t.expanded = self.activeFiltersView.stitch_type2;
        
        
    }
}

#pragma mark - Template methods overrides

//Override these methods
- (NSArray*)notificationNames{
    return [[super notificationNames] arrayByAddingObjectsFromArray:@[@"SEEDING_COMPLETED",@"DECOMPRESS_IMAGES_SUCCESS",@"IMPORT_COMPLETED",@"SEEDING_COMPLETED",@"START_IMAGE_UPDATE", @"USER_LOGGED_IN",@"USER_LOGGED_OUT"]];
}

- (void)handleNotification:(NSNotification *)notification{
    
    if([notification.name isEqualToString:@"SEEDING_COMPLETED"] || [notification.name isEqualToString:@"IMPORT_COMPLETED"] || [notification.name isEqualToString:@"DECOMPRESS_IMAGES_SUCCESS"]){
        [self.collectionView reloadData];
    }
    else if([notification.name isEqualToString:@"START_IMAGE_UPDATE"]){
        [self launchUpdateImages];
    }
    else if([notification.name isEqualToString:@"USER_LOGGED_IN"]  || [notification.name isEqualToString:@"USER_LOGGED_OUT"]){
        NSLog(@"Need to update views");
    }
}


- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];

    if(first_load)
        [self showLockScreen];
    
    [self resetIdleTimer];
    
}

- (void)showLockScreen{

    if([self.navigationController.presentedViewController.class isSubclassOfClass:[MSRLockScreenViewController class]]){
        return;
    }
    else if (![self.navigationController.presentedViewController.class isSubclassOfClass:[MSRItemsViewController class]]){
        //pop current view
        [self dismissViewControllerAnimated:NO completion:nil];
    }
    
    
    MSRLockScreenViewController *lockScreen = [self.storyboard instantiateViewControllerWithIdentifier:@"lockScreen"];
    [self.navigationController presentViewController:lockScreen animated:!first_load completion:^{
        
    }];
    
    
}

#pragma mark Handling idle timeout

- (void)resetIdleTimer {
//    if (!idleTimer) {
//        idleTimer = [NSTimer scheduledTimerWithTimeInterval:kMaxIdleTimeSeconds
//                                                      target:self
//                                                    selector:@selector(idleTimerExceeded)
//                                                    userInfo:nil
//                                                     repeats:NO];
//    }
//    else {
//        if (fabs([idleTimer.fireDate timeIntervalSinceNow]) < kMaxIdleTimeSeconds-1.0) {
//            [idleTimer setFireDate:[NSDate dateWithTimeIntervalSinceNow:kMaxIdleTimeSeconds]];
//        }
//    }
}

- (void)idleTimerExceeded {

    idleTimer = nil;
    [self showLockScreen];
    [self resetIdleTimer];
}

- (UIResponder *)nextResponder {
    [self resetIdleTimer];
    return [super nextResponder];
}

#pragma mark -

#pragma mark - IBActions

- (IBAction)importButtonTapper:(id)sender{
    idleTimer = nil;
    [self performSegueWithIdentifier:@"import" sender:self];
}

- (void)launchUpdateImages{
    importType = @"UPDATE";
    [self performSegueWithIdentifier:@"downloadView" sender:self];
}

- (IBAction)checkAuth:(id)sender{
    [self checkAuth];
}

- (void)checkAuth{
    
    if(!self.dataSync.loggedIn){
        checked_user = YES;
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Login",@"")
                                                            message:nil
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"Guest user",@"")
                                                  otherButtonTitles:NSLocalizedString(@"Sign in",@""), nil];
        
        alertView.alertViewStyle = UIAlertViewStyleLoginAndPasswordInput;
        [alertView show];
    }
    else{
        [self.dataSync signOut];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"USER_LOGGED_OUT" object:nil];
        [self.loginButton setTitle:[NSLocalizedString(@"Login",@"") uppercaseString] forState:UIControlStateNormal];
        [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"Successfully logged out", @"logout message")];
    }

}

- (BOOL)filterPanelOpened{
    return (self.activeFiltersView.frame.origin.y == 0);
}

- (IBAction)searchAndHidePanel:(id)sender{
    [self.activeFiltersView applyFilters];

    [selectedCodes removeAllObjects];
    [self.collapsedSections removeAllObjects];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.6 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self toggleFilters:sender];
    });

}

- (IBAction)clearSelected:(id)sender{
    [selectedCodes removeAllObjects];
    [self.collectionView reloadData];
}

- (IBAction)toggleFilters:(id)sender{
    
    CGRect frame = self.activeFiltersView.frame;
    if([self filterPanelOpened]){ //going to hide, 0.0 -> -220.0
        frame.origin.y = -220.0;
        [self.filtersButton setTitle:@"+" forState:UIControlStateNormal];
    }
    else{ //opening -220.0 -> 0
        frame.origin.y = 0.0;
        [self.filtersButton setTitle:@"-" forState:UIControlStateNormal];
    }
    [UIView animateWithDuration:0.3 animations:^{
        self.activeFiltersView.frame = frame;
    }];
    
}

- (IBAction)closeInfo:(id)sender{
    [self.resultsView setHidden:YES];
}
- (IBAction)toggleInfos:(id)sender{
    [self.resultsView setHidden:!self.resultsView.hidden];
}

- (IBAction)proceed:(id)sender{

    if([selectedCodes count] > 0)
        [self performSegueWithIdentifier:@"productDetails" sender:self];
    else{
        BTAlert *al = [[BTAlert alloc] initWithTitle:NSLocalizedString(@"No product selected", @"Nessun prodotto selezionato") message:NSLocalizedString(@"You need to select at least one product",@"Devi selezionare almeno un prodotto") delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [al show];
    }
}

- (IBAction)tappedOnHeader:(id)sender{
    NSLog(@"Tapped on header...");
}

#pragma mark - alertview delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    //Index1 = Sign in
    //Index0 = Guest

    if(buttonIndex==1)
    {

        NSString *userName = [alertView textFieldAtIndex:0].text;
        NSString *password = [alertView textFieldAtIndex:1].text;
        
        if([self.dataSync signInWithUsername:userName andPassword:password])
            [[NSNotificationCenter defaultCenter] postNotificationName:@"USER_LOGGED_IN" object:nil];
        else{
            [SVProgressHUD showErrorWithStatus:@"Invalid credentials"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"USER_LOGGED_OUT" object:nil];
        }
    }
    else{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"USER_LOGGED_OUT" object:nil];
    }
}

#pragma mark -

- (BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController
{
    return NO;
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
//    if([self filterPanelOpened])
//        [self toggleFilters:self];
    
//    if(!checked_user)
//        [self checkAuth:self];
}
- (void)loadPicturesHash{
    
    pictures_page += 1;
    if(searching)
        return;
    [self.managedObjectContext executeFetchRequest:[self picturesFetchRequest] completion:^(NSArray *objects, NSError *error) {
        if([objects count] > 0){
            [self appendPicturesToHash:objects];
            [self loadPicturesHash];
        }
        else{
            NSLog(@"Pictures extraction completed");
            pictures_page = 0;
        }
    }];
    
}

- (void)appendPicturesToHash:(NSArray*)pictures{
    
    for (int idx = 0; idx < [pictures count]; idx++){
        Picture *pic = [pictures objectAtIndex:idx];
        NSString *tmp = [pic filePath];
        if(tmp){
            NSLog(@"Adding url: %@",tmp);
            [self.picturesHash setObject:[[pic thumbURL] copy] forKey:pic.product_code.description];
        }
    }
}

-(NSFetchRequest*)picturesFetchRequest{
    
    NSFetchRequest * f = [[NSFetchRequest alloc] init];
    // Edit the entity name as appropriate.
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Picture" inManagedObjectContext:self.managedObjectContext];
    [f setEntity:entity];
    
    int limit = 200;
    NSMutableString *format = [[NSMutableString alloc] init];
    // Set the batch size to a suitable number.
    [f setFetchBatchSize:limit];
    [f setFetchLimit:limit];
    [f setFetchOffset:(limit * pictures_page)];
    
    [format appendFormat:@"code > 0 and language == '%@'",[self.dataSync language]];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:format];
    [f setPredicate:predicate];
    // Edit the sort key as appropriate.
    NSSortDescriptor *pictureSort = [[NSSortDescriptor alloc] initWithKey:@"product_code" ascending:YES];
    [f setSortDescriptors:@[pictureSort]];
    
    return f;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    first_load = YES;
    [self resetIdleTimer];
    selectedCodes = [[NSMutableArray alloc] init];
    
	// Do any additional setup after loading the view.
    self.collectionView.backgroundColor = [UIColor clearColor];
    importType = @"IMPORT";
    
    [self.titleView removeFromSuperview];
    self.navigationItem.titleView = self.titleView;
    [self.refreshButton setImage:[UIImage imageGlyphNamed:@"refresh" size:self.refreshButton.size color:[UIColor whiteColor]] forState:UIControlStateNormal];
    
    [self updateFlag];
    [self.activeFiltersView loadFiltersData];
    [self.activeFiltersView setDelegate:self];
    [self clearResultsView];
    
    [self prepareResultsView];
    
    //temporary disabled
    [self.refreshButton setHidden:YES];
    /*
    if(![self.dataSync existImageBundle] && ![self.dataSync existImagesInDocuments]){
        
        NSNumber *imported_assets = [self.dataSync settingsValueForKey:@"imported_assets"];
        if(!imported_assets){
            BTAlert *al = [[BTAlert alloc] initWithTitle:@"Import images" text:@"Catalog images are not pre-imported inside the app, would you like to download them now?" style:UIAlertViewStyleDefault onConfirm:^(UIAlertView *alert) {
                
                [self performSegueWithIdentifier:@"downloadView" sender:self];
                
            } onDismiss:nil];
            
            [al show];
        }
    }
     */
    
    //[self checkAuth];

    self.picturesHash = [[NSMutableDictionary alloc] init];
    pictures_page = 0;
    if(false){
    dispatch_queue_t resultsCalculationQueue = dispatch_queue_create("com.bluetouchdev.mpr.pictures_urls", NULL); // create my serial queue
    
    dispatch_async(resultsCalculationQueue, ^{
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW,
                                                 (unsigned long)NULL), ^{
            [self loadPicturesHash];
        });
    });
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
