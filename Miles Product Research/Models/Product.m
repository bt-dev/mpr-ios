#import "Product.h"


@interface Product (){
    BTDataSync *sync;
    NSArray *_sortedPictures;
    Picture *_genericPicture;
    int _defaultPictureIndex;
}

// Private interface goes here.

@end


@implementation Product

// Custom logic goes here.

//+ (S2SOrderedDictionary*)filterValues{
//    S2SOrderedDictionary *data = [[S2SOrderedDictionary alloc] init];
//    
//    [data setObject:@"" forKey:[NSString stringWithFormat:NSLocalizedString(@" -- All %@ --",@" -- Tutti i prodotti -- "),NSLocalizedString(@"products", @"products")]];
//    
//    NSSortDescriptor *sortByName = [[NSSortDescriptor alloc] initWithKey:@"item_description" ascending:YES];
//    NSFetchRequest *f = [Product fetchRequest];
//    [f setSortDescriptors:[NSArray arrayWithObject:sortByName]];
//    
//    NSMutableString *format = [[NSMutableString alloc] init];
//    [format appendFormat:@"(code > 0)",[[BTDataSync shared] language]];
//    NSPredicate *p = [NSPredicate predicateWithFormat:format];
//    [f setPredicate:p];
//    
//    for (Product *tmp in [Product objectsWithFetchRequest:f])
//        [data setObject:tmp.code.stringValue forKey:tmp.item_description];
//    
//    return data;
//}

- (int)cleanedYear{
    return (self.year && self.yearValue != 2020 ? self.yearValue : 2000);
}

#pragma mark - yarns helpers
- (NSArray*)yarns{
    
    NSPredicate *p = [NSPredicate predicateWithFormat:@"code IN (%@)",[self yarnCodes]];
    return [Yarn objectsWithPredicate:p];
}

- (Yarn*)yarn{
    
    NSArray *tmp = [self yarns];
    if(tmp && [tmp count] > 0)
        return [tmp firstObject];
    else
        return nil;
}

- (NSArray*)yarnCodes{
    NSMutableArray *tmp = [[self.yarn_codes componentsSeparatedByString:@"#"] mutableCopy];
    [tmp removeObject:@""];
    
    NSMutableArray *yarns = [[NSMutableArray alloc] init];
    for (NSString *code in tmp)
        [yarns addObject:[NSNumber numberWithInt:[code intValue]]];
    
    return yarns;
}

#pragma mark -

#pragma mark - suppliers helpers
- (Supplier*)supplier{
    
    NSArray *tmp = [self suppliers];
    if(tmp && [tmp count] > 0)
        return [tmp firstObject];
    else
        return nil;
}

- (NSArray*)suppliers{
    
    NSPredicate *p = [NSPredicate predicateWithFormat:@"code IN (%@)",[self supplierCodes]];
    return [Supplier objectsWithPredicate:p];
}

- (NSArray*)supplierCodes{
    NSMutableArray *tmp = [[self.supplier_codes componentsSeparatedByString:@"#"] mutableCopy];
    [tmp removeObject:@""];
    
    NSMutableArray *suppliers = [[NSMutableArray alloc] init];
    for (NSString *code in tmp)
        [suppliers addObject:[NSNumber numberWithInt:[code intValue]]];
    
    return suppliers;
}

#pragma mark -

#pragma mark - stitch type helpers

- (StitchType*)stitchType{
    
    NSArray *tmp = [self stitchTypes];
    if(tmp && [tmp count] > 0)
        return [tmp firstObject];
    else
        return nil;
}

- (NSArray*)stitchTypes{
    
    NSPredicate *p = [NSPredicate predicateWithFormat:@"code IN (%@)",[self stitchTypeCodes]];
    return [StitchType objectsWithPredicate:p];
}

- (NSArray*)stitchTypeCodes{
    
    NSMutableArray *tmp = [[self.stitch_type_codes componentsSeparatedByString:@"#"] mutableCopy];
    [tmp removeObject:@""];
    
    NSMutableArray *stitch_types = [[NSMutableArray alloc] init];
    for (NSString *code in tmp)
        [stitch_types addObject:[NSNumber numberWithInt:[code intValue]]];
    
    return stitch_types;

}

#pragma mark -

// Custom logic goes here.

- (NSArray*)sortedPictures{
    if(_sortedPictures)
        return _sortedPictures;
    
    if(!sync)
        sync = [BTDataSync shared];
    
    NSFetchRequest *f = [Picture fetchRequest];
    NSSortDescriptor *sortByName = [[NSSortDescriptor alloc] initWithKey:@"code" ascending:YES];
    [f setSortDescriptors:@[sortByName]];
    
    NSPredicate *p = nil;
    NSMutableString *format = [[NSMutableString alloc] init];
    
    [format appendFormat:@"(product_code == %@) AND (language == '%@')",self.code,[sync language]];
    
    p = [NSPredicate predicateWithFormat:format];
    [f setPredicate:p];
    
    _sortedPictures = [Picture objectsWithFetchRequest:f];
    
    return _sortedPictures;
    
}

- (BOOL)hasGenericPictureFile{
    
    Picture *p = [self genericPicture];
    
    if(!p)
        return NO;
    else{
        
        if(p.filePath != nil && [p.filePath rangeOfString:@"http"].location != NSNotFound)
            return YES;
        else
            return [[NSFileManager defaultManager] fileExistsAtPath:p.filePath];
        
        
    }
    
}

- (Picture*)genericPicture{
    if(_genericPicture)
        return _genericPicture;
    
    NSArray *pics = [self.pictures allObjects];
    
    if(pics && [pics count] > 0){
        for (Picture *p in pics) {
            if(p.primaryPicture) //uses default_image as 'generic'
                _genericPicture = p;
        }
    }
    
    return _genericPicture;
}

- (NSString*)genericPicturePath{
    Picture *p = [self genericPicture];
    return (!p ? nil : p.filePath);
}


- (NSURL*)genericPictureURL{
    Picture *p = [self genericPicture];
    return p.fileURL;
}

- (int)defaultPictureIndex{
    if (_defaultPictureIndex)
        return _defaultPictureIndex;
    
    NSArray *pictures = [self.pictures allObjects];//[self sortedPictures];
    int index = 0;
    
    if([pictures count] == 0)
        return 0;
    
    for(int i = 0; i < [pictures count]; i++){
        Picture *tmp = [pictures objectAtIndex:i];

        if([[tmp.image_file_name lowercaseString] isEqualToString:[self.default_image lowercaseString]]){
            index = i;
            break;
        }
    }
    
    return index;
}

- (Picture*)defaultPicture{
    NSArray *pictures = [self.pictures allObjects];//[self sortedPictures];
    Picture *p = nil;
    
    if([pictures count] == 0)
        return nil;
    else
        p = [pictures objectAtIndex:[self defaultPictureIndex]];
    
    return p;
    
}

- (NSString*)defaultPicturePath{
    Picture *p = [self defaultPicture];
    if(p)
        return p.filePath;
    else
        return @"";
}


- (NSURL*)defaultPictureURL{
    Picture *p = [self defaultPicture];
    if(p)
        return p.fileURL;
    else
        return nil;
}

- (NSURL*)thumbURL{
    Picture *p = [self defaultPicture];
    if(p)
        return p.thumbURL;
    else
        return nil;
}

@end
