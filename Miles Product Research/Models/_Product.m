// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Product.m instead.

#import "_Product.h"

const struct ProductAttributes ProductAttributes = {
	.brand_code = @"brand_code",
	.brand_type_code = @"brand_type_code",
	.category_code = @"category_code",
	.code = @"code",
	.default_image = @"default_image",
	.family_code = @"family_code",
	.fibre_codes = @"fibre_codes",
	.item_description = @"item_description",
	.language = @"language",
	.macro_season_code = @"macro_season_code",
	.model = @"model",
	.position = @"position",
	.prgshima_exists = @"prgshima_exists",
	.prgstoll_exists = @"prgstoll_exists",
	.room = @"room",
	.stitch_type_codes = @"stitch_type_codes",
	.stored = @"stored",
	.string_code = @"string_code",
	.supplier_codes = @"supplier_codes",
	.thinness = @"thinness",
	.updated_images = @"updated_images",
	.yarn_codes = @"yarn_codes",
	.year = @"year",
};

const struct ProductRelationships ProductRelationships = {
	.brand = @"brand",
	.brand_type = @"brand_type",
	.category = @"category",
	.family = @"family",
	.macro_season = @"macro_season",
	.pictures = @"pictures",
	.product_fibres = @"product_fibres",
};

const struct ProductFetchedProperties ProductFetchedProperties = {
};

@implementation ProductID
@end

@implementation _Product

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Product" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Product";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Product" inManagedObjectContext:moc_];
}

- (ProductID*)objectID {
	return (ProductID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	
	if ([key isEqualToString:@"brand_codeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"brand_code"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"brand_type_codeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"brand_type_code"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"category_codeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"category_code"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"codeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"code"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"family_codeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"family_code"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"macro_season_codeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"macro_season_code"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"positionValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"position"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"prgshima_existsValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"prgshima_exists"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"prgstoll_existsValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"prgstoll_exists"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"roomValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"room"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"storedValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"stored"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"thinnessValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"thinness"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"updated_imagesValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"updated_images"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"yearValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"year"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}




@dynamic brand_code;



- (int32_t)brand_codeValue {
	NSNumber *result = [self brand_code];
	return [result intValue];
}

- (void)setBrand_codeValue:(int32_t)value_ {
	[self setBrand_code:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveBrand_codeValue {
	NSNumber *result = [self primitiveBrand_code];
	return [result intValue];
}

- (void)setPrimitiveBrand_codeValue:(int32_t)value_ {
	[self setPrimitiveBrand_code:[NSNumber numberWithInt:value_]];
}





@dynamic brand_type_code;



- (int32_t)brand_type_codeValue {
	NSNumber *result = [self brand_type_code];
	return [result intValue];
}

- (void)setBrand_type_codeValue:(int32_t)value_ {
	[self setBrand_type_code:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveBrand_type_codeValue {
	NSNumber *result = [self primitiveBrand_type_code];
	return [result intValue];
}

- (void)setPrimitiveBrand_type_codeValue:(int32_t)value_ {
	[self setPrimitiveBrand_type_code:[NSNumber numberWithInt:value_]];
}





@dynamic category_code;



- (int32_t)category_codeValue {
	NSNumber *result = [self category_code];
	return [result intValue];
}

- (void)setCategory_codeValue:(int32_t)value_ {
	[self setCategory_code:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveCategory_codeValue {
	NSNumber *result = [self primitiveCategory_code];
	return [result intValue];
}

- (void)setPrimitiveCategory_codeValue:(int32_t)value_ {
	[self setPrimitiveCategory_code:[NSNumber numberWithInt:value_]];
}





@dynamic code;



- (int32_t)codeValue {
	NSNumber *result = [self code];
	return [result intValue];
}

- (void)setCodeValue:(int32_t)value_ {
	[self setCode:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveCodeValue {
	NSNumber *result = [self primitiveCode];
	return [result intValue];
}

- (void)setPrimitiveCodeValue:(int32_t)value_ {
	[self setPrimitiveCode:[NSNumber numberWithInt:value_]];
}





@dynamic default_image;






@dynamic family_code;



- (int32_t)family_codeValue {
	NSNumber *result = [self family_code];
	return [result intValue];
}

- (void)setFamily_codeValue:(int32_t)value_ {
	[self setFamily_code:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveFamily_codeValue {
	NSNumber *result = [self primitiveFamily_code];
	return [result intValue];
}

- (void)setPrimitiveFamily_codeValue:(int32_t)value_ {
	[self setPrimitiveFamily_code:[NSNumber numberWithInt:value_]];
}





@dynamic fibre_codes;






@dynamic item_description;






@dynamic language;






@dynamic macro_season_code;



- (int32_t)macro_season_codeValue {
	NSNumber *result = [self macro_season_code];
	return [result intValue];
}

- (void)setMacro_season_codeValue:(int32_t)value_ {
	[self setMacro_season_code:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveMacro_season_codeValue {
	NSNumber *result = [self primitiveMacro_season_code];
	return [result intValue];
}

- (void)setPrimitiveMacro_season_codeValue:(int32_t)value_ {
	[self setPrimitiveMacro_season_code:[NSNumber numberWithInt:value_]];
}





@dynamic model;






@dynamic position;



- (int32_t)positionValue {
	NSNumber *result = [self position];
	return [result intValue];
}

- (void)setPositionValue:(int32_t)value_ {
	[self setPosition:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitivePositionValue {
	NSNumber *result = [self primitivePosition];
	return [result intValue];
}

- (void)setPrimitivePositionValue:(int32_t)value_ {
	[self setPrimitivePosition:[NSNumber numberWithInt:value_]];
}





@dynamic prgshima_exists;



- (BOOL)prgshima_existsValue {
	NSNumber *result = [self prgshima_exists];
	return [result boolValue];
}

- (void)setPrgshima_existsValue:(BOOL)value_ {
	[self setPrgshima_exists:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitivePrgshima_existsValue {
	NSNumber *result = [self primitivePrgshima_exists];
	return [result boolValue];
}

- (void)setPrimitivePrgshima_existsValue:(BOOL)value_ {
	[self setPrimitivePrgshima_exists:[NSNumber numberWithBool:value_]];
}





@dynamic prgstoll_exists;



- (BOOL)prgstoll_existsValue {
	NSNumber *result = [self prgstoll_exists];
	return [result boolValue];
}

- (void)setPrgstoll_existsValue:(BOOL)value_ {
	[self setPrgstoll_exists:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitivePrgstoll_existsValue {
	NSNumber *result = [self primitivePrgstoll_exists];
	return [result boolValue];
}

- (void)setPrimitivePrgstoll_existsValue:(BOOL)value_ {
	[self setPrimitivePrgstoll_exists:[NSNumber numberWithBool:value_]];
}





@dynamic room;



- (int32_t)roomValue {
	NSNumber *result = [self room];
	return [result intValue];
}

- (void)setRoomValue:(int32_t)value_ {
	[self setRoom:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveRoomValue {
	NSNumber *result = [self primitiveRoom];
	return [result intValue];
}

- (void)setPrimitiveRoomValue:(int32_t)value_ {
	[self setPrimitiveRoom:[NSNumber numberWithInt:value_]];
}





@dynamic stitch_type_codes;






@dynamic stored;



- (BOOL)storedValue {
	NSNumber *result = [self stored];
	return [result boolValue];
}

- (void)setStoredValue:(BOOL)value_ {
	[self setStored:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveStoredValue {
	NSNumber *result = [self primitiveStored];
	return [result boolValue];
}

- (void)setPrimitiveStoredValue:(BOOL)value_ {
	[self setPrimitiveStored:[NSNumber numberWithBool:value_]];
}





@dynamic string_code;






@dynamic supplier_codes;






@dynamic thinness;



- (int32_t)thinnessValue {
	NSNumber *result = [self thinness];
	return [result intValue];
}

- (void)setThinnessValue:(int32_t)value_ {
	[self setThinness:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveThinnessValue {
	NSNumber *result = [self primitiveThinness];
	return [result intValue];
}

- (void)setPrimitiveThinnessValue:(int32_t)value_ {
	[self setPrimitiveThinness:[NSNumber numberWithInt:value_]];
}





@dynamic updated_images;



- (BOOL)updated_imagesValue {
	NSNumber *result = [self updated_images];
	return [result boolValue];
}

- (void)setUpdated_imagesValue:(BOOL)value_ {
	[self setUpdated_images:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveUpdated_imagesValue {
	NSNumber *result = [self primitiveUpdated_images];
	return [result boolValue];
}

- (void)setPrimitiveUpdated_imagesValue:(BOOL)value_ {
	[self setPrimitiveUpdated_images:[NSNumber numberWithBool:value_]];
}





@dynamic yarn_codes;






@dynamic year;



- (int32_t)yearValue {
	NSNumber *result = [self year];
	return [result intValue];
}

- (void)setYearValue:(int32_t)value_ {
	[self setYear:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveYearValue {
	NSNumber *result = [self primitiveYear];
	return [result intValue];
}

- (void)setPrimitiveYearValue:(int32_t)value_ {
	[self setPrimitiveYear:[NSNumber numberWithInt:value_]];
}





@dynamic brand;

	

@dynamic brand_type;

	
- (NSMutableSet*)brand_typeSet {
	[self willAccessValueForKey:@"brand_type"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"brand_type"];
  
	[self didAccessValueForKey:@"brand_type"];
	return result;
}
	

@dynamic category;

	

@dynamic family;

	

@dynamic macro_season;

	

@dynamic pictures;

	
- (NSMutableSet*)picturesSet {
	[self willAccessValueForKey:@"pictures"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"pictures"];
  
	[self didAccessValueForKey:@"pictures"];
	return result;
}
	

@dynamic product_fibres;

	
- (NSMutableSet*)product_fibresSet {
	[self willAccessValueForKey:@"product_fibres"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"product_fibres"];
  
	[self didAccessValueForKey:@"product_fibres"];
	return result;
}
	






@end
