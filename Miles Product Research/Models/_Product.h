// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Product.h instead.

#import <CoreData/CoreData.h>


extern const struct ProductAttributes {
	__unsafe_unretained NSString *brand_code;
	__unsafe_unretained NSString *brand_type_code;
	__unsafe_unretained NSString *category_code;
	__unsafe_unretained NSString *code;
	__unsafe_unretained NSString *default_image;
	__unsafe_unretained NSString *family_code;
	__unsafe_unretained NSString *fibre_codes;
	__unsafe_unretained NSString *item_description;
	__unsafe_unretained NSString *language;
	__unsafe_unretained NSString *macro_season_code;
	__unsafe_unretained NSString *model;
	__unsafe_unretained NSString *position;
	__unsafe_unretained NSString *prgshima_exists;
	__unsafe_unretained NSString *prgstoll_exists;
	__unsafe_unretained NSString *room;
	__unsafe_unretained NSString *stitch_type_codes;
	__unsafe_unretained NSString *stored;
	__unsafe_unretained NSString *string_code;
	__unsafe_unretained NSString *supplier_codes;
	__unsafe_unretained NSString *thinness;
	__unsafe_unretained NSString *updated_images;
	__unsafe_unretained NSString *yarn_codes;
	__unsafe_unretained NSString *year;
} ProductAttributes;

extern const struct ProductRelationships {
	__unsafe_unretained NSString *brand;
	__unsafe_unretained NSString *brand_type;
	__unsafe_unretained NSString *category;
	__unsafe_unretained NSString *family;
	__unsafe_unretained NSString *macro_season;
	__unsafe_unretained NSString *pictures;
	__unsafe_unretained NSString *product_fibres;
} ProductRelationships;

extern const struct ProductFetchedProperties {
} ProductFetchedProperties;

@class Brand;
@class BrandType;
@class ProductCategory;
@class Family;
@class MacroSeason;
@class Picture;
@class ProductFibre;

























@interface ProductID : NSManagedObjectID {}
@end

@interface _Product : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (ProductID*)objectID;





@property (nonatomic, strong) NSNumber* brand_code;



@property int32_t brand_codeValue;
- (int32_t)brand_codeValue;
- (void)setBrand_codeValue:(int32_t)value_;

//- (BOOL)validateBrand_code:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* brand_type_code;



@property int32_t brand_type_codeValue;
- (int32_t)brand_type_codeValue;
- (void)setBrand_type_codeValue:(int32_t)value_;

//- (BOOL)validateBrand_type_code:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* category_code;



@property int32_t category_codeValue;
- (int32_t)category_codeValue;
- (void)setCategory_codeValue:(int32_t)value_;

//- (BOOL)validateCategory_code:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* code;



@property int32_t codeValue;
- (int32_t)codeValue;
- (void)setCodeValue:(int32_t)value_;

//- (BOOL)validateCode:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* default_image;



//- (BOOL)validateDefault_image:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* family_code;



@property int32_t family_codeValue;
- (int32_t)family_codeValue;
- (void)setFamily_codeValue:(int32_t)value_;

//- (BOOL)validateFamily_code:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* fibre_codes;



//- (BOOL)validateFibre_codes:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* item_description;



//- (BOOL)validateItem_description:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* language;



//- (BOOL)validateLanguage:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* macro_season_code;



@property int32_t macro_season_codeValue;
- (int32_t)macro_season_codeValue;
- (void)setMacro_season_codeValue:(int32_t)value_;

//- (BOOL)validateMacro_season_code:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* model;



//- (BOOL)validateModel:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* position;



@property int32_t positionValue;
- (int32_t)positionValue;
- (void)setPositionValue:(int32_t)value_;

//- (BOOL)validatePosition:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* prgshima_exists;



@property BOOL prgshima_existsValue;
- (BOOL)prgshima_existsValue;
- (void)setPrgshima_existsValue:(BOOL)value_;

//- (BOOL)validatePrgshima_exists:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* prgstoll_exists;



@property BOOL prgstoll_existsValue;
- (BOOL)prgstoll_existsValue;
- (void)setPrgstoll_existsValue:(BOOL)value_;

//- (BOOL)validatePrgstoll_exists:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* room;



@property int32_t roomValue;
- (int32_t)roomValue;
- (void)setRoomValue:(int32_t)value_;

//- (BOOL)validateRoom:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* stitch_type_codes;



//- (BOOL)validateStitch_type_codes:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* stored;



@property BOOL storedValue;
- (BOOL)storedValue;
- (void)setStoredValue:(BOOL)value_;

//- (BOOL)validateStored:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* string_code;



//- (BOOL)validateString_code:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* supplier_codes;



//- (BOOL)validateSupplier_codes:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* thinness;



@property int32_t thinnessValue;
- (int32_t)thinnessValue;
- (void)setThinnessValue:(int32_t)value_;

//- (BOOL)validateThinness:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* updated_images;



@property BOOL updated_imagesValue;
- (BOOL)updated_imagesValue;
- (void)setUpdated_imagesValue:(BOOL)value_;

//- (BOOL)validateUpdated_images:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* yarn_codes;



//- (BOOL)validateYarn_codes:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* year;



@property int32_t yearValue;
- (int32_t)yearValue;
- (void)setYearValue:(int32_t)value_;

//- (BOOL)validateYear:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) Brand *brand;

//- (BOOL)validateBrand:(id*)value_ error:(NSError**)error_;




@property (nonatomic, strong) NSSet *brand_type;

- (NSMutableSet*)brand_typeSet;




@property (nonatomic, strong) ProductCategory *category;

//- (BOOL)validateCategory:(id*)value_ error:(NSError**)error_;




@property (nonatomic, strong) Family *family;

//- (BOOL)validateFamily:(id*)value_ error:(NSError**)error_;




@property (nonatomic, strong) MacroSeason *macro_season;

//- (BOOL)validateMacro_season:(id*)value_ error:(NSError**)error_;




@property (nonatomic, strong) NSSet *pictures;

- (NSMutableSet*)picturesSet;




@property (nonatomic, strong) NSSet *product_fibres;

- (NSMutableSet*)product_fibresSet;





@end

@interface _Product (CoreDataGeneratedAccessors)

- (void)addBrand_type:(NSSet*)value_;
- (void)removeBrand_type:(NSSet*)value_;
- (void)addBrand_typeObject:(BrandType*)value_;
- (void)removeBrand_typeObject:(BrandType*)value_;

- (void)addPictures:(NSSet*)value_;
- (void)removePictures:(NSSet*)value_;
- (void)addPicturesObject:(Picture*)value_;
- (void)removePicturesObject:(Picture*)value_;

- (void)addProduct_fibres:(NSSet*)value_;
- (void)removeProduct_fibres:(NSSet*)value_;
- (void)addProduct_fibresObject:(ProductFibre*)value_;
- (void)removeProduct_fibresObject:(ProductFibre*)value_;

@end

@interface _Product (CoreDataGeneratedPrimitiveAccessors)


- (NSNumber*)primitiveBrand_code;
- (void)setPrimitiveBrand_code:(NSNumber*)value;

- (int32_t)primitiveBrand_codeValue;
- (void)setPrimitiveBrand_codeValue:(int32_t)value_;




- (NSNumber*)primitiveBrand_type_code;
- (void)setPrimitiveBrand_type_code:(NSNumber*)value;

- (int32_t)primitiveBrand_type_codeValue;
- (void)setPrimitiveBrand_type_codeValue:(int32_t)value_;




- (NSNumber*)primitiveCategory_code;
- (void)setPrimitiveCategory_code:(NSNumber*)value;

- (int32_t)primitiveCategory_codeValue;
- (void)setPrimitiveCategory_codeValue:(int32_t)value_;




- (NSNumber*)primitiveCode;
- (void)setPrimitiveCode:(NSNumber*)value;

- (int32_t)primitiveCodeValue;
- (void)setPrimitiveCodeValue:(int32_t)value_;




- (NSString*)primitiveDefault_image;
- (void)setPrimitiveDefault_image:(NSString*)value;




- (NSNumber*)primitiveFamily_code;
- (void)setPrimitiveFamily_code:(NSNumber*)value;

- (int32_t)primitiveFamily_codeValue;
- (void)setPrimitiveFamily_codeValue:(int32_t)value_;




- (NSString*)primitiveFibre_codes;
- (void)setPrimitiveFibre_codes:(NSString*)value;




- (NSString*)primitiveItem_description;
- (void)setPrimitiveItem_description:(NSString*)value;




- (NSString*)primitiveLanguage;
- (void)setPrimitiveLanguage:(NSString*)value;




- (NSNumber*)primitiveMacro_season_code;
- (void)setPrimitiveMacro_season_code:(NSNumber*)value;

- (int32_t)primitiveMacro_season_codeValue;
- (void)setPrimitiveMacro_season_codeValue:(int32_t)value_;




- (NSString*)primitiveModel;
- (void)setPrimitiveModel:(NSString*)value;




- (NSNumber*)primitivePosition;
- (void)setPrimitivePosition:(NSNumber*)value;

- (int32_t)primitivePositionValue;
- (void)setPrimitivePositionValue:(int32_t)value_;




- (NSNumber*)primitivePrgshima_exists;
- (void)setPrimitivePrgshima_exists:(NSNumber*)value;

- (BOOL)primitivePrgshima_existsValue;
- (void)setPrimitivePrgshima_existsValue:(BOOL)value_;




- (NSNumber*)primitivePrgstoll_exists;
- (void)setPrimitivePrgstoll_exists:(NSNumber*)value;

- (BOOL)primitivePrgstoll_existsValue;
- (void)setPrimitivePrgstoll_existsValue:(BOOL)value_;




- (NSNumber*)primitiveRoom;
- (void)setPrimitiveRoom:(NSNumber*)value;

- (int32_t)primitiveRoomValue;
- (void)setPrimitiveRoomValue:(int32_t)value_;




- (NSString*)primitiveStitch_type_codes;
- (void)setPrimitiveStitch_type_codes:(NSString*)value;




- (NSNumber*)primitiveStored;
- (void)setPrimitiveStored:(NSNumber*)value;

- (BOOL)primitiveStoredValue;
- (void)setPrimitiveStoredValue:(BOOL)value_;




- (NSString*)primitiveString_code;
- (void)setPrimitiveString_code:(NSString*)value;




- (NSString*)primitiveSupplier_codes;
- (void)setPrimitiveSupplier_codes:(NSString*)value;




- (NSNumber*)primitiveThinness;
- (void)setPrimitiveThinness:(NSNumber*)value;

- (int32_t)primitiveThinnessValue;
- (void)setPrimitiveThinnessValue:(int32_t)value_;




- (NSNumber*)primitiveUpdated_images;
- (void)setPrimitiveUpdated_images:(NSNumber*)value;

- (BOOL)primitiveUpdated_imagesValue;
- (void)setPrimitiveUpdated_imagesValue:(BOOL)value_;




- (NSString*)primitiveYarn_codes;
- (void)setPrimitiveYarn_codes:(NSString*)value;




- (NSNumber*)primitiveYear;
- (void)setPrimitiveYear:(NSNumber*)value;

- (int32_t)primitiveYearValue;
- (void)setPrimitiveYearValue:(int32_t)value_;





- (Brand*)primitiveBrand;
- (void)setPrimitiveBrand:(Brand*)value;



- (NSMutableSet*)primitiveBrand_type;
- (void)setPrimitiveBrand_type:(NSMutableSet*)value;



- (ProductCategory*)primitiveCategory;
- (void)setPrimitiveCategory:(ProductCategory*)value;



- (Family*)primitiveFamily;
- (void)setPrimitiveFamily:(Family*)value;



- (MacroSeason*)primitiveMacro_season;
- (void)setPrimitiveMacro_season:(MacroSeason*)value;



- (NSMutableSet*)primitivePictures;
- (void)setPrimitivePictures:(NSMutableSet*)value;



- (NSMutableSet*)primitiveProduct_fibres;
- (void)setPrimitiveProduct_fibres:(NSMutableSet*)value;


@end
