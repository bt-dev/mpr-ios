#import "_ProductCategory.h"
#import "S2SOrderedDictionary.h"

@interface ProductCategory : _ProductCategory {}
// Custom logic goes here.
+ (S2SOrderedDictionary*)filterValues;
@end
