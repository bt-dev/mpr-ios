//
//  BTActionSheet.m
//  NeedleIOS
//
//  Created by Davide Cenzi on 29/12/13.
//  Copyright (c) 2013 Davide Cenzi. All rights reserved.
//

#import "BTActionSheet.h"

@implementation BTActionSheet

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (instancetype)initWithTitle:(NSString *)title cancelButtonTitle:(NSString *)cancelButtonTitle destructiveButtonTitle:(NSString *)destructiveButtonTitle otherButtonTitles:(NSString *)otherButtonTitles onPressBlock:(BTActionSheetOnPress)onPress onCancelBlock:(BTActionSheetOnCancel)onCancel{
    
    self = [super initWithTitle:title delegate:self cancelButtonTitle:cancelButtonTitle destructiveButtonTitle:destructiveButtonTitle otherButtonTitles:otherButtonTitles, nil];
    
    if(self){
        _onCancel = onCancel;
        _onPress = onPress;
    }
    
    return self;
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if(actionSheet.cancelButtonIndex == buttonIndex)
        return;
    
    if(_onPress)
        _onPress(actionSheet, buttonIndex);
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex{
    if(actionSheet.cancelButtonIndex == buttonIndex && _onCancel)
        _onCancel(actionSheet);
}

- (void)actionSheetCancel:(UIActionSheet *)actionSheet{
    if(_onCancel)
        _onCancel(actionSheet);
    
    //[actionSheet dismissWithClickedButtonIndex:0 animated:YES];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
