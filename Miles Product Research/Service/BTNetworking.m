//
//  BTNetworking.m
//  MobileCatalogTemplate
//
//  Created by Davide Cenzi on 16/07/13.
//  Copyright (c) 2013 BlueTouch Development di Davide Cenzi. All rights reserved.
//

#import "BTNetworking.h"

@implementation BTNetworking

- (instancetype)initWithBaseUrl:(NSString *)baseUrl modelName:(NSString *)modelName
{
    self = [super init];
    
    if (self)
    {
        self.baseUrl = baseUrl;
        self.modelName = modelName;
        
        [self configureNetworking];
        [self configureMapping];
    }
    
    return self;
}


- (void)configureNetworking
{
    // Log debugging info about Core Data
    
    RKLogConfigureByName("RestKit/CoreData", RKLogLevelWarning);
    
    //Loging about the Entity Mapping
    RKLogConfigureByName("RestKit/ObjectMapping", RKLogLevelWarning)
    
    // Logging Networking
    RKLogConfigureByName("RestKit/Network", RKLogLevelWarning);
    
    // Serialize to JSON
    [RKObjectManager sharedManager].requestSerializationMIMEType = RKMIMETypeJSON;
    [RKMIMETypeSerialization registerClass:[RKNSJSONSerialization class] forMIMEType:@"text/plain"];
    
    // Model
    RKObjectManager* objectManager = [RKObjectManager managerWithBaseURL:[NSURL URLWithString:self.baseUrl]];
    NSURL *modelURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:self.modelName ofType:@"momd"]];
    
    // NOTE: Due to an iOS 5 bug, the managed object model returned is immutable.
    NSManagedObjectModel *managedObjectModel = [[[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL] mutableCopy];
    RKManagedObjectStore *managedObjectStore = [[RKManagedObjectStore alloc] initWithManagedObjectModel:managedObjectModel];
    
    
    // Initialize the Core Data stack
    [managedObjectStore createPersistentStoreCoordinator];
    
    //Not used
    NSError *error = nil;
    NSString *path = [RKApplicationDataDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.sqlite", self.modelName]];
    NSString *seedPath = [[NSBundle mainBundle] pathForResource:@"Needle_seed" ofType:@"sqlite"];
    
    //disabling seeding
    seedPath = nil;
    
    [managedObjectStore addSQLitePersistentStoreAtPath:path fromSeedDatabaseAtPath:seedPath withConfiguration:nil options:nil error:&error];
    [managedObjectStore createManagedObjectContexts];
    
    
    // Set the default store shared instance
    objectManager.managedObjectStore = managedObjectStore;
    
    // Configure a managed object cache to ensure we do not create duplicate objects
    objectManager.managedObjectStore.managedObjectCache = [[RKInMemoryManagedObjectCache alloc] initWithManagedObjectContext:managedObjectStore.persistentStoreManagedObjectContext];
    
    
    //set spinner when client is busy
    [AFNetworkActivityIndicatorManager sharedManager].enabled = YES;
}


- (void)configureMapping
{
    // Setting della Basic Authentication per poter accedere alle API.
    //[[RKObjectManager sharedManager].HTTPClient setAuthorizationHeaderWithUsername:@"link82" password:@"test"];
    
    // Recupero oggetti per mappatura
    
    RKManagedObjectStore *store = [RKObjectManager sharedManager].managedObjectStore;
    RKObjectManager *manager = [RKObjectManager sharedManager];
    
    // Status codes
    NSIndexSet *errorStatusCodes = RKStatusCodeIndexSetForClass(RKStatusCodeClassClientError);
    NSIndexSet *successfulStatusCodes = RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful);
    
    
    
    /****** START SUPPLIER MAPPING ******/
    
    
    
    // Supplier
    RKEntityMapping *supplierMapping = [RKEntityMapping mappingForEntityForName:NSStringFromClass([Supplier class]) inManagedObjectStore:store];
    [supplierMapping setIdentificationAttributes:@[@"code"]];
    
    [supplierMapping addAttributeMappingsFromDictionary:@{
                                                          @"id":@"code",
                                                          @"description":@"item_description",
                                                          @"code":@"string_code"
                                                          }];
    
    
    //User Response descriptor
    RKResponseDescriptor *supplierResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:supplierMapping
                                                                                                    method:RKRequestMethodGET pathPattern:@"/it/suppliers" keyPath:nil statusCodes:successfulStatusCodes];
    [manager addResponseDescriptor:supplierResponseDescriptor];
    
    //Route
    [manager.router.routeSet addRoute:[RKRoute
                                       routeWithClass:[Supplier class]
                                       pathPattern:@"/it/suppliers/:code"
                                       method:RKRequestMethodGET]];
    
    /****** END SUPPLIERS MAPPING ******/
    
    
    
    /****** START YARN MAPPING ******/
    
    // Yarn
    RKEntityMapping *yarnMapping = [RKEntityMapping mappingForEntityForName:NSStringFromClass([Yarn class]) inManagedObjectStore:store];
    [yarnMapping setIdentificationAttributes:@[@"code"]];
    
    [yarnMapping addAttributeMappingsFromDictionary:@{
                                                      @"id":@"code",
                                                      @"code":@"string_code",
                                                      @"description":@"item_description",
                                                      @"supplier_id":@"supplier_code",
                                                      @"supplier_code":@"supplier_string_code",
                                                      @"description":@"item_description",
                                                      }];
    
    [yarnMapping addConnectionForRelationship:@"supplier" connectedBy:@{@"supplier_code": @"code"}];
    
    //Yarn
    
    RKResponseDescriptor *yarnResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:yarnMapping
                                                                                                method:RKRequestMethodGET pathPattern:@"/it/yarns" keyPath:nil statusCodes:successfulStatusCodes];
    [manager addResponseDescriptor:yarnResponseDescriptor];
    
    //Yarn Route
    [manager.router.routeSet addRoute:[RKRoute
                                       routeWithClass:[Yarn class]
                                       pathPattern:@"/it/yarns/:code"
                                       method:RKRequestMethodGET]];
    /****** END YARN MAPPING ******/
    
    /****** START MACRO SEASON MAPPING ******/
    
    // Macro Season
    RKEntityMapping *seasonMapping = [RKEntityMapping mappingForEntityForName:NSStringFromClass([MacroSeason class]) inManagedObjectStore:store];
    [seasonMapping setIdentificationAttributes:@[ @"code", @"language" ]];
    
    [seasonMapping addAttributeMappingsFromDictionary:@{
                                                        @"id":@"code",
                                                        @"description":@"item_description",
                                                        @"code":@"string_code",
                                                        @"language":@"language"
                                                        }];
    
    
    //User Response descriptor
    RKResponseDescriptor *seasonResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:seasonMapping
                                                                                                  method:RKRequestMethodGET pathPattern:@"/:language/macro_seasons" keyPath:nil statusCodes:successfulStatusCodes];
    [manager addResponseDescriptor:seasonResponseDescriptor];
    
    //Route
    [manager.router.routeSet addRoute:[RKRoute
                                       routeWithClass:[MacroSeason class]
                                       pathPattern:@"/:language/macro_seasons/:code"
                                       method:RKRequestMethodGET]];
    
    
    /****** END MACRO SEASON MAPPING ******/
    
    
    /****** START BRAND MAPPING ******/
    
    // Brand
    RKEntityMapping *brandMapping = [RKEntityMapping mappingForEntityForName:NSStringFromClass([Brand class]) inManagedObjectStore:store];
    [brandMapping setIdentificationAttributes:@[ @"code"]];
    
    [brandMapping addAttributeMappingsFromDictionary:@{
                                                       @"id":@"code",
                                                       @"description":@"item_description",
                                                       @"code":@"string_code",
                                                       }];
    
    
    //User Response descriptor
    
    RKResponseDescriptor *brandResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:brandMapping
                                                                                                 method:RKRequestMethodGET pathPattern:@"/it/brands" keyPath:nil statusCodes:successfulStatusCodes];
    [manager addResponseDescriptor:brandResponseDescriptor];
    
    //Yarn Route
    [manager.router.routeSet addRoute:[RKRoute
                                       routeWithClass:[Brand class]
                                       pathPattern:@"/it/brands/:code"
                                       method:RKRequestMethodGET]];
    
    /****** END MACRO BRAND MAPPING ******/
    
    
    
    /****** START BRAND TYPE MAPPING ******/
    
    // Brand
    RKEntityMapping *brandTypeMapping = [RKEntityMapping mappingForEntityForName:NSStringFromClass([BrandType class]) inManagedObjectStore:store];
    [brandTypeMapping setIdentificationAttributes:@[ @"code", @"language"]];
    
    [brandTypeMapping addAttributeMappingsFromDictionary:@{
                                                           @"id":@"code",
                                                           @"description":@"item_description",
                                                           @"code":@"string_code",
                                                           @"language": @"language"
                                                           }];
    
    
    //User Response descriptor
    
    RKResponseDescriptor *brandTypeResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:brandTypeMapping
                                                                                                     method:RKRequestMethodGET pathPattern:@"/:language/brand_types" keyPath:nil statusCodes:successfulStatusCodes];
    [manager addResponseDescriptor:brandTypeResponseDescriptor];
    
    //Yarn Route
    [manager.router.routeSet addRoute:[RKRoute
                                       routeWithClass:[BrandType class]
                                       pathPattern:@"/:language/brand_types/:code"
                                       method:RKRequestMethodGET]];
    
    /****** END BRAND TYPE MAPPING ******/
    
    
    
    /****** START FAMILY MAPPING ******/
    
    RKEntityMapping *familyMapping = [RKEntityMapping mappingForEntityForName:NSStringFromClass([Family class]) inManagedObjectStore:store];
    [familyMapping setIdentificationAttributes:@[ @"code", @"language"]];
    
    [familyMapping addAttributeMappingsFromDictionary:@{
                                                        @"id":@"code",
                                                        @"description":@"item_description",
                                                        @"code":@"string_code",
                                                        @"language": @"language"
                                                        }];
    
    
    //User Response descriptor
    
    RKResponseDescriptor *familyResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:familyMapping
                                                                                                  method:RKRequestMethodGET pathPattern:@"/:language/families" keyPath:nil statusCodes:successfulStatusCodes];
    [manager addResponseDescriptor:familyResponseDescriptor];
    
    //Route
    [manager.router.routeSet addRoute:[RKRoute
                                       routeWithClass:[Family class]
                                       pathPattern:@"/:language/families/:code"
                                       method:RKRequestMethodGET]];
    
    /****** END FAMILY MAPPING ******/
    
    
    
    
    
    /****** START PRODUCT CATEGORY MAPPING ******/
    
    // Brand
    RKEntityMapping *productCategoryMapping = [RKEntityMapping mappingForEntityForName:NSStringFromClass([ProductCategory class]) inManagedObjectStore:store];
    [productCategoryMapping setIdentificationAttributes:@[ @"code", @"language"]];
    
    [productCategoryMapping addAttributeMappingsFromDictionary:@{
                                                                 @"id":@"code",
                                                                 @"code":@"string_code",
                                                                 @"description":@"item_description",
                                                                 @"family_id":@"family_code",
                                                                 @"family_code":@"family_string_code",
                                                                 @"language": @"language"
                                                                 }];
    
    
    
    [productCategoryMapping addConnectionForRelationship:@"family" connectedBy:@{@"family_code": @"code", @"language" : @"language"}];
    
    RKResponseDescriptor *productCategoryResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:productCategoryMapping
                                                                                                           method:RKRequestMethodGET pathPattern:@"/:language/categories" keyPath:nil statusCodes:successfulStatusCodes];
    [manager addResponseDescriptor:productCategoryResponseDescriptor];
    
    //Yarn Route
    [manager.router.routeSet addRoute:[RKRoute
                                       routeWithClass:[ProductCategory class]
                                       pathPattern:@"/:language/categories/:code"
                                       method:RKRequestMethodGET]];
    
    /****** END PRODUCT CATEGORY MAPPING ******/
    
    
    /****** START FIBRE MAPPING ******/
    
    // Brand
    RKEntityMapping *fibreMapping = [RKEntityMapping mappingForEntityForName:NSStringFromClass([Fibre class]) inManagedObjectStore:store];
    [fibreMapping setIdentificationAttributes:@[ @"code", @"language"]];
    
    [fibreMapping addAttributeMappingsFromDictionary:@{
                                                       @"id":@"code",
                                                       @"description":@"item_description",
                                                       @"code":@"string_code",
                                                       @"language": @"language"
                                                       }];
    
    
    //User Response descriptor
    
    RKResponseDescriptor *fibreResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:fibreMapping
                                                                                                 method:RKRequestMethodGET pathPattern:@"/:language/fibres" keyPath:nil statusCodes:successfulStatusCodes];
    [manager addResponseDescriptor:fibreResponseDescriptor];
    
    //Route
    [manager.router.routeSet addRoute:[RKRoute
                                       routeWithClass:[Fibre class]
                                       pathPattern:@"/:language/fibres/:code"
                                       method:RKRequestMethodGET]];
    
    /****** END FIBRE MAPPING ******/
    
    
    
    
    
    /****** START STITCH TYPE MAPPING ******/
    
    // Stitch type
    RKEntityMapping *stitchTypeMapping = [RKEntityMapping mappingForEntityForName:NSStringFromClass([StitchType class]) inManagedObjectStore:store];
    [stitchTypeMapping setIdentificationAttributes:@[ @"code", @"language" ]];
    
    [stitchTypeMapping addAttributeMappingsFromDictionary:@{
                                                            @"id":@"code",
                                                            @"language":@"language",
                                                            @"description":@"item_description",
                                                            @"full_description":@"full_description",
                                                            @"code":@"string_code",
                                                            @"parent_id":@"parent_code",
                                                            @"leaf":@"leaf",
                                                            @"created_at":@"created_at",
                                                            @"updated_at":@"updated_at"
                                                            }];
    
    [stitchTypeMapping addConnectionForRelationship:@"parent" connectedBy:@{@"parent_code": @"code", @"language":@"language"}];
    
    RKResponseDescriptor *stitchTypeResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:stitchTypeMapping
                                                                                                      method:RKRequestMethodGET pathPattern:@"/:language/stitch_types" keyPath:nil statusCodes:successfulStatusCodes];
    [manager addResponseDescriptor:stitchTypeResponseDescriptor];
    
    //Stitch Type Route
    [manager.router.routeSet addRoute:[RKRoute
                                       routeWithClass:[StitchType class]
                                       pathPattern:@"/:language/stitch_types/:code"
                                       method:RKRequestMethodGET]];
    
    /****** END STITCH TYPE MAPPING ******/
    
    
    /****** START PICTURE MAPPING ******/
    
    // Picture
    RKEntityMapping *pictureMapping = [RKEntityMapping mappingForEntityForName:NSStringFromClass([Picture class]) inManagedObjectStore:store];
    [pictureMapping setIdentificationAttributes:@[ @"code", @"language"]];
    
    [pictureMapping addAttributeMappingsFromDictionary:@{
                                                         @"id":@"code",
                                                         @"description":@"image_file_name",
                                                         @"product_id":@"product_code",
                                                         @"url":@"url",
                                                         @"sketch":@"sketch",
                                                         @"language":@"language"
                                                         }];
    
    [pictureMapping addConnectionForRelationship:@"product" connectedBy:@{@"product_code": @"code", @"language":@"language"}];
    
    /****** END PICTURE MAPPING ******/
    
    
    /****** START PICTURE MAPPING ******/
    
    // Picture
    RKEntityMapping *productFibreMapping = [RKEntityMapping mappingForEntityForName:NSStringFromClass([ProductFibre class]) inManagedObjectStore:store];
    [productFibreMapping setIdentificationAttributes:@[ @"code", @"language"]];
    
    [productFibreMapping addAttributeMappingsFromDictionary:@{
                                                         @"id":@"code",
                                                         @"product_id":@"product_code",
                                                         @"fibre_id":@"fibre_code",
                                                         @"percentage":@"percentage",
                                                         @"language":@"language"
                                                         }];
    
    [productFibreMapping addConnectionForRelationship:@"product" connectedBy:@{@"product_code": @"code", @"language":@"language"}];
    [productFibreMapping addConnectionForRelationship:@"fibre" connectedBy:@{@"fibre_code": @"code", @"language":@"language"}];
    /****** END PICTURE MAPPING ******/
    
    
    
    
    
    /****** START PRODUCT MAPPING ******/
    
    // Product
    RKEntityMapping *productMapping = [RKEntityMapping mappingForEntityForName:NSStringFromClass([Product class]) inManagedObjectStore:store];
    [productMapping setIdentificationAttributes:@[ @"code", @"language" ]];
    
    [productMapping addAttributeMappingsFromDictionary:@{
                                                         @"id":@"code",
                                                         @"brand_id":@"brand_code",
                                                         @"brand_type_id":@"brand_type_code",
                                                         @"category_id":@"category_code",
                                                         @"default_image":@"default_image",
                                                         @"description":@"item_description",
                                                         @"family_id":@"family_code",
                                                         @"macro_season_id":@"macro_season_code",
                                                         @"model":@"model",
                                                         @"position": @"position",
                                                         @"prgshima_exists" : @"prgshima_exists",
                                                         @"prgstoll_exists" :@"prgstoll_exists",
                                                         @"room": @"room",
                                                         @"stored":@"stored",
                                                         @"thinness":@"thinness",
                                                         @"year":@"year",
                                                         @"yarn_codes":@"yarn_codes",
                                                         @"supplier_codes":@"supplier_codes",
                                                         @"fibre_codes":@"fibre_codes",
                                                         @"stitch_type_codes":@"stitch_type_codes",
                                                         @"language":@"language",
                                                         }];
    
    [productMapping addConnectionForRelationship:@"brand" connectedBy:@{@"brand_code": @"code"}];
    [productMapping addConnectionForRelationship:@"brand_type" connectedBy:@{@"brand_type_code": @"code", @"language":@"language"}];
    [productMapping addConnectionForRelationship:@"category" connectedBy:@{@"category_code": @"code", @"language":@"language"}];
    [productMapping addConnectionForRelationship:@"family" connectedBy:@{@"family_code": @"code", @"language":@"language"}];
    [productMapping addConnectionForRelationship:@"macro_season" connectedBy:@{@"macro_season_code": @"code", @"language":@"language"}];
    
    [productMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"pictures" toKeyPath:@"pictures" withMapping:pictureMapping]];
    [productMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"product_fibres" toKeyPath:@"product_fibres" withMapping:productFibreMapping]];
    
    //User Response descriptor
    RKResponseDescriptor *productResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:productMapping
                                                                                                   method:RKRequestMethodGET pathPattern:@"/:language/products" keyPath:nil statusCodes:successfulStatusCodes];
    [manager addResponseDescriptor:productResponseDescriptor];
    
    //Yarn Route
    [manager.router.routeSet addRoute:[RKRoute
                                       routeWithClass:[Product class]
                                       pathPattern:@"/:language/products/:code"
                                       method:RKRequestMethodGET]];
    
    /****** END PRODUCT MAPPING ******/
    
    
    if(false){
        //Error mapping
        RKObjectMapping *errorMapping = [RKObjectMapping mappingForClass:[RKErrorMessage class]];
        [errorMapping addPropertyMapping: [RKAttributeMapping attributeMappingFromKeyPath:nil toKeyPath:@"errorMessage"]];
        RKResponseDescriptor *errorDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:errorMapping method:RKRequestMethodGET pathPattern:nil keyPath:@"error" statusCodes:errorStatusCodes];
        
        [manager addResponseDescriptor:errorDescriptor];
    }
    
}


@end
