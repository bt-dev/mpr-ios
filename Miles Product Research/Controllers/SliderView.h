//
//  SliderView.h
//  Miles Product Research
//
//  Created by Davide Cenzi on 06/07/14.
//  Copyright (c) 2014 BlueTouch Development di Davide Cenzi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SliderView : UIView{
    BOOL _active;
}

@property (nonatomic, strong) NSNumber *productCode;

@property (nonatomic, weak) IBOutlet UIImageView *productImage;
@property (nonatomic, weak) IBOutlet UILabel *seasonLabel;
@property (nonatomic, weak) IBOutlet UILabel *supplierLabel;
@property (nonatomic, weak) IBOutlet UILabel *categoryLabel;
@property (nonatomic, weak) IBOutlet UIButton *selectionButton;

- (BOOL)isActive;
- (void)setActive:(BOOL)active;

@end
