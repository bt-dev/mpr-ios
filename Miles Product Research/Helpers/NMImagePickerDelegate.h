//
//  NMImagePickerDelegate.h
//  Needle
//
//  Created by Davide Cenzi on 21/08/13.
//  Copyright (c) 2013 BlueTouch Development di Davide Cenzi. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^NMPickedImageBlock)(UIImage *image);
typedef void (^NMDismissedPickerBlock)(void);


@interface NMImagePickerDelegate : NSObject <UINavigationControllerDelegate, UIImagePickerControllerDelegate, UIAlertViewDelegate>{
    UIImage *_selectedImage;
    NSString *_alertText;
    
    id _viewController;
    UIImagePickerControllerSourceType _sourceType;
    UIImagePickerController * _pickerController;
    NMPickedImageBlock _pickedBlock;
    NMDismissedPickerBlock _dismissedBlock;
    
}



//UIImagePickerControllerSourceType
- (id)initWithSourceType:(UIImagePickerControllerSourceType)sourceType pickedImageBlock:(NMPickedImageBlock)pickedBlock dismissedBlock:(NMDismissedPickerBlock)dismissedBlock;

- (id)initWithRequestText:(NSString*)alertText pickedImageBlock:(NMPickedImageBlock)pickedBlock dismissedBlock:(NMDismissedPickerBlock)dismissedBlock;

- (void)presentInViewController:(id)viewController;

@end
