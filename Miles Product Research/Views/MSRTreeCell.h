//
//  MSRTreeCell.h
//  MSR
//
//  Created by Davide Cenzi on 17/12/13.
//  Copyright (c) 2013 BlueTouch Development di Davide Cenzi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MSRTreeCell : UITableViewCell{
}
@property (nonatomic, weak) IBOutlet UIButton *checkButton;
@property (nonatomic, weak) IBOutlet UILabel *titleLabel;

@end
