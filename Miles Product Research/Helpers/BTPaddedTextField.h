//
//  BTPaddedTextField.h
//  NeedleIOS
//
//  Created by Davide Cenzi on 23/01/13.
//  Copyright (c) 2013 Davide Cenzi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BTPaddedTextField : UITextField

@property (nonatomic, assign) float verticalPadding;
@property (nonatomic, assign) float horizontalPadding;

@end
