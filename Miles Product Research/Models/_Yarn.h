// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Yarn.h instead.

#import <CoreData/CoreData.h>


extern const struct YarnAttributes {
	__unsafe_unretained NSString *code;
	__unsafe_unretained NSString *item_description;
	__unsafe_unretained NSString *string_code;
	__unsafe_unretained NSString *supplier_code;
	__unsafe_unretained NSString *supplier_string_code;
} YarnAttributes;

extern const struct YarnRelationships {
	__unsafe_unretained NSString *supplier;
} YarnRelationships;

extern const struct YarnFetchedProperties {
} YarnFetchedProperties;

@class Supplier;







@interface YarnID : NSManagedObjectID {}
@end

@interface _Yarn : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (YarnID*)objectID;





@property (nonatomic, strong) NSNumber* code;



@property int32_t codeValue;
- (int32_t)codeValue;
- (void)setCodeValue:(int32_t)value_;

//- (BOOL)validateCode:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* item_description;



//- (BOOL)validateItem_description:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* string_code;



//- (BOOL)validateString_code:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* supplier_code;



//- (BOOL)validateSupplier_code:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* supplier_string_code;



//- (BOOL)validateSupplier_string_code:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) Supplier *supplier;

//- (BOOL)validateSupplier:(id*)value_ error:(NSError**)error_;





@end

@interface _Yarn (CoreDataGeneratedAccessors)

@end

@interface _Yarn (CoreDataGeneratedPrimitiveAccessors)


- (NSNumber*)primitiveCode;
- (void)setPrimitiveCode:(NSNumber*)value;

- (int32_t)primitiveCodeValue;
- (void)setPrimitiveCodeValue:(int32_t)value_;




- (NSString*)primitiveItem_description;
- (void)setPrimitiveItem_description:(NSString*)value;




- (NSString*)primitiveString_code;
- (void)setPrimitiveString_code:(NSString*)value;




- (NSString*)primitiveSupplier_code;
- (void)setPrimitiveSupplier_code:(NSString*)value;




- (NSString*)primitiveSupplier_string_code;
- (void)setPrimitiveSupplier_string_code:(NSString*)value;





- (Supplier*)primitiveSupplier;
- (void)setPrimitiveSupplier:(Supplier*)value;


@end
