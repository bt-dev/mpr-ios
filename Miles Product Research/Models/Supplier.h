#import "_Supplier.h"
#import "S2SOrderedDictionary.h"

@interface Supplier : _Supplier {}
// Custom logic goes here.
+ (S2SOrderedDictionary*)filterValues;
- (NSString *)cleanedDescription;

@end
