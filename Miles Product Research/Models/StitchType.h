#import "_StitchType.h"
#import "S2SOrderedDictionary.h"

@interface StitchType : _StitchType {}
// Custom logic goes here.
+ (S2SOrderedDictionary*)filterValues;
+ (NSArray*)rootObjects;
@end
