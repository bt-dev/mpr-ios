//
//  BTCollapsableCollectionViewController.m
//  MSR
//
//  Created by Davide Cenzi on 04/06/14.
//  Copyright (c) 2014 BlueTouch Development di Davide Cenzi. All rights reserved.
//

#import "BTCollapsableCollectionViewController.h"
#import "BTCollectionHeaderView.h"

@interface BTCollapsableCollectionViewController ()

- (IBAction)tappedOnHeader:(id)sender;

@end

@implementation BTCollapsableCollectionViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (BOOL)collapsedSection:(NSInteger)section{
    BOOL found = false;
    NSString *string_section = [NSString stringWithFormat:@"%d",section];
    for (NSString *sectionNr in self.collapsedSections) {
        if([sectionNr isEqualToString:string_section]){
            found = true;
            break;
        }
    }
    
    return found;
}

- (void)toggleCollapseSection:(NSInteger)section{
    NSString *string_section = [NSString stringWithFormat:@"%d",section];
    
    if(![self collapsedSection:section])
        [self.collapsedSections addObject:string_section];
    else
        [self.collapsedSections removeObject:string_section];
    
    [self.collectionView reloadData];
}

- (IBAction)toggleCollapse:(id)sender{
    UIButton *btn = (UIButton*)sender;
    [self toggleCollapseSection:btn.tag];
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return [[self.fetchedResultsController sections] count];
    
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if([self collapsedSection:section]){
        return 0;
    }
    else{
        id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
        return [sectionInfo numberOfObjects];
    }
    
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    return CGSizeMake(1024.0, 44.0);
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    if (kind == UICollectionElementKindSectionHeader) {
        
        BTCollectionHeaderView *reusableview = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"BTSectionView" forIndexPath:indexPath];
        
        if (reusableview==nil) {
            reusableview=[[BTCollectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, 1024, 44)];
            
            UITapGestureRecognizer *oneFinger = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tappedOnHeader:)];
            [oneFinger setNumberOfTapsRequired:1];
            [reusableview addGestureRecognizer:oneFinger];
        }
        
        [reusableview setBackgroundColor:[UIColor blackColor]];
        reusableview.titleButton.tag = indexPath.section;
        reusableview.collapsedSymbolLabel.text = ([self collapsedSection:indexPath.section] ? @"+" : @"-");
        
        [reusableview.titleLabel setTextColor:[UIColor whiteColor]];
        reusableview.titleLabel.text=[NSString stringWithFormat:@"Section #%i", indexPath.section + 1];
        

        
        return reusableview;
    }
    return nil;
}




- (IBAction)tappedOnHeader:(id)sender{
    UITapGestureRecognizer *r = (UITapGestureRecognizer*)sender;
    UIView *v = r.view;
    [self toggleCollapseSection:v.tag];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    self.collapsedSections = [[NSMutableArray alloc] init];
    
    //[self.collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
    [self.collectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"HeaderView"];

    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
