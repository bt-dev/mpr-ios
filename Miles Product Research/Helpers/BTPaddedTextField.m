//
//  BTPaddedTextField.m
//  NeedleIOS
//
//  Created by Davide Cenzi on 23/01/13.
//  Copyright (c) 2013 Davide Cenzi. All rights reserved.
//

#import "BTPaddedTextField.h"

@implementation BTPaddedTextField
@synthesize horizontalPadding, verticalPadding;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (CGRect)textRectForBounds:(CGRect)bounds {
    return CGRectMake(bounds.origin.x + horizontalPadding, bounds.origin.y + verticalPadding, bounds.size.width - horizontalPadding*2, bounds.size.height - verticalPadding*2);
}
- (CGRect)editingRectForBounds:(CGRect)bounds {
    return [self textRectForBounds:bounds];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
