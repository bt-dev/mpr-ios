// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to StitchType.h instead.

#import <CoreData/CoreData.h>


extern const struct StitchTypeAttributes {
	__unsafe_unretained NSString *code;
	__unsafe_unretained NSString *created_at;
	__unsafe_unretained NSString *full_description;
	__unsafe_unretained NSString *item_description;
	__unsafe_unretained NSString *language;
	__unsafe_unretained NSString *leaf;
	__unsafe_unretained NSString *parent_code;
	__unsafe_unretained NSString *string_code;
	__unsafe_unretained NSString *updated_at;
} StitchTypeAttributes;

extern const struct StitchTypeRelationships {
	__unsafe_unretained NSString *childrens;
	__unsafe_unretained NSString *parent;
} StitchTypeRelationships;

extern const struct StitchTypeFetchedProperties {
} StitchTypeFetchedProperties;

@class StitchType;
@class StitchType;











@interface StitchTypeID : NSManagedObjectID {}
@end

@interface _StitchType : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (StitchTypeID*)objectID;





@property (nonatomic, strong) NSNumber* code;



@property int32_t codeValue;
- (int32_t)codeValue;
- (void)setCodeValue:(int32_t)value_;

//- (BOOL)validateCode:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSDate* created_at;



//- (BOOL)validateCreated_at:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* full_description;



//- (BOOL)validateFull_description:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* item_description;



//- (BOOL)validateItem_description:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* language;



//- (BOOL)validateLanguage:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* leaf;



@property BOOL leafValue;
- (BOOL)leafValue;
- (void)setLeafValue:(BOOL)value_;

//- (BOOL)validateLeaf:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* parent_code;



@property int32_t parent_codeValue;
- (int32_t)parent_codeValue;
- (void)setParent_codeValue:(int32_t)value_;

//- (BOOL)validateParent_code:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* string_code;



//- (BOOL)validateString_code:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSDate* updated_at;



//- (BOOL)validateUpdated_at:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSSet *childrens;

- (NSMutableSet*)childrensSet;




@property (nonatomic, strong) StitchType *parent;

//- (BOOL)validateParent:(id*)value_ error:(NSError**)error_;





@end

@interface _StitchType (CoreDataGeneratedAccessors)

- (void)addChildrens:(NSSet*)value_;
- (void)removeChildrens:(NSSet*)value_;
- (void)addChildrensObject:(StitchType*)value_;
- (void)removeChildrensObject:(StitchType*)value_;

@end

@interface _StitchType (CoreDataGeneratedPrimitiveAccessors)


- (NSNumber*)primitiveCode;
- (void)setPrimitiveCode:(NSNumber*)value;

- (int32_t)primitiveCodeValue;
- (void)setPrimitiveCodeValue:(int32_t)value_;




- (NSDate*)primitiveCreated_at;
- (void)setPrimitiveCreated_at:(NSDate*)value;




- (NSString*)primitiveFull_description;
- (void)setPrimitiveFull_description:(NSString*)value;




- (NSString*)primitiveItem_description;
- (void)setPrimitiveItem_description:(NSString*)value;




- (NSString*)primitiveLanguage;
- (void)setPrimitiveLanguage:(NSString*)value;




- (NSNumber*)primitiveLeaf;
- (void)setPrimitiveLeaf:(NSNumber*)value;

- (BOOL)primitiveLeafValue;
- (void)setPrimitiveLeafValue:(BOOL)value_;




- (NSNumber*)primitiveParent_code;
- (void)setPrimitiveParent_code:(NSNumber*)value;

- (int32_t)primitiveParent_codeValue;
- (void)setPrimitiveParent_codeValue:(int32_t)value_;




- (NSString*)primitiveString_code;
- (void)setPrimitiveString_code:(NSString*)value;




- (NSDate*)primitiveUpdated_at;
- (void)setPrimitiveUpdated_at:(NSDate*)value;





- (NSMutableSet*)primitiveChildrens;
- (void)setPrimitiveChildrens:(NSMutableSet*)value;



- (StitchType*)primitiveParent;
- (void)setPrimitiveParent:(StitchType*)value;


@end
