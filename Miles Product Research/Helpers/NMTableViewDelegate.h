//
//  NMTableViewDelegate.h
//  Needle
//
//  Created by Davide Cenzi on 03/08/13.
//  Copyright (c) 2013 BlueTouch Development di Davide Cenzi. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NSInteger (^NMNumberOfSectionsBlock)(UITableView* tableView);
typedef NSInteger (^NMNumberOfRowsInSectionBlock)(UITableView* tableView, NSInteger section);
typedef UITableViewCell * (^NMCellAtIndexPathBlock)(UITableView* tableView, NSIndexPath* indexPath);

typedef void(^NMSelectAtIndexPathBlock)(UITableView* tableView, NSIndexPath* indexPath);

//optional
typedef CGFloat (^NMCellHeightsBlock)(UITableView *tableView, NSIndexPath* indexPath);
typedef CGFloat (^NMHeaderHeightsBlock)(UITableView *tableView, NSInteger section);
typedef CGFloat (^NMFooterHeightsBlock)(UITableView *tableView, NSInteger section);

typedef UIView * (^NMHeaderViewBlock)(UITableView *tableView, NSInteger section);
typedef UIView * (^NMFooterViewBlock)(UITableView *tableView, NSInteger section);

@interface NMTableViewDelegate : NSObject <UITableViewDataSource, UITableViewDelegate>{
    UITableView *_tableView;
    NMNumberOfSectionsBlock _sectionBlock;
    NMNumberOfRowsInSectionBlock _rowsBlock;
    NMCellAtIndexPathBlock _cellsBlock;
    NMSelectAtIndexPathBlock _selectsBlock;
    
    NMCellHeightsBlock _heightsBlock;
    NMHeaderHeightsBlock _headersHeightsBlock;
    NMFooterHeightsBlock _footersHeightsBlock;
    
    NMHeaderViewBlock _headerViewBlock;
    NMFooterViewBlock _footerViewBlock;
}

- (id)initWithTableView:(UITableView*)tableView sectionsBlock:(NMNumberOfSectionsBlock)sectionsBlock rowsBlock:(NMNumberOfRowsInSectionBlock)rowsBlock cellsBlock:(NMCellAtIndexPathBlock)cellsBlock selectsBlock:(NMSelectAtIndexPathBlock)selectsBlock;

- (void)setCellHeightsBlock:(NMCellHeightsBlock)heightsBlock;

- (void)setHeadersHeightsBlock:(NMHeaderHeightsBlock)heightsBlock;
- (void)setFootersHeightsBlock:(NMFooterHeightsBlock)heightsBlock;

- (void)setHeadersViewBlock:(NMHeaderViewBlock)heightsBlock;
- (void)setFootersViewBlock:(NMFooterViewBlock)heightsBlock;



@end
