//
//  BTSwitchButton.h
//  MSR
//
//  Created by Davide Cenzi on 20/12/13.
//  Copyright (c) 2013 BlueTouch Development di Davide Cenzi. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface BTSwitchButton : UIButton

typedef void (^BTSwitchButtonOnChange)(BTSwitchButton *button, BOOL state);

- (id)initWithTitle:(NSString*)title onChangeBlock:(BTSwitchButtonOnChange)onChangeBlock initialState:(BOOL)checkedState;
- (void)setOnChangeBlock:(BTSwitchButtonOnChange)onChangeBlock;
- (BOOL)checked;

- (IBAction)tappedButton:(id)sender;
@end
