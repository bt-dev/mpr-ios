//
//  FiltersViewController.m
//  Miles Product Research
//
//  Created by Davide Cenzi on 29/06/14.
//  Copyright (c) 2014 BlueTouch Development di Davide Cenzi. All rights reserved.
//

#import "FiltersView.h"
#import "DataModels.h"
#import "NMFieldDelegate.h"
#import "MSRItemsViewController.h"

@interface FiltersView() <GCTagListDelegate, GCTagListDataSource>{
    S2SOrderedDictionary * macro_season_filters;
    S2SOrderedDictionary *  yarn_filters;
    S2SOrderedDictionary *  composition_filters;
    S2SOrderedDictionary *  supplier_filters;
    S2SOrderedDictionary *  family_filters;
    
    S2SOrderedDictionary * composition_percentages;
    S2SOrderedDictionary * activeFilters;
    NMFieldDelegate *fieldsDelegate;
    
    NSString *macro_season_code, *yarn_code, *brand_code, *family_code, *supplier_codes;
    NSString *composition_code0, *composition_perc0, *composition_code1, *composition_perc1, *composition_code2, *composition_perc2;


}

- (void)notifyDelegate;
- (void)applyFiltersAndNotifyDelegate:(BOOL)notifyDelegate;

@end

@implementation FiltersView

#pragma mark - Filters logic

- (void)awakeFromNib{
    
    //setup textfields delegate
    fieldsDelegate = [[NMFieldDelegate alloc] initWithFields:@[self.yearField,self.thinnessField] fieldChangeBlock:^(id textField, id nextField) {
        
    } shouldReturnBlock:^BOOL(id textField) {
        return NO;
    } shouldEndBlock:^BOOL(id textField) {
        
        UITextField *tmp = textField;
        NSString *alert = nil;
        
        if(![tmp.text isEqualToString:@""] && [tmp.text rangeOfString:@" "].location != NSNotFound){
            alert = NSLocalizedString(@"Space characters are not allowed",@"error text text field");
        }
        
        //year field, not empty, not numeric value
        if(([tmp isEqual:self.yearField])&& ![NMHelpers numericString:tmp.text]){
            alert = NSLocalizedString(@"Please insert a valid value",@"error text text field");
        }
        
        if(alert){
            
            double delayInSeconds = 0.5;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                
                UIAlertView *al = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Invalid value",@"error title text field") message:alert delegate:Nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                [al show];
                
            });
            
            
            return NO;
        }
        else{
            [self applyFiltersAndNotifyDelegate:NO];
            return YES;
        }
        
    } withAutoScrollOnView:NO];
    
    self.yearField.keyboardType = UIKeyboardTypePhonePad;
    self.thinnessField.keyboardType = UIKeyboardTypePhonePad;
    activeFilters = [[S2SOrderedDictionary alloc] init];
    
    [self clearFiltersLabels];
}

- (void)setupActiveFiltersView{
    
    self.filterslist.firstRowLeftMargin = 40.f;
    self.filterslist.dataSource = self;
    self.filterslist.backgroundColor = [UIColor blackColor];

    [self.filterslist reloadData];
    
}

#pragma mark - GCTagList Datasource / Delegate methods

- (NSInteger)numberOfTagLabelInTagList:(GCTagList *)tagList {
    return [[activeFilters allKeys] count];
}

- (GCTagLabel*)tagList:(GCTagList *)tagList tagLabelAtIndex:(NSInteger)index {
    
    static NSString* identifier = @"TagLabelIdentifier";
    
    GCTagLabel* tag = [tagList dequeueReusableTagLabelWithIdentifier:identifier];
    if(!tag) {
        tag = [GCTagLabel tagLabelWithReuseIdentifier:identifier];
        tag.labelTextColor = [UIColor whiteColor];
        tag.labelBackgroundColor = [UIColor clearColor];
        //[UIColor colorWithRed:84/255.f green:164/255.f blue:222/255.f alpha:1.f];
    }
    
    NSString *key = [activeFilters keyAtIndex:index];
    [tag setLabelText:[activeFilters objectForKey:key]
        accessoryType:GCTagLabelAccessoryCrossSign];
    
    return tag;
}


/**
 * after reloadData, if the height of TagList has changed, will call this method.
 */
- (void)tagList:(GCTagList *)taglist didChangedHeight:(CGFloat)newHeight{
    
}

/**
 * Tapped the TagLabel, will call this mehtod.
 */
- (void)tagList:(GCTagList *)taglist didSelectedLabelAtIndex:(NSInteger)index{
    
}

/**
 * Tapped the TagLabel's accessoryButton, will call this mehtod.
 */
- (void)tagList:(GCTagList *)tagList accessoryButtonTappedAtIndex:(NSInteger)index{
    //dismiss filter
    NSString *key = [activeFilters keyAtIndex:index];
    
    if([key isEqualToString:@"year"]){
        self.yearField.text = @"";
    }
    else if([key isEqualToString:@"thinness"]){
        self.thinnessField.text = @"";
    }
    else if([key isEqualToString:@"with_pictures"]){
        self.with_pictures.on = NO;
    }
    else if([key isEqualToString:@"stored"]){
        self.stored.on = NO;
    }
    else if([key isEqualToString:@"pgrshima_exists"]){
        self.shima.on = NO;
    }
    else if([key isEqualToString:@"prgstoll_exists"]){
        self.stoll.on = NO;
    }
    else if([key isEqualToString:@"macro_season_code"]){
        macro_season_code = @"";
        [self.seasonButton setTitle:@"" forState:UIControlStateNormal];
    }
    else if([key isEqualToString:@"family_code"]){
        family_code = @"";
        [self.familyButton setTitle:@"" forState:UIControlStateNormal];
    }
    else if([key isEqualToString:@"composition0"]){
        composition_code0 = @"";
        composition_perc0 = @"";
        [self.compositionButton0 setTitle:@"" forState:UIControlStateNormal];
        [self.compositionPercentageButton0 setTitle:@"" forState:UIControlStateNormal];
    }
    else if([key isEqualToString:@"composition1"]){
        composition_code1 = @"";
        composition_perc1 = @"";
        [self.compositionButton1 setTitle:@"" forState:UIControlStateNormal];
        [self.compositionPercentageButton1 setTitle:@"" forState:UIControlStateNormal];
    }
    else if([key isEqualToString:@"composition2"]){
        composition_code2 = @"";
        composition_perc2 = @"";
        [self.compositionButton2 setTitle:@"" forState:UIControlStateNormal];
        [self.compositionPercentageButton2 setTitle:@"" forState:UIControlStateNormal];
    }
    
    else if([key isEqualToString:@"stitch_type0"]){
        self.stitch_type0 = nil;
        [self.stitchTypeButton0 setTitle:@"" forState:UIControlStateNormal];
    }
    else if([key isEqualToString:@"stitch_type1"]){
        self.stitch_type1 = nil;
        [self.stitchTypeButton1 setTitle:@"" forState:UIControlStateNormal];
    }
    else if([key isEqualToString:@"stitch_type2"]){
        self.stitch_type2 = nil;
        [self.stitchTypeButton2 setTitle:@"" forState:UIControlStateNormal];
    }
    
    
    
    else if([key isEqualToString:@"supplier_codes"]){
        supplier_codes = @"";
        [self.supplierButton setTitle:@"" forState:UIControlStateNormal];
    }
    else if([key isEqualToString:@"yarn_codes"]){
        yarn_code = @"";
        [self.yarnButton setTitle:@"" forState:UIControlStateNormal];
    }
    
    [self applyFilters];

}

/**
 * if implement protocol <GCTagLabelListDataSource> method 'maxNumberOfRowAtTagList'
 * and the taglist's rows is more than the maxRow, this method will be call.
 *
 * @retVal NSString the text for the TagLabel of theMaxRow's last one.
 */
//- (NSString*)tagList:(GCTagList *)tagList labelTextForGroupTagLabel:(NSInteger)interruptIndex{
//    
//}

#pragma mark -
- (void)clearFiltersLabels{
    
    self.yearField.text = @"";
    self.thinnessField.text = @"";
    [self.seasonButton setTitle:@"" forState:UIControlStateNormal];
    [self.familyButton setTitle:@"" forState:UIControlStateNormal];
    [self.compositionButton0 setTitle:@"" forState:UIControlStateNormal];
    [self.compositionPercentageButton0 setTitle:@"" forState:UIControlStateNormal];
    [self.compositionButton1 setTitle:@"" forState:UIControlStateNormal];
    [self.compositionPercentageButton1 setTitle:@"" forState:UIControlStateNormal];
    [self.compositionButton2 setTitle:@"" forState:UIControlStateNormal];
    [self.compositionPercentageButton2 setTitle:@"" forState:UIControlStateNormal];
    
    [self.stitchTypeButton0 setTitle:@"" forState:UIControlStateNormal];
    [self.stitchTypeButton1 setTitle:@"" forState:UIControlStateNormal];
    [self.stitchTypeButton2 setTitle:@"" forState:UIControlStateNormal];

    
}

- (void)clearFilters{
    //setup empty filter fields
    self.filtersFields = [[S2SOrderedDictionary alloc] init];
    
    [self.filtersFields setObject:@"" forKey:@"year"];
    [self.filtersFields setObject:@"" forKey:@"thinness"];
    [self.filtersFields setObject:@"" forKey:@"macro_season_code"];
    [self.filtersFields setObject:@"" forKey:@"family_code"];
    
    [self.filtersFields setObject:@"" forKey:@"composition0"];
    [self.filtersFields setObject:@"" forKey:@"composition1"];
    [self.filtersFields setObject:@"" forKey:@"composition2"];
    
    [self.filtersFields setObject:@"" forKey:@"composition_perc0"];
    [self.filtersFields setObject:@"" forKey:@"composition_perc1"];
    [self.filtersFields setObject:@"" forKey:@"composition_perc2"];
    
    [self.filtersFields setObject:@"" forKey:@"stitch_type0"];
    [self.filtersFields setObject:@"" forKey:@"stitch_type1"];
    [self.filtersFields setObject:@"" forKey:@"stitch_type2"];
    
    [self.filtersFields setObject:@"" forKey:@"supplier_codes"];
    [self.filtersFields setObject:@"" forKey:@"yarn_codes"];
    
    [self.filtersFields setObject:@"" forKey:@"with_pictures"];
    [self.filtersFields setObject:@"" forKey:@"stored"];
    [self.filtersFields setObject:@"" forKey:@"shima"];
    [self.filtersFields setObject:@"" forKey:@"stoll"];
}

- (void)loadFiltersData{
    
    macro_season_filters = [MacroSeason filterValues];
    yarn_filters = [Yarn filterValues];
    composition_filters = [Fibre filterValues];
    supplier_filters = [Supplier filterValues];
    family_filters = [Family filterValues];
    
    composition_percentages = [[S2SOrderedDictionary alloc] init];
    [composition_percentages setObject:@"" forKey:@""];
    [composition_percentages setObject:@"composition_percentage.any" forKey:@"any"];
    [composition_percentages setObject:@"composition_percentage.10_percent" forKey:@"10"];
    [composition_percentages setObject:@"composition_percentage.20_percent" forKey:@"20"];
    [composition_percentages setObject:@"composition_percentage.50_percent" forKey:@"50"];
    [composition_percentages setObject:@"composition_percentage.75_percent" forKey:@"75"];
    [composition_percentages setObject:@"composition_percentage.100_percent" forKey:@"100"];
    //[self checkBrandsVisibility];
}

#pragma mark - TreeviewDelegate

- (void)selectedStitchType:(StitchType *)stitchType{
    NSLog(@"Received selection....");
    
    if([self.stitch_type_key isEqualToString:@"stitch_type0"]){
        self.stitch_type0 = stitchType;
        [self setTitle:self.stitch_type0.item_description forButton:self.stitchTypeButton0];
    }
    else if([self.stitch_type_key isEqualToString:@"stitch_type1"]){
        self.stitch_type1 = stitchType;
        [self setTitle:self.stitch_type1.item_description forButton:self.stitchTypeButton1];
    }
    else{
        self.stitch_type2 = stitchType;
        [self setTitle:self.stitch_type2.item_description forButton:self.stitchTypeButton2];
    }
    
    [self applyFiltersAndNotifyDelegate:YES];
}

#pragma mark -

- (void)applyFilters{
    [self applyFiltersAndNotifyDelegate:YES];
}

- (void)applyFiltersAndNotifyDelegate:(BOOL)notifyDelegate{
    
    [self clearFilters];
    activeFilters = [[S2SOrderedDictionary alloc] init];
    
    if(self.yearField.text && ![self.yearField.text isEqualToString:@""]){
        NSString *year = ([self.yearField.text isEqualToString:@"2000"] ? @"2020" : self.yearField.text);
        [self.filtersFields setObject:year forKey:@"year"];
                           
        [activeFilters setObject:[NSString stringWithFormat:@"%@: %d",NSLocalizedString(@"active_filter.year", @"year"),[self.yearField.text intValue]] forKey:@"year"];
    }
    
    if(self.thinnessField.text && ![self.thinnessField.text isEqualToString:@""]){
        [self.filtersFields setObject:self.thinnessField.text forKey:@"thinness"];
        [activeFilters setObject:[NSString stringWithFormat:@"%@: %d",NSLocalizedString(@"active_filter.thinness", @"thinness"),[self.thinnessField.text intValue]] forKey:@"thinness"];
    }
    
    if(macro_season_code && ![macro_season_code isEqualToString:@""]){
        [self.filtersFields setObject:macro_season_code forKey:@"macro_season_code"];
        
        NSString *key = [macro_season_filters keyOfObject:macro_season_code];
        NSString *value = [NSString stringWithFormat:@"%@: %@",NSLocalizedString(@"active_filter.macro_season_code", @"macro_season_code"),key];
        [activeFilters setObject:value forKey:@"macro_season_code"];
    }
    
    if(family_code && ![family_code isEqualToString:@""]){
        [self.filtersFields setObject:family_code forKey:@"family_code"];
        NSString *key = [family_filters keyOfObject:family_code];
        NSString *value = [NSString stringWithFormat:@"%@: %@",NSLocalizedString(@"active_filter.family_code", @"family_code"),key];
        [activeFilters setObject:value forKey:@"family_code"];
    }
    
    if(yarn_code && ![yarn_code isEqualToString:@""]){
        [self.filtersFields setObject:yarn_code forKey:@"yarn_codes"];
        NSString *key = [yarn_filters keyOfObject:yarn_code];
        NSString *value = [NSString stringWithFormat:@"%@: %@",NSLocalizedString(@"active_filter.yarn_code", @"yarn_code"),key];
        [activeFilters setObject:value forKey:@"yarn_codes"];
    }
    
    if(supplier_codes && ![supplier_codes isEqualToString:@""]){
        [self.filtersFields setObject:supplier_codes forKey:@"supplier_codes"];
        NSString *key = [supplier_filters keyOfObject:supplier_codes];
        NSString *value = [NSString stringWithFormat:@"%@: %@",NSLocalizedString(@"active_filter.supplier_code", @"supplier_code"),key];
        [activeFilters setObject:value forKey:@"supplier_codes"];
    }
    
    //Composition stuff
    if(composition_code0  && ![composition_code0 isEqualToString:@""]){
        [self.filtersFields setObject:composition_code0 forKey:@"composition0"];
        if(composition_perc0)
            [self.filtersFields setObject:composition_perc0 forKey:@"composition_perc0"];
        
        NSMutableString *description = [[NSMutableString alloc] initWithString:[composition_filters keyOfObject:composition_code0]];
        
        if(composition_perc0 && ![composition_perc0 isEqualToString:@""] && ![composition_perc0 isEqualToString:@"composition_percentage.any"])
            [description appendFormat:@"%@ %@%%",([ProductFibre floatPercentageFromString:composition_perc0] < 100.0 ? @" > " : @""), NSLocalizedString(composition_perc0, @"")];
        
        [activeFilters setObject:description forKey:@"composition0"];
    }
    
    if(composition_code1  && ![composition_code1 isEqualToString:@""]){
        [self.filtersFields setObject:composition_code1 forKey:@"composition1"];
        if(composition_perc1)
            [self.filtersFields setObject:composition_perc1 forKey:@"composition_perc1"];
        
        NSMutableString *description = [[NSMutableString alloc] initWithString:[composition_filters keyOfObject:composition_code1]];
        
        if(composition_perc1 && ![composition_perc1 isEqualToString:@""] && ![composition_perc1 isEqualToString:@"composition_percentage.any"])
            [description appendFormat:@"%@ %@%%",([ProductFibre floatPercentageFromString:composition_perc1] < 100.0 ? @" > " : @""), NSLocalizedString(composition_perc1, @"")];
        
        [activeFilters setObject:description forKey:@"composition1"];
    }
    
    if(composition_code2 && ![composition_code2 isEqualToString:@""]){
        [self.filtersFields setObject:composition_code2 forKey:@"composition2"];
        if(composition_perc2)
            [self.filtersFields setObject:composition_perc2 forKey:@"composition_perc2"];
        
        NSMutableString *description = [[NSMutableString alloc] initWithString:[composition_filters keyOfObject:composition_code2]];
        
        if(composition_perc2  && ![composition_perc2 isEqualToString:@""] && ![composition_perc2 isEqualToString:@"composition_percentage.any"])
            [description appendFormat:@"%@ %@%%",([ProductFibre floatPercentageFromString:composition_perc2] < 100.0 ? @" > " : @""), NSLocalizedString(composition_perc2, @"")];
        
        [activeFilters setObject:description forKey:@"composition2"];
    }
    
    //Stitch Type stuff
    if(self.stitch_type0  && self.stitch_type0 != nil){
        [self.filtersFields setObject:self.stitch_type0.code forKey:@"stitch_type0"];
        
        NSString *value = [NSString stringWithFormat:@"%@: %@",NSLocalizedString(@"active_filter.stitch_type", @"stitch_type"),self.stitch_type0.item_description];
        [activeFilters setObject:value forKey:@"stitch_type0"];
    }
    
    if(self.stitch_type1  && self.stitch_type1 != nil){
        [self.filtersFields setObject:self.stitch_type1.code forKey:@"stitch_type1"];
        NSString *value = [NSString stringWithFormat:@"%@: %@",NSLocalizedString(@"active_filter.stitch_type", @"stitch_type"),self.stitch_type1.item_description];
        [activeFilters setObject:value forKey:@"stitch_type1"];
    }
    
    if(self.stitch_type2  && self.stitch_type2 != nil){
        [self.filtersFields setObject:self.stitch_type2.code forKey:@"stitch_type2"];
        NSString *value = [NSString stringWithFormat:@"%@: %@",NSLocalizedString(@"active_filter.stitch_type", @"stitch_type"),self.stitch_type2.item_description];
        [activeFilters setObject:value forKey:@"stitch_type2"];
    }
    
    //new fields
    if(self.with_pictures.on){
        [self.filtersFields setObject:[NSNumber numberWithBool:self.with_pictures.on] forKey:@"with_pictures"];
        [activeFilters setObject:NSLocalizedString(@"active_filter.with_pictures", @"with_pictures") forKey:@"with_pictures"];
    }
    
    if(self.stored.on){
        [self.filtersFields setObject:[NSNumber numberWithBool:self.stored.on] forKey:@"stored"];
        [activeFilters setObject:NSLocalizedString(@"active_filter.only_stored", @"only_stored") forKey:@"stored"];
    }
    
    if(self.shima.on){
        [self.filtersFields setObject:[NSNumber numberWithBool:self.shima.on] forKey:@"prgshima_exists"];
        [activeFilters setObject:NSLocalizedString(@"active_filter.only_shima", @"only_shima") forKey:@"prgshima_exists"];
    }
    
    if(self.stoll.on){
        [self.filtersFields setObject:[NSNumber numberWithBool:self.stoll.on] forKey:@"prgstoll_exists"];
        [activeFilters setObject:NSLocalizedString(@"active_filter.only_stoll", @"only_stoll") forKey:@"prgstoll_exists"];
    }
    
    
 
    [self setupActiveFiltersView];
    
    if(notifyDelegate)
        [self notifyDelegate];
}

- (void)setTitle:(NSString *)title forButton:(UIButton*)button{
    if([title rangeOfString:@" -- "].location == NSNotFound)
        [button setTitle:title forState:UIControlStateNormal];
    else
        [button setTitle:@"" forState:UIControlStateNormal];
}

- (void)notifyDelegate{
    if(self.delegate && [self.delegate respondsToSelector:@selector(filtersUpdated:)])
        [self.delegate performSelector:@selector(filtersUpdated:) withObject:self.filtersFields];

}

- (void) disableOtherCompositionsThan:(NSString*)composition_key{
    
    if(![composition_key isEqualToString:@"composition_percentage0"]){
        composition_code0 = @"";
        [self.compositionButton0 setTitle:@"" forState:UIControlStateNormal];
        [self.compositionButton0 setEnabled:NO];
        [self.compositionButton0 setAlpha:0.6];
        
        composition_perc1 = @"";
        [self.compositionPercentageButton0 setTitle:@"" forState:UIControlStateNormal];
        [self.compositionPercentageButton0 setEnabled:NO];
        [self.compositionPercentageButton0 setAlpha:0.6];
    }
    
    if(![composition_key isEqualToString:@"composition_percentage1"]){
        composition_code1 = @"";
        [self.compositionButton1 setTitle:@"" forState:UIControlStateNormal];
        [self.compositionButton1 setEnabled:NO];
        [self.compositionButton1 setAlpha:0.6];
        
        composition_perc1 = @"";
        [self.compositionPercentageButton1 setTitle:@"" forState:UIControlStateNormal];
        [self.compositionPercentageButton1 setEnabled:NO];
        [self.compositionPercentageButton1 setAlpha:0.6];
    }
    
    if(![composition_key isEqualToString:@"composition_percentage2"]){
        composition_code2 = @"";
        [self.compositionButton2 setTitle:@"" forState:UIControlStateNormal];
        [self.compositionButton2 setEnabled:NO];
        [self.compositionButton2 setAlpha:0.6];
        
        
        composition_perc2 = @"";
        [self.compositionPercentageButton2 setTitle:@"" forState:UIControlStateNormal];
        [self.compositionPercentageButton2 setEnabled:NO];
        [self.compositionPercentageButton2 setAlpha:0.6];
    }
}

- (void) enableCompositions{
    
    [self.compositionButton0 setEnabled:YES];
    [self.compositionButton0 setAlpha:1.0];
    [self.compositionPercentageButton0 setEnabled:YES];
    [self.compositionPercentageButton0 setAlpha:1.0];
    
    [self.compositionButton1 setEnabled:YES];
    [self.compositionButton1 setAlpha:1.0];
    [self.compositionPercentageButton1 setEnabled:YES];
    [self.compositionPercentageButton1 setAlpha:1.0];

    [self.compositionButton2 setEnabled:YES];
    [self.compositionButton2 setAlpha:1.0];
    [self.compositionPercentageButton2 setEnabled:YES];
    [self.compositionPercentageButton2 setAlpha:1.0];

}

#pragma mark - IBActions

- (IBAction)pressedOnButton:(id)sender{
    UIButton *btn = sender;
    NSUInteger tag = btn.tag;
    
    switch (tag) {
        case 0:
            [self.yearField becomeFirstResponder];
            break;
        case 1:
            [self pickSeason:self];
            break;
        case 2:
            [self pickfamily:self];
            break;
        case 3:
            [self pickYarn:self];
            break;
        case 4:
            [self pickSupplier:self];
            break;
        case 5:
            [self.thinnessField becomeFirstResponder];
            break;
        default:
            break;
    }
}

- (IBAction)search:(id)sender{
    [self applyFilters];
}

- (IBAction)pickSeason:(id)sender{
    
    ActionStringDoneBlock done = ^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        [self setTitle:selectedValue forButton:self.seasonButton];
        macro_season_code = [macro_season_filters objectForKey:selectedValue];
        [self applyFilters];

    };
    ActionStringCancelBlock cancel = ^(ActionSheetStringPicker *picker) {
        NSLog(@"Block Picker Canceled");
    };
    
    NSString *key = [macro_season_filters keyOfObject:macro_season_code];
    NSUInteger selected_index = (key == nil || [macro_season_filters indexForKey:key] == -1 ? 0 : [macro_season_filters indexForKey:key]);
    
    ActionSheetStringPicker *picker = [[ActionSheetStringPicker alloc] initWithTitle:NSLocalizedString(@"Select a season",@"") rows:macro_season_filters.allKeys initialSelection:selected_index doneBlock:done cancelBlock:cancel origin:self.superview];
    
    [picker setHideCancel:YES];
    [picker addCustomButtonWithTitle:NSLocalizedString(@"Reset",@"reset filter") value:@""];
    
    [picker showActionSheetPicker];
    
    
}

#pragma mark - composition pickers

- (IBAction)pickComposition0:(id)sender{
    
    ActionStringDoneBlock done = ^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        [self setTitle:selectedValue forButton:self.compositionButton0];
        composition_code0 = [composition_filters objectForKey:selectedValue];
        
        [self applyFilters];
    };
    
    ActionStringCancelBlock cancel = ^(ActionSheetStringPicker *picker) {
        NSLog(@"Block Picker Canceled");
    };
    
    NSString *key = [composition_filters keyOfObject:composition_code0];
    NSUInteger selected_index = (key == nil || [composition_filters indexForKey:key] == -1 ? 0 : [composition_filters indexForKey:key]);
    
    ActionSheetStringPicker *picker = [[ActionSheetStringPicker alloc] initWithTitle:NSLocalizedString(@"Select a composition",@"") rows:composition_filters.allKeys initialSelection:selected_index doneBlock:done cancelBlock:cancel origin:self.superview];
    
    [picker setUseSearchField:YES];
    [picker setHideCancel:YES];
    [picker addCustomButtonWithTitle:NSLocalizedString(@"Reset",@"reset filter") value:@""];
    
    [picker showActionSheetPicker];
}

- (IBAction)pickComposition1:(id)sender{
    
    ActionStringDoneBlock done = ^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        [self setTitle:selectedValue forButton:self.compositionButton1];
        composition_code1 = [composition_filters objectForKey:selectedValue];
        [self applyFilters];
    };
    ActionStringCancelBlock cancel = ^(ActionSheetStringPicker *picker) {
        NSLog(@"Block Picker Canceled");
    };
    
    NSString *key = [composition_filters keyOfObject:composition_code1];
    NSUInteger selected_index = (key == nil || [composition_filters indexForKey:key] == -1 ? 0 : [composition_filters indexForKey:key]);
    
    ActionSheetStringPicker *picker = [[ActionSheetStringPicker alloc] initWithTitle:NSLocalizedString(@"Select a composition",@"") rows:composition_filters.allKeys initialSelection:selected_index doneBlock:done cancelBlock:cancel origin:self.superview];
    
    [picker setUseSearchField:YES];
    [picker setHideCancel:YES];
    [picker addCustomButtonWithTitle:NSLocalizedString(@"Reset",@"reset filter") value:@""];
    
    [picker showActionSheetPicker];
}

- (IBAction)pickComposition2:(id)sender{
    
    ActionStringDoneBlock done = ^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        [self setTitle:selectedValue forButton:self.compositionButton2];
        composition_code2 = [composition_filters objectForKey:selectedValue];
        [self applyFilters];
    };
    ActionStringCancelBlock cancel = ^(ActionSheetStringPicker *picker) {
        NSLog(@"Block Picker Canceled");
    };
    
    NSString *key = [composition_filters keyOfObject:composition_code2];
    NSUInteger selected_index = (key == nil || [composition_filters indexForKey:key] == -1 ? 0 : [composition_filters indexForKey:key]);
    
    ActionSheetStringPicker *picker = [[ActionSheetStringPicker alloc] initWithTitle:NSLocalizedString(@"Select a composition",@"") rows:composition_filters.allKeys initialSelection:selected_index doneBlock:done cancelBlock:cancel origin:self.superview];
    
    [picker setUseSearchField:YES];
    [picker setHideCancel:YES];
    [picker addCustomButtonWithTitle:NSLocalizedString(@"Reset",@"reset filter") value:@""];
    
    [picker showActionSheetPicker];
}

#pragma mark - Stitch Type popovers

- (IBAction)pickStitchType0:(id)sender{
    self.stitch_type_key = @"stitch_type0";
    if(self.delegate)
        [(MSRItemsViewController *)self.delegate performSegueWithIdentifier:@"treeView" sender:self];
}

- (IBAction)pickStitchType1:(id)sender{
    self.stitch_type_key = @"stitch_type1";
    if(self.delegate)
        [(MSRItemsViewController *)self.delegate performSegueWithIdentifier:@"treeView" sender:self];
}

- (IBAction)pickStitchType2:(id)sender{
    self.stitch_type_key = @"stitch_type2";
    if(self.delegate)
        [(MSRItemsViewController *)self.delegate performSegueWithIdentifier:@"treeView" sender:self];
}

#pragma mark -

- (IBAction)pickPercentage0:(id)sender{
    
    ActionStringDoneBlock done = ^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        [self setTitle:selectedValue forButton:self.compositionPercentageButton0];
        composition_perc0 = [composition_percentages objectForKey:selectedValue];
        
        if([selectedValue isEqualToString:@"100"])
            [self disableOtherCompositionsThan:@"composition_percentage0"];
        else
            [self enableCompositions];
        
        [self applyFilters];
    };
    
    S2SOrderedDictionary *translated = [self translatedPercentages];
    
    ActionStringCancelBlock cancel = ^(ActionSheetStringPicker *picker) {
        NSLog(@"Block Picker Canceled");
    };
    
    NSString *key = [translated keyOfObject:composition_perc0];
    NSUInteger selected_index = (key == nil || [translated indexForKey:key] == -1 ? 0 : [translated indexForKey:key]);
    
    ActionSheetStringPicker *picker = [[ActionSheetStringPicker alloc] initWithTitle:NSLocalizedString(@"Select a percentage",@"") rows:translated.allKeys initialSelection:selected_index doneBlock:done cancelBlock:cancel origin:self.superview];
    
    [picker setUseSearchField:YES];
    [picker setHideCancel:YES];
    [picker addCustomButtonWithTitle:NSLocalizedString(@"Reset",@"reset filter") value:@""];
    
    [picker showActionSheetPicker];
}

- (IBAction)pickPercentage1:(id)sender{
    
    ActionStringDoneBlock done = ^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        [self setTitle:selectedValue forButton:self.compositionPercentageButton1];
        composition_perc1 = [composition_percentages objectForKey:selectedValue];
        
        if([selectedValue isEqualToString:@"100"])
            [self disableOtherCompositionsThan:@"composition_percentage1"];
        else
            [self enableCompositions];
        
        
        [self applyFilters];
    };
    
    S2SOrderedDictionary *translated = [self translatedPercentages];
    
    ActionStringCancelBlock cancel = ^(ActionSheetStringPicker *picker) {
        NSLog(@"Block Picker Canceled");
    };
    
    NSString *key = [translated keyOfObject:composition_perc1];
    NSUInteger selected_index = (key == nil || [translated indexForKey:key] == -1 ? 0 : [translated indexForKey:key]);
    
    ActionSheetStringPicker *picker = [[ActionSheetStringPicker alloc] initWithTitle:NSLocalizedString(@"Select a percentage",@"") rows:translated.allKeys initialSelection:selected_index doneBlock:done cancelBlock:cancel origin:self.superview];
    
    [picker setUseSearchField:YES];
    [picker setHideCancel:YES];
    [picker addCustomButtonWithTitle:NSLocalizedString(@"Reset",@"reset filter") value:@""];
    
    [picker showActionSheetPicker];
}


- (IBAction)pickPercentage2:(id)sender{
    
    ActionStringDoneBlock done = ^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        [self setTitle:selectedValue forButton:self.compositionPercentageButton2];
        composition_perc2 = [composition_percentages objectForKey:selectedValue];
        
        if([selectedValue isEqualToString:@"100"])
            [self disableOtherCompositionsThan:@"composition_percentage2"];
        else
            [self enableCompositions];
        
        [self applyFilters];
    };
    
    S2SOrderedDictionary *translated = [self translatedPercentages];
    
    ActionStringCancelBlock cancel = ^(ActionSheetStringPicker *picker) {
        NSLog(@"Block Picker Canceled");
    };
    
    NSString *key = [translated keyOfObject:composition_perc2];
    NSUInteger selected_index = (key == nil || [translated indexForKey:key] == -1 ? 0 : [translated indexForKey:key]);
    
    ActionSheetStringPicker *picker = [[ActionSheetStringPicker alloc] initWithTitle:NSLocalizedString(@"Select a percentage",@"") rows:translated.allKeys initialSelection:selected_index doneBlock:done cancelBlock:cancel origin:self.superview];
    
    [picker setUseSearchField:YES];
    [picker setHideCancel:YES];
    [picker addCustomButtonWithTitle:NSLocalizedString(@"Reset",@"reset filter") value:@""];
    
    [picker showActionSheetPicker];
}


- (S2SOrderedDictionary*)translatedPercentages{
    S2SOrderedDictionary *tmp = [[S2SOrderedDictionary alloc] init];
    
    for (NSString *key in composition_percentages.allKeys)
        [tmp setObject:NSLocalizedString([composition_percentages objectForKey:key], key) forKey:key];

    return tmp;
    
}

#pragma mark -

- (IBAction)pickSupplier:(id)sender{
    
    ActionStringDoneBlock done = ^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        [self setTitle:selectedValue forButton:self.supplierButton];
        supplier_codes = [supplier_filters objectForKey:selectedValue];
        [self applyFilters];
    };
    ActionStringCancelBlock cancel = ^(ActionSheetStringPicker *picker) {
        NSLog(@"Block Picker Canceled");
    };
    
    NSString *key = [supplier_filters keyOfObject:supplier_codes];
    NSUInteger selected_index = (key == nil || [supplier_filters indexForKey:key] == -1 ? 0 : [supplier_filters indexForKey:key]);
    
    ActionSheetStringPicker *picker = [[ActionSheetStringPicker alloc] initWithTitle:NSLocalizedString(@"Select a supplier",@"") rows:supplier_filters.allKeys initialSelection:selected_index doneBlock:done cancelBlock:cancel origin:self.superview];
    
    [picker setUseSearchField:YES];
    [picker setHideCancel:YES];
    [picker addCustomButtonWithTitle:NSLocalizedString(@"Reset",@"reset filter") value:@""];
    
    [picker showActionSheetPicker];
}

- (IBAction)pickYarn:(id)sender{
    
    ActionStringDoneBlock done = ^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        [self setTitle:selectedValue forButton:self.yarnButton];
        yarn_code = [yarn_filters objectForKey:selectedValue];
        [self applyFilters];
    };
    ActionStringCancelBlock cancel = ^(ActionSheetStringPicker *picker) {
        NSLog(@"Block Picker Canceled");
    };
    
    NSString *key = [yarn_filters keyOfObject:yarn_code];
    NSUInteger selected_index = (key == nil || [yarn_filters indexForKey:key] == -1 ? 0 : [yarn_filters indexForKey:key]);
    
    ActionSheetStringPicker *picker = [[ActionSheetStringPicker alloc] initWithTitle:NSLocalizedString(@"Select a yarn",@"") rows:yarn_filters.allKeys initialSelection:selected_index doneBlock:done cancelBlock:cancel origin:self.superview];
    
    [picker setUseSearchField:YES];
    [picker setHideCancel:YES];
    [picker addCustomButtonWithTitle:NSLocalizedString(@"Reset",@"reset filter") value:@""];
    
    [picker showActionSheetPicker];
}

- (IBAction)pickfamily:(id)sender{
    
    ActionStringDoneBlock done = ^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        [self setTitle:selectedValue forButton:self.familyButton];
        family_code = [family_filters objectForKey:selectedValue];
        [self applyFilters];
    };
    ActionStringCancelBlock cancel = ^(ActionSheetStringPicker *picker) {
        NSLog(@"Block Picker Canceled");
    };
    
    NSString *key = [family_filters keyOfObject:family_code];
    NSUInteger selected_index = (key == nil || [family_filters indexForKey:key] == -1 ? 0 : [family_filters indexForKey:key]);
    
    ActionSheetStringPicker *picker = [[ActionSheetStringPicker alloc] initWithTitle:NSLocalizedString(@"Select a family",@"") rows:family_filters.allKeys initialSelection:selected_index doneBlock:done cancelBlock:cancel origin:self.superview];
    
    [picker setUseSearchField:YES];
    [picker setHideCancel:YES];
    [picker addCustomButtonWithTitle:NSLocalizedString(@"Reset",@"reset filter") value:@""];
    
    [picker showActionSheetPicker];
}

#pragma mark -

@end
