//
//  BTActivation.m
//  Geoplast
//
//  Created by Davide Cenzi on 19/08/13.
//
//

#import "BTActivation.h"

#define TIMEOUT 120 //days
#define BASE_URL @"http://web-activations.s3.amazonaws.com"

@interface BTActivation ()

+ (NSString*)appActivationKey;

@end

@implementation BTActivation

+ (NSString*)appBundleIdentifier{
    return [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleIdentifier"];
}

+ (NSString*)appVersion{
    return [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
}

+ (NSString*)appBuildNr{
    return [[NSBundle mainBundle] objectForInfoDictionaryKey:(NSString*)kCFBundleVersionKey];
}

+ (NSDate*)appBuildDate{
    
    NSDate *buildDate = nil;
    NSString *buildDateString = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"BTBundleDateString"];
    
    if(buildDateString != nil && ![buildDateString isEqualToString:@""]){
        
        NSDateFormatter *df = [[NSDateFormatter alloc] init];
        [df setLocale:[NSLocale currentLocale]];
        [df setTimeZone:[NSTimeZone defaultTimeZone]];
        [df setDateFormat:@"yyyy-MM-dd"];
    
        buildDate = [df dateFromString:buildDateString];
    }
    
    return buildDate;
}

+ (int)elapsedDays{
    
    NSCalendar *c = [NSCalendar currentCalendar];
    NSDate *d1 = [NSDate date]; //today
    NSDate *d2 = [BTActivation appBuildDate]; //[NSDate dateWithTimeIntervalSince1970:1340323201];//2012-06-22
    NSDateComponents *components = [c components:NSDayCalendarUnit fromDate:d2 toDate:d1 options:0];
    NSInteger diff = components.day;
    
    // NSDayCalendarUnit|NSHourCalendarUnit|NSMinuteCalendarUnit
    return diff;
}

+ (BOOL)timeoutElapsed{
    
    return ([BTActivation elapsedDays] >= TIMEOUT);
}

+ (NSString*)appActivationKey{
    return [NSString stringWithFormat:@"%@-%@",[BTActivation appBundleIdentifier],[BTActivation appVersion]];
}

+ (BOOL)appActivated{
    NSString *app_key = [BTActivation appActivationKey];
    NSNumber *active = [[NSUserDefaults standardUserDefaults] objectForKey:app_key];
    
    NSLog(@"App activation unique-key: %@ - Value: %@",app_key,active.description);
    
    return ((active != nil) && ([active boolValue] == YES));
}

+ (NSString*)remoteActivationUrl{
    return [NSString stringWithFormat:@"/%@/%@/activation.json",[BTActivation appBundleIdentifier],[BTActivation appVersion]];
}

+ (void)checkActivation{
    
    //ignore if already activated current version
    if([BTActivation appActivated])
        return;
    
    //ignore check if timeout not reached
    if(![BTActivation timeoutElapsed])
        return;
    
    NSString *reachability_descr = @"";
    
    if([[Reachability reachabilityForInternetConnection] currentReachabilityStatus] == NotReachable)
        reachability_descr = @"Not reachable";
    else if([[Reachability reachabilityForInternetConnection] currentReachabilityStatus] == ReachableViaWiFi)
        reachability_descr = @"Reachable via WiFi";
    else if([[Reachability reachabilityForInternetConnection] currentReachabilityStatus] == ReachableViaWWAN)
        reachability_descr = @"Reachable via 3G";
    else{
        reachability_descr = @"Not determinated";
    }
        /*
         NotReachable     = kNotReachable,
         ReachableViaWiFi = kReachableViaWiFi,
         ReachableViaWWAN = kReachableViaWWAN
         */
    
    NSLog(@"Current reachability status: %@",reachability_descr);
    
    if([[Reachability reachabilityForInternetConnection] currentReachabilityStatus] != NotReachable){
        
        NSString *url = [BTActivation remoteActivationUrl];
        
        AFHTTPClient * _httpClient = [AFHTTPClient clientWithBaseURL:[NSURL URLWithString:BASE_URL]];
        _httpClient.parameterEncoding = AFJSONParameterEncoding;
        [_httpClient setDefaultHeader:@"Accept" value:@"application/json"];
        [_httpClient registerHTTPOperationClass:[AFJSONRequestOperation class]];
        
        [_httpClient getPath:url parameters:nil success:^(AFHTTPRequestOperation *op, NSDictionary* JSON){
            
            NSLog(@"Verification completed: %@",[JSON objectForKey:@"status"]);
            
            NSNumber *activeStatus = [NSNumber numberWithBool:([[JSON objectForKey:@"status"] isEqualToString:@"active"] ? YES : NO)];
            
            //saving to user defaults
            NSUserDefaults *d = [NSUserDefaults standardUserDefaults];
            [d setObject:activeStatus forKey:[BTActivation appActivationKey]];
            [d synchronize];
            
            
            if(![[JSON objectForKey:@"status"] isEqualToString:@"active"]){
                [[NSNotificationCenter defaultCenter] postNotificationName:@"APP_NOT_ACTIVE" object:nil userInfo:nil];
            }

            
        } failure:^(AFHTTPRequestOperation *op, NSError *error){
            
            NSLog(@"Error verifying app activation: %@",error.localizedDescription);
            [[NSNotificationCenter defaultCenter] postNotificationName:@"APP_NOT_ACTIVE" object:nil userInfo:nil];
            //Maybe missing file?
            //[[NSNotificationCenter defaultCenter] postNotificationName:@"ACTIVATION_CHECK_FAILURE" object:nil userInfo:nil];
        }];
        
    }
    else{ //timeout fired, app not activated, currently offline. Call application block
        [[NSNotificationCenter defaultCenter] postNotificationName:@"APP_NOT_ACTIVE" object:nil userInfo:nil];
    }
}

@end
