// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Fibre.h instead.

#import <CoreData/CoreData.h>


extern const struct FibreAttributes {
	__unsafe_unretained NSString *code;
	__unsafe_unretained NSString *item_description;
	__unsafe_unretained NSString *language;
	__unsafe_unretained NSString *string_code;
} FibreAttributes;

extern const struct FibreRelationships {
	__unsafe_unretained NSString *product_fibres;
} FibreRelationships;

extern const struct FibreFetchedProperties {
} FibreFetchedProperties;

@class ProductFibre;






@interface FibreID : NSManagedObjectID {}
@end

@interface _Fibre : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (FibreID*)objectID;





@property (nonatomic, strong) NSNumber* code;



@property int32_t codeValue;
- (int32_t)codeValue;
- (void)setCodeValue:(int32_t)value_;

//- (BOOL)validateCode:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* item_description;



//- (BOOL)validateItem_description:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* language;



//- (BOOL)validateLanguage:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* string_code;



//- (BOOL)validateString_code:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSSet *product_fibres;

- (NSMutableSet*)product_fibresSet;





@end

@interface _Fibre (CoreDataGeneratedAccessors)

- (void)addProduct_fibres:(NSSet*)value_;
- (void)removeProduct_fibres:(NSSet*)value_;
- (void)addProduct_fibresObject:(ProductFibre*)value_;
- (void)removeProduct_fibresObject:(ProductFibre*)value_;

@end

@interface _Fibre (CoreDataGeneratedPrimitiveAccessors)


- (NSNumber*)primitiveCode;
- (void)setPrimitiveCode:(NSNumber*)value;

- (int32_t)primitiveCodeValue;
- (void)setPrimitiveCodeValue:(int32_t)value_;




- (NSString*)primitiveItem_description;
- (void)setPrimitiveItem_description:(NSString*)value;




- (NSString*)primitiveLanguage;
- (void)setPrimitiveLanguage:(NSString*)value;




- (NSString*)primitiveString_code;
- (void)setPrimitiveString_code:(NSString*)value;





- (NSMutableSet*)primitiveProduct_fibres;
- (void)setPrimitiveProduct_fibres:(NSMutableSet*)value;


@end
