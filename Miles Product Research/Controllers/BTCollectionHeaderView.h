//
//  BTCollectionHeaderView.h
//  Miles Product Research
//
//  Created by Davide Cenzi on 03/07/14.
//  Copyright (c) 2014 BlueTouch Development di Davide Cenzi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BTCollectionHeaderView : UICollectionReusableView
@property (nonatomic,weak) IBOutlet UILabel *titleLabel;
@property (nonatomic,weak) IBOutlet UILabel *collapsedSymbolLabel;
@property (nonatomic,weak) IBOutlet UIButton *titleButton;
@end
