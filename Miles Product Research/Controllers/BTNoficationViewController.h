//
//  BTNoficationViewController.h
//  Needle
//
//  Created by Davide Cenzi on 01/08/13.
//  Copyright (c) 2013 BlueTouch Development di Davide Cenzi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BTDataSync.h"

@interface BTNoficationViewController : UIViewController

@property (nonatomic,strong) BTDataSync *dataSync;

- (NSArray*)notificationNames;
- (void)handleNotification:(NSNotification *)notification;
- (void)startListeningForNotifications;
- (void)stopListeningForNotifications;


- (void)showBannerWithText:(NSString*)text andDuration:(NSTimeInterval)duration;

@end
